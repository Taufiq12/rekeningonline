using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class CancelPaymentFaspay
    {
        [Key]
        public int Id { get; set; }
        public string response { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public int trx_status_code { get; set; }
        public string trx_status_desc { get; set; }
        public int payment_status_code { get; set; }
        public string payment_status_desc { get; set; }
        public string payment_cancel_msg { get; set; }
        public DateTime? payment_cancel_date { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set ;}

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<CancelPaymentFaspay> db()
        {
            return cx().CancelPaymentFaspay;
        }
        public static CancelPaymentFaspay Find(int Id){
            return db().Where(x=>x.Id==Id).FirstOrDefault();
        }
        
        public static Boolean Insert(CancelPaymentFaspay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.CancelPaymentFaspay.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(CancelPaymentFaspay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.CancelPaymentFaspay.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(CancelPaymentFaspay row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.CancelPaymentFaspay.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                response = this.response,
                trx_id = this.trx_id,
                merchant_id = this.merchant_id,
                merchant = this.merchant,
                bill_no = this.bill_no,
                trx_status_code = this.trx_status_code,
                trx_status_desc = this.trx_status_desc,
                payment_status_code = this.payment_status_code,
                payment_status_desc = this.payment_status_desc,
                payment_cancel_msg = this.payment_cancel_msg,
                payment_cancel_date = this.payment_cancel_date,
                response_code = this.response_code,
                response_desc = this.response_desc
            });
        }
    }
}