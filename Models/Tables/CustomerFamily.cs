using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using Newtonsoft.Json.Linq;
using ProjectBYB.Models.Views;
using ProjectBYB.Models.Tables;
using System.Collections.Generic;
using System.IO;
namespace ProjectBYB.Models.Tables
{
    public class CustomerFamily
    {
        [Key]
        public int id { get; set; }
        public string cif { get; set; }
        public string Mail { get; set; }
        public string FamilyFullName { get; set; }
        public string FamilyRelationshipWithTheApplicant { get; set; }
        public string FamilyPhoneNumber { get; set; }
        public string FamilyAddress { get; set; }
        public string FamilyRT { get; set; }
        public string FamilyRW { get; set; }
        public string FamilyKel { get; set; }
        public string FamilyKec { get; set; }
        public string FamilyKab { get; set; }
        public string FamilyProvince { get; set; }
        public string FamilyPostalCode { get; set; }
        public DateTime? Created_Date { get; set; }

        public static BYB cx()
        {
            return new BYB();
        }
        public static DbSet<CustomerFamily> db()
        {
            return cx().CustomerFamily;
        }
        public static CustomerFamily Find(string Mail)
        {
            return db().Where(x => x.Mail == Mail).FirstOrDefault();
        }

        public CustomerFamily cusFam(){
            var row = CustomerFamily.Find(Mail);
            if(row==null) row = CustomerFamily.Find(Mail);
            return row == null ? new CustomerFamily() : row;
        }

        public void SaveStringFamily(){
            var cus = this.cusFam();
            var row = this;

            var first = CustomerFamily.db().Where(x=>x.Mail==Mail).FirstOrDefault();

            if(row.cif!=null) row.cif = row.cif;
            else if(cus.cif!=null) row.cif = cus.cif;

            if(row.Mail!=null) row.Mail = row.Mail;
            else if(cus.Mail!=null) row.Mail = cus.Mail;

            if(row.FamilyFullName!=null) row.FamilyFullName = row.FamilyFullName;
            else if(cus.FamilyFullName!=null) row.FamilyFullName = cus.FamilyFullName;

            if(row.FamilyRelationshipWithTheApplicant!=null) row.FamilyRelationshipWithTheApplicant = row.FamilyRelationshipWithTheApplicant;
            else if(cus.FamilyRelationshipWithTheApplicant!=null) row.FamilyRelationshipWithTheApplicant = cus.FamilyRelationshipWithTheApplicant;
            
            if(row.FamilyPhoneNumber!=null) row.FamilyPhoneNumber = row.FamilyPhoneNumber;
            else if(cus.FamilyPhoneNumber!=null) row.FamilyPhoneNumber = cus.FamilyPhoneNumber;
            
            if(row.FamilyAddress!=null) row.FamilyAddress = row.FamilyAddress;
            else if(cus.FamilyAddress!=null) row.FamilyAddress = cus.FamilyAddress;
            
            if(row.FamilyRT!=null) row.FamilyRT = row.FamilyRT;
            else if(cus.FamilyRT!=null) row.FamilyRT = cus.FamilyRT;
            
            if(row.FamilyRW!=null) row.FamilyRW = row.FamilyRW;
            else if(cus.FamilyRW!=null) row.FamilyRW = cus.FamilyRW;

            if(row.FamilyKel!=null) row.FamilyKel = row.FamilyKel;
            else if(cus.FamilyKel!=null) row.FamilyKel = cus.FamilyKel;

            if(row.FamilyKec!=null) row.FamilyKec = row.FamilyKec;
            else if(cus.FamilyKec!=null) row.FamilyKec = cus.FamilyKec;

            if(row.FamilyKab!=null) row.FamilyKab = row.FamilyKab;
            else if(cus.FamilyKab!=null) row.FamilyKab = cus.FamilyKab;

            if(row.FamilyProvince!=null) row.FamilyProvince = row.FamilyProvince;
            else if(cus.FamilyProvince!=null) row.FamilyProvince = cus.FamilyProvince;

            if(row.FamilyPostalCode!=null) row.FamilyPostalCode = row.FamilyPostalCode;
            else if(cus.FamilyPostalCode!=null) row.FamilyPostalCode = cus.FamilyPostalCode;

            if(first==null){
				 CustomerFamily.Insert(row);
			}else{
				 CustomerFamily.Update(row);
			}
        }

        public static Boolean Insert(CustomerFamily fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerFamily.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(CustomerFamily fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerFamily.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(CustomerFamily row)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerFamily.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}