using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class ResponsePaymentNotification
    {
         [Key]
        public int Id { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string bill_no { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set; }
        public DateTime response_date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<ResponsePaymentNotification> db()
        {
            return cx().ResponsePaymentNotification;
        }
        public static ResponsePaymentNotification Find(int Id){
            return db().Where(x=>x.Id==Id).FirstOrDefault();
        }
        
        public static Boolean Insert(ResponsePaymentNotification fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.ResponsePaymentNotification.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(ResponsePaymentNotification fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.ResponsePaymentNotification.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(ResponsePaymentNotification row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.ResponsePaymentNotification.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                trx_id = this.trx_id,
                merchant_id = this.merchant_id,
                bill_no = this.bill_no,
                response_code = this.response_code,
                response_desc = this.response_desc,
                response_date = this.response_date
            });
        }
    }
}