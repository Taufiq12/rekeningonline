using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class LoginDukcapil
    {
        [Key]
        public int id { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<LoginDukcapil> db()
        {
            return cx().LoginDukcapil;
        }
        public static LoginDukcapil Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        public static LoginDukcapil FindMail(string Mail){
            return db().Where(x=>x.email==Mail).FirstOrDefault();
        }
        
        public static Boolean Insert(LoginDukcapil fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LoginDukcapil.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(LoginDukcapil fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LoginDukcapil.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(LoginDukcapil row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LoginDukcapil.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.id,
                email = this.email,
                password = this.password
            });
        }
    }
}