using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class ApprovalLogs
    {
        [Key]
        public int id { get; set; }
        public string Mail { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<ApprovalLogs> db()
        {
            return cx().ApprovalLogs;
        }
        public static ApprovalLogs Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        
        public static Boolean Insert(ApprovalLogs fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.ApprovalLogs.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(ApprovalLogs fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.ApprovalLogs.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(ApprovalLogs row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.ApprovalLogs.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

         public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                Mail = this.Mail,
                Status=this.Status,
                Remarks=this.Remarks,
                Created_By=this.Created_By,
                Created_Date=this.Created_Date,
            });
        }
    }
}