using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Tables
{
    public class PostalArea
    {
        [Key]
		public int Id { get; set; }
		public string Kelurahan { get; set; }
		public string Kecamatan { get; set; }
		public string Kabupaten { get; set; }
		public string Provinsi { get; set; }
		public string Kodepos { get; set; }
		public DateTime? Updated_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static IQueryable<PostalArea> db(){
            return cx().PostalArea;
        }
        public static PostalArea Find(int id){
            return db().Where(x=>x.Id==id).FirstOrDefault();
        }

        public static Boolean Insert(PostalArea fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PostalArea.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(PostalArea fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PostalArea.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(PostalArea row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PostalArea.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.Id,
            });
        }
    }
}