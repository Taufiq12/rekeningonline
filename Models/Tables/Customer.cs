using System.Globalization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using Newtonsoft.Json.Linq;
using ProjectBYB.Models.Views;
using ProjectBYB.Models.Tables;
using System.Collections.Generic;
using System.IO;

namespace ProjectBYB.Models.Tables
{
    public class Customer
    {
        [Key]
        public int id { get; set; }
        public string cif { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Title { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MotherName { get; set; }
        public string Country { get; set; }
        public string CountryOther { get; set; }
        public string Identity { get; set; }
        public string TypeIdentity { get; set; }
        public DateTime? StartDateIdentity { get; set; }
        public DateTime? EndDateIdentity { get; set; }
        public string AddressIdentity { get; set; }
        public string RTIdentity { get; set; }
        public string RWIdentity { get; set; }
        public string KelIdentity { get; set; }
        public string KecIdentity { get; set; }
        public string KabIdentity { get; set; }
        public string ProvinceIdentity { get; set; }
        public string PostalCodeIdentity { get; set; }
        public string ResidenceAddress { get; set; }
        public string RTResidence { get; set; }
        public string RWResidence { get; set; }
        public string KelResidence { get; set; }
        public string KecResidence { get; set; }
        public string KabResidence { get; set; }
        public string ProvinceResidence { get; set; }
        public string PostalCodeResidence { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Mail { get; set; }
        public string Education { get; set; }
        public string religion { get; set; }
        public string MaterialStatus { get; set; }
        public int NumberOfDependents { get; set; }
        public string HomeStatus { get; set; }
        public int LongOccupyYear { get; set; }
        public int LongOccupyMonth { get; set; }
        public string Hobby { get; set; }
        public string NPWP { get; set; }
        public int? msg_id { get; set; }
        public DateTime? Created_Date { get; set; }
        public string Status { get; set; }
        public string StatusType { get; set; }
        public Boolean? IsDomisili { get; set; }
        public string KodeCabang { get; set; }
        public static BYB cx()
        {
            return new BYB();
        }
        public static DbSet<Customer> db()
        {
            return cx().Customer;
        }
        public static Customer Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        public static Customer Find(string Mail){
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }
        public static Customer FindMail(string Mail){
            if(Mail==null) return null;
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }

        public Customer info(){
            var row = Customer.Find(id);
            return row == null ? new Customer() : row;
        }

        public JObject GetData(){
            var row = Gen.Combine(this, new {
            });
            return Gen.Combine(row, info());
        }

        public static Boolean Insert(Customer fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.Customer.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Customer fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.Customer.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(Customer row)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.Customer.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}
