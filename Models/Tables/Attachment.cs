using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Attachment
    {
        [Key]
        public int Id { get; set; }
        public string Mail { get; set; }
        public string Attachment1 { get; set; }
        public string Attachment2 { get; set; }
        public string Attachment3 { get; set; }
        public DateTime? Upload_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Attachment> db()
        {
            return cx().Attachment;
        }
        public static Attachment Find(int id){
            return db().Where(x=>x.Id==id).FirstOrDefault();
        }

        public static Attachment FindMail(string Mail){
            return db().Where(x=>x.Mail == Mail).FirstOrDefault();
        }
        
        public static Boolean Insert(Attachment fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Attachment.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Attachment fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Attachment.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Attachment row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Attachment.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

         public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.Id,
                Mail = this.Mail,
                Attachment1=this.Attachment1,
                Attachment2=this.Attachment2,
                Attachment3=this.Attachment3
            });
        }
    }
}