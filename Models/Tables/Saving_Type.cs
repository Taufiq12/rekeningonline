using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Saving_Type
    {
        [Key]
        public int id { get; set; }
        public string TypeCard { get; set; }
        public string TypeName { get; set; }
        public string SavingType { get; set; }
        public decimal Nominal_Saldo { get; set; }
        public string Terbilang { get; set; }
        public string Saldo { get; set; }
        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Saving_Type> db()
        {
            return cx().Saving_Type;
        }
        public static Saving_Type Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }

        public static Saving_Type FindType(string TypeName){
            return db().Where(x=>x.TypeName == TypeName).FirstOrDefault();
        }
        
        public static Saving_Type FindSavingType(string SavingType, string TypeCard){
            return db().Where(x=>x.SavingType == SavingType && x.TypeCard == TypeCard).FirstOrDefault();
        }
        public static Boolean Insert(Saving_Type fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Saving_Type.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Saving_Type fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Saving_Type.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Saving_Type row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Saving_Type.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                TypeCard = this.TypeCard,
                TypeName = this.TypeName,
                SavingType = this.SavingType,
                Nominal_Saldo = this.Nominal_Saldo,
                Terbilang = this.Terbilang,
                Saldo = this.Saldo
            });
        }
    }
}