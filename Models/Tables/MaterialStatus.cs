using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class MaterialStatus
    {
        [Key]
        public int id { get; set; }
        public string MaterialStatus_Type { get; set; }
        public string MaterialStatus_Desc { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<MaterialStatus> db()
        {
            return cx().MaterialStatus;
        }
        public static MaterialStatus Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        
        public static Boolean Insert(MaterialStatus fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.MaterialStatus.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(MaterialStatus fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.MaterialStatus.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(MaterialStatus row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.MaterialStatus.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

         public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                MaterialStatus_Type = this.MaterialStatus_Type,
                MaterialStatus_Desc=this.MaterialStatus_Desc,
            });
        }
    }
}