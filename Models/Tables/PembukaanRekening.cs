using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class PembukaanRekening
    {
        [Key]
        public string jenisPenggunaan { get; set; }
        public string jenisRekening { get; set; }
        public string kodeCabang { get; set; }
        public string kodeMataUang { get; set; }
        public string kolektibilitas { get; set; }
        public string lokasi { get; set; }
        public string namaNasabah { get; set; }
        public string namaRekening { get; set; }
        public string nomorNasabah { get; set; }
        public string nomorRekening { get; set; }
        public string request { get; set; }
        public string response { get; set; }
        public string sektorEkonomi { get; set; }
        public DateTime tanggalBuka { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<PembukaanRekening> db()
        {
            return cx().PembukaanRekening;
        }

        public static PembukaanRekening FindCIF(string nomorNasabah){
            return db().Where(x=> x.nomorNasabah == nomorNasabah).FirstOrDefault();
        }
        
        public static PembukaanRekening Find(string namaNasabah, string namaRekening,string nomorNasabah,string nomorRekening){
            return db().Where(x=>x.namaNasabah == namaNasabah && x.namaRekening == namaRekening && x.nomorNasabah == nomorNasabah && x.nomorRekening == nomorRekening).FirstOrDefault();
        }
        
        public static Boolean Insert(PembukaanRekening fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PembukaanRekening.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(PembukaanRekening fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PembukaanRekening.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(PembukaanRekening row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PembukaanRekening.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                jenisPenggunaan = this.jenisPenggunaan,
                jenisRekening = this.jenisRekening,
                kodeCabang = this.kodeCabang,
                kodeMataUang = this.kodeMataUang,
                kolektibilitas = this.kolektibilitas,
                lokasi = this.lokasi,
                namaNasabah = this.namaNasabah,
                namaRekening = this.namaRekening,
                nomorNasabah = this.nomorNasabah,
                nomorRekening = this.nomorRekening,
                request = this.request,
                response = this.response,
                sektorEkonomi = this.sektorEkonomi,
                tanggalBuka = this.tanggalBuka
            });
        }
    }
}