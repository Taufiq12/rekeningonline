using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class LogActivity
    {
        [Key]
        public int id { get; set; }
        public string Email { get; set; }
        public string IssueDate { get; set; }
        public string SystemOperations { get; set; }
        public string Browser { get; set; }
        public string Location { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<LogActivity> db()
        {
            return cx().LogActivity;
        }
        public static LogActivity Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        public static LogActivity FindMail(string Mail){
            return db().Where(x=>x.Email==Mail).FirstOrDefault();
        }
        
        public static Boolean Insert(LogActivity fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LogActivity.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(LogActivity fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LogActivity.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(LogActivity row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LogActivity.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                Email = this.Email,
                IssueDate = this.IssueDate,
                SystemOperations = this.SystemOperations,
                Browser = this.Browser,
                Location = this.Location
            });
        }
    }
}