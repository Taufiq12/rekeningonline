using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using ProjectBYB.Models.Tables;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class CustomerTypeBook
    {
        [Key]
        public string Mail { get; set; }
        public string SavingType { get; set; }
        public string TypeCard { get; set; }

        public static BYB cx()
        {
            return new BYB();
        }
        public static DbSet<CustomerTypeBook> db()
        {
            return cx().CustomerTypeBook;
        }
        public static CustomerTypeBook Find(string Mail)
        {
            return db().Where(x => x.Mail == Mail).FirstOrDefault();
        }

        public CustomerTypeBook cusJobs(){
            var row = CustomerTypeBook.Find(Mail);
            if(row==null) row = CustomerTypeBook.Find(Mail);
            return row == null ? new CustomerTypeBook() : row;
        }

        public static Boolean Insert(CustomerTypeBook fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerTypeBook.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(CustomerTypeBook fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerTypeBook.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(CustomerTypeBook row)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerTypeBook.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}