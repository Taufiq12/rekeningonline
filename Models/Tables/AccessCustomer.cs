using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Tables
{
    public class AccessCustomer
    {
        [Key]
        public int ae_id { get; set; }
        public string ae_mail { get; set; }
        public int? ae_al_id { get; set; }
        public Boolean ae_status { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<AccessCustomer> db(){
            return cx().AccessCustomer;
        }
        public static int lastId(){
            var last = db().OrderByDescending(x=>x.ae_id).FirstOrDefault();
            return last == null ? 0 : last.ae_id;
        }
        public static AccessCustomer Find(int id){
            return db().Where(x=>x.ae_id==id).FirstOrDefault();
        }
        public static Boolean Insert(AccessCustomer fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.AccessCustomer.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(AccessCustomer fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.AccessCustomer.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(AccessCustomer row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.AccessCustomer.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.ae_id,
                status = this.ae_status,
            });
        }
    }
}