using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using ProjectBYB.Models.Tables;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class CustomerJobs
    {
        [Key]
        public int id { get; set; }
        public string cif { get; set; }
        public string Mail { get; set; }
        public string Jobs { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string BusinessField { get; set; }
        public string CompanyAddress { get; set; }
        public string RTCompany { get; set; }
        public string RWCompany { get; set; }
        public string KelCompany { get; set; }
        public string KecCompany { get; set; }
        public string KabCompany { get; set; }
        public string ProvinceCompany { get; set; }
        public string PostalCodeCompany { get; set; }
        public string PhoneNumberCompany { get; set; }
        public string FaxNumberCompany { get; set; }
        public string ExtNumberCompany1 { get; set; }
        public string ExtNumberCompany2 { get; set; }
        public int LengthOfWorkYear { get; set; }
        public int LengthOfWorkMonth { get; set; }
        public DateTime? Created_Date { get; set; }

        public static BYB cx()
        {
            return new BYB();
        }
        public static DbSet<CustomerJobs> db()
        {
            return cx().CustomerJobs;
        }
        public static CustomerJobs Find(string Mail)
        {
            return db().Where(x => x.Mail == Mail).FirstOrDefault();
        }

        public CustomerJobs cusJobs(){
            var row = CustomerJobs.Find(Mail);
            if(row==null) row = CustomerJobs.Find(Mail);
            return row == null ? new CustomerJobs() : row;
        }

        public void SaveStringJobs(){
            var cus = this.cusJobs();
            var row = this;

            var first = CustomerJobs.db().Where(x=>x.Mail==Mail).FirstOrDefault();

            if(row.cif!=null) row.cif = row.cif;
            else if(cus.cif!=null) row.cif = cus.cif;

            if(row.Mail!=null) row.Mail = row.Mail;
            else if(cus.Mail!=null) row.Mail = cus.Mail;

            if(row.Jobs!=null) row.Jobs = row.Jobs;
            else if(cus.Jobs!=null) row.Jobs = cus.Jobs;

            if(row.CompanyName!=null) row.CompanyName = row.CompanyName;
            else if(cus.CompanyName!=null) row.CompanyName = cus.CompanyName;
            
            if(row.Position!=null) row.Position = row.Position;
            else if(cus.Position!=null) row.Position = cus.Position;
            
            if(row.BusinessField!=null) row.BusinessField = row.BusinessField;
            else if(cus.BusinessField!=null) row.BusinessField = cus.BusinessField;
           
            if(row.CompanyAddress!=null) row.CompanyAddress = row.CompanyAddress;
            else if(cus.CompanyAddress!=null) row.CompanyAddress = cus.CompanyAddress;
            
            if(row.RTCompany!=null) row.RTCompany = row.RTCompany;
            else if(cus.RTCompany!=null) row.RTCompany = cus.RTCompany;
            
            if(row.RWCompany!=null) row.RWCompany = row.RWCompany;
            else if(cus.RWCompany!=null) row.RWCompany = cus.RWCompany;

            if(row.KelCompany!=null) row.KelCompany = row.KelCompany;
            else if(cus.KelCompany!=null) row.KelCompany = cus.KelCompany;
            
            if(row.KecCompany!=null) row.KecCompany = row.KecCompany;
            else if(cus.KecCompany!=null) row.KecCompany = cus.KecCompany;
            
            if(row.KabCompany!=null) row.KabCompany = row.KabCompany;
            else if(cus.KabCompany!=null) row.KabCompany = cus.KabCompany;
            
            if(row.ProvinceCompany!=null) row.ProvinceCompany = row.ProvinceCompany;
            else if(cus.ProvinceCompany!=null) row.ProvinceCompany = cus.ProvinceCompany;
           
            if(row.PostalCodeCompany!=null) row.PostalCodeCompany = row.PostalCodeCompany;
            else if(cus.PostalCodeCompany!=null) row.PostalCodeCompany = cus.PostalCodeCompany;
            
            if(row.PhoneNumberCompany!=null) row.PhoneNumberCompany = row.PhoneNumberCompany;
            else if(cus.PhoneNumberCompany!=null) row.PhoneNumberCompany = cus.PhoneNumberCompany;
            
            if(row.FaxNumberCompany!=null) row.FaxNumberCompany = row.FaxNumberCompany;
            else if(cus.FaxNumberCompany!=null) row.FaxNumberCompany = cus.FaxNumberCompany;
            
            if(row.ExtNumberCompany1!=null) row.ExtNumberCompany1 = row.ExtNumberCompany1;
            else if(cus.ExtNumberCompany1!=null) row.ExtNumberCompany1 = cus.ExtNumberCompany1;
            
            if(row.ExtNumberCompany2!=null) row.ExtNumberCompany2 = row.ExtNumberCompany2;
            else if(cus.ExtNumberCompany2!=null) row.ExtNumberCompany2 = cus.ExtNumberCompany2;
            
            if(row.LengthOfWorkYear!=0) row.LengthOfWorkYear = row.LengthOfWorkYear;
            else if(cus.LengthOfWorkYear!=0) row.LengthOfWorkYear = cus.LengthOfWorkYear;
            
            if(row.LengthOfWorkMonth!=0) row.LengthOfWorkMonth = row.LengthOfWorkMonth;
            else if(cus.LengthOfWorkMonth!=0) row.LengthOfWorkMonth = cus.LengthOfWorkMonth;

            if(first==null){
				 CustomerJobs.Insert(row);
			}else{
				 CustomerJobs.Update(row);
			}
        }

        public static Boolean Insert(CustomerJobs fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerJobs.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(CustomerJobs fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerJobs.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(CustomerJobs row)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerJobs.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}