using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class UserVirtualAccountFasPay
    {
        [Key]
        public int Id { get; set; }
        public string response { get; set; }
        public string Email { get; set; }
        public string trx_id { get; set; }
        public string merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set; }
        public string product { get; set; }
        public int qty { get; set; }
        public decimal amount { get; set; }
        public int payment_plan { get; set; }
        public string merchant_id_item { get; set; }
        public string tenor { get; set; }
        public string redirect_url { get; set; }
        public DateTime created_date { get; set; }


        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<UserVirtualAccountFasPay> db()
        {
            return cx().UserVirtualAccountFasPay;
        }
        public static UserVirtualAccountFasPay Find(int Id){
            return db().Where(x=>x.Id==Id).FirstOrDefault();
        }

        public static Boolean Insert(UserVirtualAccountFasPay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.UserVirtualAccountFasPay.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(UserVirtualAccountFasPay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.UserVirtualAccountFasPay.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(UserVirtualAccountFasPay row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.UserVirtualAccountFasPay.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                response = this.response,
                trx_id = this.trx_id,
                merchant_id = this.merchant_id,
                merchant = this.merchant,
                bill_no = this.bill_no,
                response_code = this.response_code,
                response_desc = this.response_desc,
                product = this.product,
                qty = this.qty,
                amount = this.amount,
                payment_plan = this.payment_plan,
                merchant_id_item = this.merchant_id_item,
                tenor = this.tenor
            });
        }
    }
}