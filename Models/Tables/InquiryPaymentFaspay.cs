using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class InquiryPaymentFaspay
    {
        [Key]
        public int Id { get; set; }
        public string response { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public string payment_reff { get; set; }
        public DateTime? payment_date { get; set; }
        public int payment_status_code { get; set; }
        public string payment_status_desc { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<InquiryPaymentFaspay> db()
        {
            return cx().InquiryPaymentFaspay;
        }
        public static InquiryPaymentFaspay Find(int Id){
            return db().Where(x=>x.Id==Id).FirstOrDefault();
        }
        
        public static Boolean Insert(InquiryPaymentFaspay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.InquiryPaymentFaspay.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(InquiryPaymentFaspay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.InquiryPaymentFaspay.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(InquiryPaymentFaspay row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.InquiryPaymentFaspay.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                response = this.response,
                trx_id = this.trx_id,
                merchant_id = this.merchant_id,
                merchant = this.merchant,
                bill_no = this.bill_no,
                payment_reff = this.payment_reff,
                payment_date = this.payment_date,
                payment_status_code = this.payment_status_code,
                payment_status_desc = this.payment_status_desc,
                response_code = this.response_code,
                response_desc = this.response_desc
            });
        }
    }
}