using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class KodeReferance
    {
        [Key]
        public int id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string KTP { get; set; }
        public string Reporting  { get; set; }
        public string Kode_AO { get; set; }
        public string Kode_Referral { get; set; }
        public DateTime? Created_Date { get; set; }
        public DateTime? Updated_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<KodeReferance> db()
        {
            return cx().KodeReferance;
        }

        public static KodeReferance Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }

        public static KodeReferance FindKodeAO(string Kode_AO){
            return db().Where(x=>x.Kode_AO==Kode_AO).FirstOrDefault();
        }
        public static KodeReferance FindKodeReferance(string Kode_Referral){
            return db().Where(x=>x.Kode_Referral==Kode_Referral).FirstOrDefault();
        }
        
        public static Boolean Insert(KodeReferance fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.KodeReferance.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(KodeReferance fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.KodeReferance.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(KodeReferance row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.KodeReferance.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                id=this.id,
                Name=this.Name,
                Title=this.Title,
                KTP=this.KTP,
                Reporting=this.Reporting,
                Kode_AO = this.Kode_AO,
                Kode_Referral=this.Kode_Referral,
                Created_Date=this.Created_Date,
                Updated_Date=this.Updated_Date
            });
        }
    }
}