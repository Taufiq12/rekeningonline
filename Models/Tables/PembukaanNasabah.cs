using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class PembukaanNasabah
    {
        [Key]
        public string agama { get; set; }
        public string alamatEmail { get; set; }
        public string alamatID1 { get; set; }
        public string alamatID2 { get; set; }
        public int anak { get; set; }
        public string apuDataProfileResiko { get; set; }
        public string apuIdentitasNasabah { get; set; }
        public string apuInformasiLain1 { get; set; }
        public string apuJumlahTransaksi { get; set; }
        public string apuKegiatanUsaha { get; set; }
        public string apuLokasiUsaha { get; set; }
        public string apuProfilNasabah { get; set; }
        public string apuResumeAkhir { get; set; }
        public string apuStrukturKepemilikan { get; set; }
        public string bidangUsaha { get; set; }
        public string golonganPemilik { get; set; }
        public int istri { get; set; }
        public string jenisIdentitas { get; set; }
        public string jenisKelamin { get; set; }
        public string jenisNasabah { get; set; }
        public string jenisPekerjaan { get; set; }
        public int jumlahTanggungan { get; set; }
        public string kecamatan { get; set; }
        public string kelurahan { get; set; }
        public string kodeAO { get; set; }
        public string kodeCabang { get; set; }
        public string kodeNomorTelepon { get; set; }
        public string kodePos { get; set; }
        public string kota { get; set; }
        public string lamaBekerja { get; set; }
        public string namaBadanUsaha { get; set; }
        public string namaIbuKandung { get; set; }
        public string namaNasabah { get; set; }
        public string negaraAsal { get; set; }
        public string noID { get; set; }
        public string nomorHP { get; set; }
        public string nomorNasabah { get; set; }
        public string nomorTelepon { get; set; }
        public string npwp { get; set; }
        public string pendidikanTerakhir { get; set; }
        public string penghasilanPerBulan { get; set; }
        public string propinsi { get; set; }
        public string request { get; set; }
        public string response { get; set; }
        public string statusPerkawinan { get; set; }
        public string sumberPenghasilan { get; set; }
        public DateTime tanggalBerakhirID { get; set; }
        public DateTime tanggalLahir { get; set; }
        public DateTime tanggalTerbitID { get; set; }
        public string tempatLahir { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<PembukaanNasabah> db()
        {
            return cx().PembukaanNasabah;
        }
        public static PembukaanNasabah Find(string namaNasabah, string noID, string namaIbuKandung,DateTime tanggalLahir, string tempatLahir){
            return db().Where(x=>x.namaNasabah==namaNasabah && x.noID == noID && x.namaIbuKandung == namaIbuKandung && x.tanggalLahir == tanggalLahir && x.tempatLahir == tempatLahir).FirstOrDefault();
        }
        
        public static Boolean Insert(PembukaanNasabah fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PembukaanNasabah.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(PembukaanNasabah fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PembukaanNasabah.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(PembukaanNasabah row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PembukaanNasabah.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                agama = this.agama,
                alamatEmail = this.alamatEmail,
                alamatID1 = this.alamatID1,
                alamatID2 = this.alamatID2,
                anak = this.anak,
                apuDataProfileResiko = this.apuDataProfileResiko,
                apuIdentitasNasabah = this.apuIdentitasNasabah,
                apuInformasiLain1 = this.apuInformasiLain1,
                apuJumlahTransaksi = this.apuJumlahTransaksi,
                apuKegiatanUsaha = this.apuKegiatanUsaha,
                apuLokasiUsaha = this.apuLokasiUsaha,
                apuProfilNasabah = this.apuProfilNasabah,
                apuResumeAkhir = this.apuResumeAkhir,
                apuStrukturKepemilikan = this.apuStrukturKepemilikan,
                bidangUsaha = this.bidangUsaha,
                golonganPemilik = this.golonganPemilik,
                istri = this.istri,
                jenisIdentitas = this.jenisIdentitas,
                jenisKelamin = this.jenisKelamin,
                jenisNasabah = this.jenisNasabah,
                jenisPekerjaan = this.jenisPekerjaan,
                jumlahTanggungan = this.jumlahTanggungan,
                kecamatan = this.kecamatan,
                kelurahan = this.kelurahan,
                kodeAO = this.kodeAO,
                kodeCabang = this.kodeCabang,
                kodeNomorTelepon = this.kodeNomorTelepon,
                kodePos = this.kodePos,
                kota = this.kota,
                lamaBekerja = this.lamaBekerja,
                namaBadanUsaha = this.namaBadanUsaha,
                namaIbuKandung = this.namaIbuKandung,
                namaNasabah = this.namaNasabah,
                negaraAsal = this.negaraAsal,
                noID = this.noID,
                nomorHP = this.nomorHP,
                nomorNasabah = this.nomorNasabah,
                nomorTelepon = this.nomorTelepon,
                npwp = this.npwp,
                pendidikanTerakhir = this.pendidikanTerakhir,
                penghasilanPerBulan = this.penghasilanPerBulan,
                propinsi = this.propinsi,
                request = this.request,
                response = this.response,
                statusPerkawinan = this.statusPerkawinan,
                sumberPenghasilan = this.sumberPenghasilan,
                tanggalBerakhirID = this.tanggalBerakhirID,
                tanggalLahir = this.tanggalLahir,
                tanggalTerbitID = this.tanggalTerbitID,
                tempatLahir = this.tempatLahir
            });
        }
    }
}