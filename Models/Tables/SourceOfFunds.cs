using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class SourceOfFunds
    {
        [Key]
        public int id { get; set; }
        public string SourceOfFunds_Desc { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<SourceOfFunds> db()
        {
            return cx().SourceOfFunds;
        }
        public static SourceOfFunds Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        
        public static Boolean Insert(SourceOfFunds fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.SourceOfFunds.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(SourceOfFunds fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.SourceOfFunds.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(SourceOfFunds row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.SourceOfFunds.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                SourceOfFunds_Desc=this.SourceOfFunds_Desc,
            });
        }
    }
}