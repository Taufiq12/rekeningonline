using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class CekSaldo
    {
        [Key]
        public int id { get; set; }
        public string cif { get; set; }
        public string accountNumber { get; set; }
        public string accountName { get; set; }
        public string accountStatus { get; set; }
        public string accountType { get; set; }
        public string productType { get; set; }
        public string accountCurrency { get; set; }
        public string branch { get; set; }
        public decimal plafond { get; set; }
        public string plafondSign { get; set; }
        public decimal blockedBalance { get; set; }
        public string blockedBalanceSign { get; set; }
        public decimal minimumBalance { get; set; }
        public string minimumBalanceSign { get; set; }
        public decimal clearingSettlement { get; set; }
        public string clearingSettlementSign { get; set; }
        public decimal availableBalance { get; set; }
        public string availableSign { get; set; }
        public decimal ledgerBalance { get; set; }
        public string ledgerBalanceSign { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<CekSaldo> db()
        {
            return cx().CekSaldo;
        }
        public static CekSaldo Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        
        public static Boolean Insert(CekSaldo fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.CekSaldo.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(CekSaldo fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.CekSaldo.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(CekSaldo row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.CekSaldo.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

         public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                cif = this.cif,
                accountNumber = this.accountNumber,
                accountName = this.accountName,
                accountStatus = this.accountStatus,
                accountType = this.accountType,
                productType = this.productType,
                accountCurrency = this.accountCurrency,
                branch = this.branch,
                plafond = this.plafond,
                plafondSign = this.plafondSign,
                blockedBalance = this.blockedBalance,
                blockedBalanceSign = this.blockedBalanceSign,
                minimumBalance = this.minimumBalance,
                minimumBalanceSign = this.minimumBalanceSign,
                clearingSettlement = this.clearingSettlement,
                clearingSettlementSign = this.clearingSettlementSign,
                availableBalance = this.availableBalance,
                availableSign = this.availableSign,
                ledgerBalance = this.ledgerBalance,
                ledgerBalanceSign = this.ledgerBalanceSign
            });
        }
    }
}