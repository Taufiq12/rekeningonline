using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Tables
{
    public class PasPay_VA
    {
        [Key]
		public int Id { get; set; }
		public string Mail { get; set; }
        public string MobileNumber { get; set; }
        public string VA_Number { get; set; }
        public string Status { get; set; }
		public DateTime? Updated_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static IQueryable<PasPay_VA> db(){
            return cx().PasPay_VA;
        }
        public static PasPay_VA Find(string Mail){
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }

        public static Boolean Insert(PasPay_VA fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PasPay_VA.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(PasPay_VA fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PasPay_VA.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(PasPay_VA row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PasPay_VA.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.Id,
            });
        }
    }
}