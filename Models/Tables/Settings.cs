using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Settings
    {
        [Key]
        public int s_id { get; set; }
        public string s_slug { get; set; }
        public string s_name { get; set; }
        public string s_type { get; set; }
        public string s_values { get; set; }
        public string s_note { get; set; }
        public string s_updated_by { get; set; }
        public DateTime? s_updated_at { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Settings> db(){
            return cx().Settings;
        }
        public static int lastId(){
            var last = db().OrderByDescending(x=>x.s_id).FirstOrDefault();
            return last == null ? 0 : last.s_id;
        }
        public static Settings Find(int id){
            return db().Where(x=>x.s_id==id).FirstOrDefault();
        }
        public static Settings Find(string slug){
            return db().Where(x=>x.s_slug==slug).FirstOrDefault();
        }
        public static Boolean Insert(Settings fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Settings.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Settings fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Settings.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(Settings row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Settings.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.s_id,
				s_values = values(),
            });
        }
        public dynamic values(){
            return JsonConvert.DeserializeObject(this.s_values!=null?this.s_values:"[]");
        }
    }
}