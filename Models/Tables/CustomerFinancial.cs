using System.Globalization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using Newtonsoft.Json.Linq;
using ProjectBYB.Models.Views;
using System.Collections.Generic;
using System.IO;


namespace ProjectBYB.Models.Tables
{
    public class CustomerFinancial
    {
        [Key]
        public int id { get; set; }
        public string cif { get; set; }
        public string Mail { get; set; }
        public string SourceOfIncome { get; set; }
        public string TotalIncome { get; set; }
        public string SourceOfFunds { get; set; }
        public string PurposeOfOpeningAccount { get; set; }
        public Boolean? AnotherAccount { get; set; }
        public string BankName { get; set; }
        public DateTime? Created_Date { get; set; }

        public static BYB cx()
        {
            return new BYB();
        }
        public static DbSet<CustomerFinancial> db()
        {
            return cx().CustomerFinancial;
        }
        public static CustomerFinancial Find(string Mail)
        {
            return db().Where(x => x.Mail == Mail).FirstOrDefault();
        }

        public CustomerFinancial cusFan(){
            var row = CustomerFinancial.Find(Mail);
            if(row==null) row = CustomerFinancial.Find(Mail);
            return row == null ? new CustomerFinancial() : row;
        }

        public void SaveStringFinancial(){
            var cus = this.cusFan();
            var row = this;

            var first = CustomerFinancial.db().Where(x=>x.Mail==Mail).FirstOrDefault();

            if(row.cif!=null) row.cif = row.cif;
            else if(cus.cif!=null) row.cif = cus.cif;

            if(row.Mail!=null) row.Mail = row.Mail;
            else if(cus.Mail!=null) row.Mail = cus.Mail;

            if(row.SourceOfIncome!=null) row.SourceOfIncome = row.SourceOfIncome;
            else if(cus.SourceOfIncome!=null) row.SourceOfIncome = cus.SourceOfIncome;

            if(row.TotalIncome!=null) row.TotalIncome = row.TotalIncome;
            else if(cus.TotalIncome!=null) row.TotalIncome = cus.TotalIncome;
            
            if(row.SourceOfFunds!=null) row.SourceOfFunds = row.SourceOfFunds;
            else if(cus.SourceOfFunds!=null) row.SourceOfFunds = cus.SourceOfFunds;
            
            if(row.PurposeOfOpeningAccount!=null) row.PurposeOfOpeningAccount = row.PurposeOfOpeningAccount;
            else if(cus.PurposeOfOpeningAccount!=null) row.PurposeOfOpeningAccount = cus.PurposeOfOpeningAccount;
            
            if(row.AnotherAccount!=null) row.AnotherAccount = row.AnotherAccount;
            else if(cus.AnotherAccount!=null) row.AnotherAccount = cus.AnotherAccount;
            
            if(row.BankName!=null) row.BankName = row.BankName;
            else if(cus.BankName!=null) row.BankName = cus.BankName;

            if(first==null){
				 CustomerFinancial.Insert(row);
			}else{
				 CustomerFinancial.Update(row);
			}
        }

        public static Boolean Insert(CustomerFinancial fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerFinancial.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(CustomerFinancial fill)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerFinancial.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(CustomerFinancial row)
        {
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction())
            {
                try
                {
                    cxt.CustomerFinancial.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}