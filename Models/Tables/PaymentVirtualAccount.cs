using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class PaymentVirtualAccount
    {
        [Key]
        public string payment_id { get; set; }
        public string callback_virtual_account_id { get; set; }
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string account_number { get; set; }
        public string bank_code { get; set; }
        public decimal amount { get; set; }
        public int merchant_code { get; set; }
        public string id { get; set; }
        public DateTime transaction_timestamp { get; set; }
        public DateTime updated { get; set; }
        public DateTime created { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<PaymentVirtualAccount> db()
        {
            return cx().PaymentVirtualAccount;
        }
        public static PaymentVirtualAccount Find(string external_id, string owner_id){
            return db().Where(x=>x.external_id==external_id && x.owner_id == owner_id).FirstOrDefault();
        }
        
        public static Boolean Insert(PaymentVirtualAccount fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PaymentVirtualAccount.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(PaymentVirtualAccount fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PaymentVirtualAccount.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(PaymentVirtualAccount row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PaymentVirtualAccount.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                payment_id = this.payment_id,
                callback_virtual_account_id = this.callback_virtual_account_id,
                owner_id = this.owner_id,
                external_id = this.external_id,
                account_number = this.account_number,
                bank_code = this.bank_code,
                amount = this.amount,
                merchant_code = this.merchant_code,
                id = this.id
            });
        }
    }
}