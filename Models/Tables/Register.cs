using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Register
    {
        [Key]
        public int id { get; set; }
        public string Mail { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string MobileNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string MotherName { get; set; }
        public int AccessLevel_Id { get; set; }
        public string Identity { get; set; }
        public string TypeIdentity { get; set; }
        public string Kode_Referance { get; set; }
        public int isActive { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Register> db()
        {
            return cx().Register;
        }
        public static Register Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        public static Register Find(string Mail){
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }

        public static Register FindBackOffice(int id){
            return db().Where(x=>x.AccessLevel_Id==id).FirstOrDefault();
        }
        public static Register FindMail(string Mail){
            if(Mail==null) return null;
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }
        
        public Register info(){
            var row = Register.Find(id);
            return row == null ? new Register() : row;
        }

        // public JObject GetData(){
        //     var row = Gen.Combine(this, new {
        //         Mail = this.Mail,
        //         FullName = this.FullName,
        //         NickName = this.NickName
        //     });
        //     return Gen.Combine(row, info());
        // }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                Mail = this.Mail,
                FullName = this.FullName,
                NickName = this.NickName,
                Password = Encrypt.Decrypt(this.Password)
            });
        }

        public static Boolean Insert(Register fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Register.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Register fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Register.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Register row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Register.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}