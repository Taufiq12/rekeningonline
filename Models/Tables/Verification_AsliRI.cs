using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Verification_AsliRI
    {
        [Key]
        public string Nik { get; set; }
        public string Name { get; set; }
        public string birthdate { get; set; }
        public string birthplace { get; set; }
        public string address { get; set; }
        public string addressasliri { get; set; }
        public string identity_photo { get; set; }
        public string selfie_photo { get; set; }
        public string status { get; set; }
        public DateTime? Created_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Verification_AsliRI> db()
        {
            return cx().Verification_AsliRI;
        }
        public static Verification_AsliRI Find(string Nik){
            return db().Where(x=>x.Nik==Nik).FirstOrDefault();
        }
        
        public static Boolean Insert(Verification_AsliRI fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Verification_AsliRI.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Verification_AsliRI fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Verification_AsliRI.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Verification_AsliRI row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Verification_AsliRI.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Nik = this.Nik,
                Name=this.Name,
                birthdate=this.birthdate,
                birthplace=this.birthplace,
                address=this.address,
                addressasliri=this.addressasliri,
                identity_photo=this.identity_photo,
                status=this.status
            });
        }
    }
}