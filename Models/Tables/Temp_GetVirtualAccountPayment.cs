using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Temp_GetVirtualAccountPayment
    {
        [Key]
        public string id { get; set; }
        public string payment_id { get; set; }
        public string callback_virtual_account_id { get; set; }
        public string external_id { get; set; }
        public int merchant_code { get; set; }
        public string account_number { get; set; }
        public string bank_code { get; set; }
        public decimal amount { get; set; }
        public DateTime transaction_timestamp { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Temp_GetVirtualAccountPayment> db()
        {
            return cx().Temp_GetVirtualAccountPayment;
        }
        public static Temp_GetVirtualAccountPayment Find(string external_id){
            return db().Where(x=>x.external_id==external_id).FirstOrDefault();
        }
        
        public static Boolean Insert(Temp_GetVirtualAccountPayment fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Temp_GetVirtualAccountPayment.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Temp_GetVirtualAccountPayment fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Temp_GetVirtualAccountPayment.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Temp_GetVirtualAccountPayment row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Temp_GetVirtualAccountPayment.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.id,
                payment_id = this.payment_id,
                callback_virtual_account_id = this.callback_virtual_account_id,
                external_id = this.external_id,
                merchant_code = this.merchant_code,
                account_number = this.account_number,
                bank_code = this.bank_code,
                amount = this.amount,
                transaction_timestamp = this.transaction_timestamp
            });
        }
    }
}
