using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Confrim_Transfer
    {
        [Key]
        public string Kode_Cabang { get; set; }
        public string Tf_Number { get; set; }
        public string Mail { get; set; }
        public string FullName { get; set; }
        public int Status { get; set; }
        public DateTime Created_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Confrim_Transfer> db()
        {
            return cx().Confrim_Transfer;
        }
        public static Confrim_Transfer Find(string Tf_Number){
            return db().Where(x=>x.Tf_Number==Tf_Number).FirstOrDefault();
        }
        
        public static Boolean Insert(Confrim_Transfer fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Confrim_Transfer.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Confrim_Transfer fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Confrim_Transfer.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Confrim_Transfer row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Confrim_Transfer.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

         public JObject GetData(){
            return Gen.Combine(this, new {
                Tf_Number = this.Tf_Number,
                Mail = this.Mail,
                FullName=this.FullName,
                Status=this.Status,
                Created_Date=this.Created_Date
            });
        }

        //Generate Unique Number
        public static string TransNumber(string KodeCabang){
			
            var aoi = Confrim_Transfer.db().Where(x=>x.Kode_Cabang==KodeCabang && x.Created_Date.Year == DateTime.Now.Year && x.Created_Date.Month == DateTime.Now.Month && x.Created_Date.Day == DateTime.Now.Day).OrderByDescending(x=>x.Tf_Number).FirstOrDefault();
            var formatNumber = KodeCabang+"-"+DateTime.Now.Year+"-"+DateTime.Now.Month+"-"+DateTime.Now.Day;
			if(aoi==null) return formatNumber+"-0001";
			var number = int.Parse(aoi.Tf_Number.Split("-")[4]);
            return formatNumber+"-"+(number+1).ToString("00000");
        }
    }
}