using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace ProjectBYB.Models.Tables
{
    public class EndOfDay
    {
        
        [Key]
        public int id { get; set; }
        public Boolean is_eod { get; set; }
        public DateTime eod_Date { get; set; }
        
        public static BYB cx(){
            return new BYB();
        }

        public static DbSet<EndOfDay> db(){
            return cx().EndOfDay;
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.id,
                is_eod = this.is_eod,
                eod_Date = this.eod_Date
            });
        }
    }
}