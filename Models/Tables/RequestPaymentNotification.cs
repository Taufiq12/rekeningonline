using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Tables
{
    public class RequestPaymentNotification
    {
        [Key]
        public int Id { get; set; }
        public string request { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public string payment_reff { get; set; }
        public DateTime payment_date { get; set; }
        public string payment_status_code { get; set; }
        public string payment_status_desc { get; set; }
        public decimal bill_total { get; set; }
        public decimal payment_total { get; set; }
        public int payment_channel_uid { get; set; }
        public string payment_channel { get; set; }
        public string signature { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<RequestPaymentNotification> db()
        {
            return cx().RequestPaymentNotification;
        }
        public static RequestPaymentNotification Find(int Id){
            return db().Where(x=>x.Id==Id).FirstOrDefault();
        }
        
        public static Boolean Insert(RequestPaymentNotification fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.RequestPaymentNotification.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(RequestPaymentNotification fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.RequestPaymentNotification.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(RequestPaymentNotification row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.RequestPaymentNotification.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                request = this.request,
                trx_id = this.trx_id,
                merchant_id = this.merchant_id,
                merchant = this.merchant,
                bill_no = this.bill_no,
                payment_reff = this.payment_reff,
                payment_date = this.payment_date,
                payment_status_code = this.payment_status_code,
                payment_status_desc = this.payment_status_desc,
                bill_total = this.bill_total,
                payment_total = this.payment_total,
                payment_channel_uid = this.payment_channel_uid,
                payment_channel = this.payment_channel,
                signature = this.signature
            });
        }
    }
}