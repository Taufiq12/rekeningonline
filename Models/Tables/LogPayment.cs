using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProjectBYB.Models.Tables
{
    public class LogPayment
    {
        [Key]
        public int No { get; set; }
        public string id { get; set; }
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string status { get; set; }
        public DateTime created { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<LogPayment> db()
        {
            return cx().LogPayment;
        }
        public static LogPayment Find(string external_id, string owner_id){
            return db().Where(x=>x.external_id==external_id && x.owner_id == owner_id).FirstOrDefault();
        }
        
        public static Boolean Insert(LogPayment fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LogPayment.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(LogPayment fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LogPayment.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(LogPayment row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.LogPayment.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public LogPayment aiData;
        public LogPayment ai(){
            if(aiData!=null) return aiData;
            aiData = LogPayment.Find(external_id,owner_id);
            return aiData == null ? new LogPayment() : aiData;
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                No = this.No,
                id = this.id,
                owner_id = this.owner_id,
                external_id = this.external_id,
                status = this.status
            });
        }

    }
}