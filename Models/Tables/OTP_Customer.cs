using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class OTP_Customer
    {
        [Key]
        public int Id { get; set; }
        public string Mail { get; set; }
        public string FullName { get; set; }
        public string OTP { get; set; }
        public DateTime ctime_Stamp { get; set; }
        public int Status { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<OTP_Customer> db()
        {
            return cx().OTP_Customer;
        }
        public static OTP_Customer Find(string Mail){
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }
        
        public static Boolean Insert(OTP_Customer fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.OTP_Customer.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(OTP_Customer fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.OTP_Customer.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(OTP_Customer row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.OTP_Customer.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Mail = this.Mail,
                FullName = this.FullName,
                OTP =this.OTP,
                ctime_Stamp = this.ctime_Stamp,
                Status = this.Status
            });
        }
    }
}