using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class UserVirtualAccount
    {
        [Key]
        public string id { get; set; }
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string Mail { get; set; }
        public string bank_code { get; set; }
        public int merchant_code { get; set ; }
        public string name { get; set; }
        public string account_number { get; set; }
        public Boolean is_closed { get; set; }
        public Boolean is_single_use { get; set; }
        public string status { get; set; }
        public DateTime? expiration_date { get; set; }
        public decimal suggested_amount { get; set; }
        public DateTime? updated { get; set; }
        public DateTime? created { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<UserVirtualAccount> db()
        {
            return cx().UserVirtualAccount;
        }
        public static UserVirtualAccount FindMobile(string external_id){
            return db().Where(x=>x.external_id==external_id).FirstOrDefault();
        }

        public static UserVirtualAccount FindID(string id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        public static UserVirtualAccount Find(string external_id, string owner_id){
            return db().Where(x=>x.external_id==external_id && x.owner_id == owner_id).FirstOrDefault();
        }

        public static UserVirtualAccount FindUserAccount(string id,string owner_id,string external_id,string name,string bank_code){
            return db().Where(x=>x.id == id && x.owner_id == owner_id && x.external_id == external_id && x.name == name && x.bank_code == bank_code).FirstOrDefault();
        }
        
        public static Boolean Insert(UserVirtualAccount fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.UserVirtualAccount.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(UserVirtualAccount fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.UserVirtualAccount.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(UserVirtualAccount row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.UserVirtualAccount.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                is_closed = this.is_closed,
                id = this.id,
                owner_id = this.owner_id,
                external_id = this.external_id, 
                Mail = this.Mail,
                bank_code = this.bank_code,
                merchant_code = this.merchant_code, 
                name = this.name, 
                account_number = this.account_number,
                is_single_use = this.is_single_use
            });
        }
    }
}