using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class RequestVirtualAccountFasPay
    {
        [Key]
        public int Id { get; set; }
        public string Request { get; set; }
        public string Merchant_id { get; set; }
        public string Merchant { get; set; }
        public string bill_no { get; set; }
        public string bill_reff { get; set; }
        public string bill_date { get; set; }
        public string bill_expired { get; set; }
        public string bill_desc { get; set; }
        public string bill_currency { get; set; }
        public decimal bill_total { get; set; }
        public string payment_channel { get; set; }
        public string pay_type { get; set; }
        public string cust_no { get; set; }
        public string cust_name { get; set; }
        public string Msisdn { get; set; }
        public string Email { get; set; }
        public int Terminal { get; set; }
        public string Product { get; set; }
        public Decimal Amount { get; set; }
        public int Qty { get; set; }
        public int payment_plan { get; set; }
        public string tenor { get; set; }
        public int merchant_id_item { get; set; }
        public string Signature { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<RequestVirtualAccountFasPay> db()
        {
            return cx().RequestVirtualAccountFasPay;
        }
        public static RequestVirtualAccountFasPay Find(int Id){
            return db().Where(x=>x.Id==Id).FirstOrDefault();
        }
        
        public static Boolean Insert(RequestVirtualAccountFasPay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.RequestVirtualAccountFasPay.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(RequestVirtualAccountFasPay fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.RequestVirtualAccountFasPay.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(RequestVirtualAccountFasPay row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.RequestVirtualAccountFasPay.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Request = this.Request,
                Merchant_id = this.Merchant_id,
                Merchant = this.Merchant,
                bill_no = this.bill_no,
                bill_reff = this.bill_reff,
                bill_date = this.bill_date,
                bill_expired = this.bill_expired,
                bill_desc = this.bill_desc,
                bill_currency = this.bill_currency,
                bill_total = this.bill_total,
                payment_channel = this.payment_channel,
                pay_type = this.pay_type,
                cust_no = this.cust_no,
                cust_name = this.cust_name,
                Msisdn = this.Msisdn,
                Email = this.Email,
                Terminal = this.Terminal,
                Product = this.Product,
                Amount = this.Amount,
                Qty = this.Qty,
                payment_plan = this.payment_plan,
                tenor = this.tenor,
                merchant_id = this.merchant_id_item,
                Signature = this.Signature
            });
        }
    }
}