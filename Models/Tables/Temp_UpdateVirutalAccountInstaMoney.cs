using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class Temp_UpdateVirutalAccountInstaMoney
    {
        [Key]
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string bank_code { get; set; }
        public int merchant_code { get; set; }
        public string name { get; set; }
        public string account_number { get; set; }
        public decimal suggested_amount { get; set; }
        public Boolean is_closed {get; set; }
        public string id { get; set; }
        public Boolean is_single_use { get; set; }
        public string status { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Temp_UpdateVirutalAccountInstaMoney> db()
        {
            return cx().Temp_UpdateVirutalAccountInstaMoney;
        }
        public static Temp_UpdateVirutalAccountInstaMoney Find(string external_id){
            return db().Where(x=>x.external_id==external_id).FirstOrDefault();
        }
        
        public static Boolean Insert(Temp_UpdateVirutalAccountInstaMoney fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Temp_UpdateVirutalAccountInstaMoney.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Temp_UpdateVirutalAccountInstaMoney fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Temp_UpdateVirutalAccountInstaMoney.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(Temp_UpdateVirutalAccountInstaMoney row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Temp_UpdateVirutalAccountInstaMoney.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                owner_id = this.owner_id,
                external_id = this.external_id, 
                bank_code = this.bank_code,
                merchant_code = this.merchant_code, 
                name = this.name, 
                account_number = this.account_number,
                is_closed = this.is_closed,
                id = this.id,
                is_single_use = this.is_single_use
            });
        }
    }
}