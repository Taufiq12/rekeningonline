using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using Newtonsoft.Json.Linq;
using ProjectBYB.Models.Views;

namespace ProjectBYB.Models.Tables
{
    public class AccessLevel
    {
		[Key]
		public int al_id { get; set; }
		public string al_name { get; set; }
		public string al_access_menu { get; set; }
		public string al_access_module { get; set; }
		public string al_created_by { get; set; }
		public DateTime? al_created_at { get; set; }
		public string al_updated_by { get; set; }
		public DateTime? al_updated_at { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<AccessLevel> db(){
            return cx().AccessLevel;
        }
        public static int lastId(){
            var last = db().OrderByDescending(x=>x.al_id).FirstOrDefault();
            return last == null ? 0 : last.al_id;
        }
        public static AccessLevel Find(int id){
            return db().Where(x=>x.al_id==id).FirstOrDefault();
        }
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.al_id,
                al_access_menu = this.al_access_menu!=null?this.al_access_menu.Split(","):new string[0],
                al_access_module = this.al_access_module!=null?this.al_access_module.Split(","):new string[0],
            });
        }
        public static Boolean Insert(AccessLevel fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.AccessLevel.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(AccessLevel fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.AccessLevel.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception){
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(AccessLevel row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.AccessLevel.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception){
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}