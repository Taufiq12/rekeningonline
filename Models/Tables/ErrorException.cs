using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Tables
{
  	public class ErrorException
    {
        [Key]
        public int Id { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string InnerSource { get; set; }
        public string InnerMessage { get; set; }
        public string InnerStackTrace { get; set; }
        public string HelpLink { get; set; }
        public DateTime ErrorAt { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<ErrorException> db(){
            return cx().ErrorException;
        }
        public static Boolean Logs(Exception e){
            var ctx = cx();
            using (var transaction = ctx.Database.BeginTransaction()){
                try{
                    var fill = new ErrorException();
                    fill.Source = e.Source;
                    fill.Message = e.Message;
                    fill.StackTrace = e.StackTrace;
                    fill.InnerSource = e.InnerException != null ? e.InnerException.Source : null;
                    fill.InnerMessage = e.InnerException != null ? e.InnerException.Message : null;
                    fill.InnerStackTrace = e.InnerException != null ? e.InnerException.StackTrace : null;
                    fill.HelpLink = e.HelpLink;
                    fill.ErrorAt = DateTime.Now;
                    ctx.ErrorException.Add(fill);
                    ctx.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch{
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}