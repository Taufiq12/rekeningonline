using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Tables
{
  public class Template
    {
        [Key]
		public int Mt_Id { get; set; }
		public int Mt_Type { get; set; }
		public string Mt_Slug { get; set; }
		public string Mt_Title { get; set; }
		public string Mt_Template { get; set; }
		public DateTime? Mt_Updated_Date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<Template> db(){
            return cx().Template;
        }
        public static int lastId(){
            var last = db().OrderByDescending(x=>x.Mt_Id).FirstOrDefault();
            return last == null ? 0 : last.Mt_Id;
        }
        public static Template Find(int id){
            return db().Where(x=>x.Mt_Id==id).FirstOrDefault();
        }
        public static Template Find(string slug){
            return db().Where(x=>x.Mt_Slug==slug).FirstOrDefault();
        }
        public static Boolean Insert(Template fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Template.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(Template fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Template.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Remove(Template row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.Template.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.Mt_Id,
            });
        }
    }
}