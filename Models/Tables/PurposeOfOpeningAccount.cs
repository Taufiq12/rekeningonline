using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Tables
{
    public class PurposeOfOpeningAccount
    {
        [Key]
        public int id { get; set; }
        public string PurposeOfOpeningAccount_Desc { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<PurposeOfOpeningAccount> db()
        {
            return cx().PurposeOfOpeningAccount;
        }
        public static PurposeOfOpeningAccount Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        
        public static Boolean Insert(PurposeOfOpeningAccount fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PurposeOfOpeningAccount.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(PurposeOfOpeningAccount fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PurposeOfOpeningAccount.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(PurposeOfOpeningAccount row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.PurposeOfOpeningAccount.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                PurposeOfOpeningAccount_Desc=this.PurposeOfOpeningAccount_Desc,
            });
        }
    }
}