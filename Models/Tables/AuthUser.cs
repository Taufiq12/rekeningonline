using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using NETCore.Encrypt;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

namespace ProjectBYB.Models.Tables
{
    public class AuthUser
    {
        string token;

        [Key]
        public int au_id { get; set; }
        public string au_email { get; set; }
        public string au_useragent { get; set; }
        public string au_token { get; set; }
        public DateTime? au_valid_until { get; set; }
        public DateTime? au_created_at { get; set; }

        public AuthUser() { }

        public AuthUser(string tk)
        {
            var cx = new BYB();
            token = tk;
        }

        public static AuthUser Find(string Mail){
            var cx = new BYB();
            return cx.AuthUser.Where(x => x.au_email == Mail).FirstOrDefault();
        }

        public AuthUser authData(){
            var cx = new BYB();
            return cx.AuthUser.Where(x=>x.au_token==token).FirstOrDefault();
        }
        public vCustomer authProfile(){
            var authCustomer = this.authData();
            return vCustomer.Find(authCustomer.au_email);
        }


        public static string GenerateToken(string Mail, string useragent){
            var cx = new BYB();
            var token = EncryptProvider.Sha1(DateTime.Now.ToString() + Guid.NewGuid().ToString("N"));
            var authCustomer = cx.AuthUser.Where(x => x.au_email==Mail).FirstOrDefault();
            if(authCustomer != null){
                authCustomer.au_token = token;
                authCustomer.au_useragent = useragent;
                authCustomer.au_created_at = DateTime.Now;
                cx.AuthUser.Update(authCustomer);
                cx.SaveChanges();
            }else{
                authCustomer = new AuthUser();
                authCustomer.au_email = Mail.ToUpper();
                authCustomer.au_token = token;
                authCustomer.au_useragent = useragent;
                authCustomer.au_created_at = DateTime.Now;
                cx.AuthUser.Add(authCustomer);
                cx.SaveChanges();
            }

            return token;
        }
    }
}