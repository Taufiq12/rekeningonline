using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Microsoft.EntityFrameworkCore;

namespace ProjectBYB.Models
{    public class BYB : DbContext
    {
        public BYB() { }

        public BYB(DbContextOptions<BYB> options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //connection database
                optionsBuilder.UseSqlServer(Config.BYB);
                //optionsBuilder.UseNpgsql(Config.BYB);
                //optionsBuilder.UseMySQL(Config.BYB);
            }
        }
       
        // Tables

        public DbSet<AuthUser> AuthUser { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerFamily> CustomerFamily { get; set; }
        public DbSet<CustomerJobs> CustomerJobs { get; set; }
        public DbSet<CustomerFinancial> CustomerFinancial { get; set; }
        public DbSet<AccessCustomer> AccessCustomer { get; set; }
        public DbSet<AccessLevel> AccessLevel { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<ErrorException> ErrorException {get;set;}
        public DbSet<Register> Register {get;set;}
        public DbSet<Template> Template { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Identity> Identity { get; set; }
        public DbSet<Education> Education { get; set; }
        public DbSet<Religion> Religion {get; set; }
        public DbSet<MaterialStatus> MaterialStatus { get; set; }
        public DbSet<HomeStatus> HomeStatus { get; set; }
        public DbSet<Hobby> Hobby { get; set;}
        public DbSet<Jobs> Jobs { get; set; }
        public DbSet<SourceOfIncome> SourceOfIncome { get; set; }
        public DbSet<TotalIncome> TotalIncome { get; set; }
        public DbSet<SourceOfFunds> SourceOfFunds { get; set; }
        public DbSet<PurposeOfOpeningAccount> PurposeOfOpeningAccount { get; set; }
        public DbSet<PostalArea> PostalArea { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<ApprovalLogs> ApprovalLogs { get; set; }
        public DbSet<Attachment> Attachment { get; set; }
        public DbSet<CustomerTypeBook> CustomerTypeBook { get; set; }
        public DbSet<Saving_Type> Saving_Type { get; set; }
        public DbSet<PasPay_VA> PasPay_VA { get; set; }
        public DbSet<Verification_AsliRI> Verification_AsliRI { get; set; }
        public DbSet<Confrim_Transfer> Confrim_Transfer { get; set; }
        public DbSet<PembukaanNasabah> PembukaanNasabah { get; set; }
        public DbSet<PembukaanRekening> PembukaanRekening { get; set; }
        public DbSet<BadanUsaha> BadanUsaha { get; set; }
        public DbSet<KodeReferance> KodeReferance { get; set; }
        public DbSet<OTP_Customer> OTP_Customer { get; set; }
        public DbSet<LogActivity> LogActivity { get; set; }
        public DbSet<EndOfDay> EndOfDay { get; set; }
        public DbSet<LoginDukcapil> LoginDukcapil { get; set; }

        //Temp Log Virtual Account
        public DbSet<UserVirtualAccount> UserVirtualAccount { get; set; }
        public DbSet<PaymentVirtualAccount> PaymentVirtualAccount { get; set; }
        public DbSet<LogPayment> LogPayment { get; set; }
        public DbSet<Temp_UpdateVirutalAccountInstaMoney> Temp_UpdateVirutalAccountInstaMoney { get; set; }
        public DbSet<Temp_GetVirtualAccountPayment> Temp_GetVirtualAccountPayment { get; set; }
        public DbSet<Temp_GetVirtualAccountRequest> Temp_GetVirtualAccountRequest { get; set; }
        public DbSet<RequestVirtualAccountFasPay> RequestVirtualAccountFasPay { get; set; }
        public DbSet<UserVirtualAccountFasPay> UserVirtualAccountFasPay { get; set; }
        public DbSet<InquiryPaymentFaspay> InquiryPaymentFaspay { get; set; }
        public DbSet<CancelPaymentFaspay> CancelPaymentFaspay { get; set; }
        public DbSet<RequestPaymentNotification> RequestPaymentNotification { get; set; }
        public DbSet<ResponsePaymentNotification> ResponsePaymentNotification { get; set; }
        public DbSet<LevelApproval> LevelApproval { get; set; }
        public DbSet<CekSaldo> CekSaldo { get; set; }

        // Views
        public DbSet<vAuthUser> vAuthUser { get; set; }
        public DbSet<vCustomer> vCustomer { get; set; }
        public DbSet<vCustomerDetail> vCustomerDetail { get; set; }
        public DbSet<vInbox> vInbox { get; set; }
        public DbSet<vPaymentList> vPaymentList { get; set; }
        public DbSet<vCustomerCIF> vCustomerCIF { get; set; }
        public DbSet<vCustomerRekening> vCustomerRekening { get; set; }
        public DbSet<vDataRekeningNasabah> vDataRekeningNasabah { get; set; }
        public DbSet<vInquiryPaymentFaspay> vInquiryPaymentFaspay { get; set; }
        public DbSet<vHeaderInquiryPaymentFaspay> vHeaderInquiryPaymentFaspay { get; set; }
        public DbSet<vRoleAccess> vRoleAccess { get; set; }
        public DbSet<vCancelPayment> vCancelPayment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}