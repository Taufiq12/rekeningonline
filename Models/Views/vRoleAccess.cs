using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using ProjectBYB.Models.Tables;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Views
{
    public class vRoleAccess
    {
        [Key]
         public int id { get; set; }

        public string Mail { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public int al_id { get; set; }
        public string al_name { get; set; }
        public string al_access_menu { get; set; }
        public string al_access_module { get; set; }
        public string Password_Dukcapil { get; set; }

         public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vRoleAccess> db()
        {
            return cx().vRoleAccess;
        }
        public static vRoleAccess Find(int id){
            return db().Where(x=>x.id==id).FirstOrDefault();
        }
        public static vRoleAccess Findal_name(string al_name){
            return db().Where(x=>x.al_name==al_name).FirstOrDefault();
        }
        public static vRoleAccess Find(string Mail){
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }

        public static vRoleAccess FindBackOffice(int id){
            return db().Where(x=>x.al_id==id).FirstOrDefault();
        }
        public static vRoleAccess FindMail(string Mail){
            if(Mail==null) return null;
            return db().Where(x=>x.Mail==Mail).FirstOrDefault();
        }
        
        public vRoleAccess info(){
            var row = vRoleAccess.Find(id);
            return row == null ? new vRoleAccess() : row;
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                Id = this.id,
                Mail = this.Mail,
                FullName = this.FullName,
                NickName = this.NickName,
                Password = Encrypt.Decrypt(this.Password),
                al_id = this.al_id,
                al_name = this.al_name,
                al_access_menu = this.al_access_menu,
                al_access_module = this.al_access_module,
                Password_Dukcapil = this.Password_Dukcapil
            });
        }

        public static Boolean Insert(vRoleAccess fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.vRoleAccess.Add(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
        public static Boolean Update(vRoleAccess fill){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.vRoleAccess.Update(fill);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public static Boolean Remove(vRoleAccess row){
            var cxt = cx();
            using (var transaction = cxt.Database.BeginTransaction()){
                try{
                    cxt.vRoleAccess.Remove(row);
                    cxt.SaveChanges();
                    transaction.Commit();
                    return true;
                }catch (Exception e){
                    ErrorException.Logs(e);
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}