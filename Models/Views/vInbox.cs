using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Helpers;
using System.Collections.Generic;
using ProjectBYB.Models.Tables;

namespace ProjectBYB.Models.Views
{
    public class vInbox
    {
        [Key]
        public Int64 row_no { get; set; }
        public int Id { get; set; }
		public string Mail { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; } 
        public string TypeIdentity { get; set; } 
        public string Identity { get; set; }
        public string AddressIdentity { get; set; }
        public DateTime? Created_Date { get; set; }
        public string status { get; set; }
        public string statusType { get; set; }

        public static BYB cx(){
            return new BYB();
        }

        public static IQueryable<vInbox> db(){
            return cx().vInbox.FromSql("select ROW_NUMBER() OVER (ORDER BY id DESC) AS row_no, * from vInbox");
        }

        public static IQueryable<vInbox> dbCus(){
            return cx().vInbox.FromSql("select ROW_NUMBER() OVER (ORDER BY id DESC) AS row_no,Id,Mail,FullName,Title,TypeIdentity,[Identity],AddressIdentity,Created_Date,status ,CASE statusType WHEN 'BOADMIN' THEN '' WHEN 'BOSPV' THEN '' ELSE statusType END statusType from vInbox");
        }
        public static vInbox Find(int id){
            return db().Where(x=>x.Id==id).FirstOrDefault();
        }

        public static vInbox FindMail(string Mail)
        {
            return db().Where(x=>x.Mail == Mail).FirstOrDefault();
        }
        
        public vCustomerDetail toCus(string mail){
            var row = vCustomerDetail.Find(mail);
            return row == null ? new vCustomerDetail() : row;
        }

        public string toMail(){
            if(statusType!="CUS") return Mail;
            if(statusType=="BOADMIN") return vRoleAccess.Findal_name("Admin Back Office").Mail;
            if(statusType=="BOSPV") return vRoleAccess.Findal_name("SPV Back Office").Mail;
            return "";
        }

        public vCustomerDetail toEmp(){
            var row = vCustomerDetail.Find(toMail());
            return row == null ? new vCustomerDetail() : row;
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = Id,
                aa = toEmp()
            });
        }

        public int msg_id(){
            var row = Customer.db().Where(x=>x.Mail==Mail).Select(x=>new{x.msg_id}).FirstOrDefault();
            return row != null ? (row.msg_id != null ? row.msg_id.Value : 0) : 0;
        }
    }
}