using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ProjectBYB.Models.Views
{
    public class vCustomer
    {
        [Key]
        public int id { get; set; }
        public string Mail { get; set; }
        public string cif { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Title { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Identity { get; set; }
        public string MobileNumber { get; set; }
        public string MotherName { get; set; }
        public string al_name { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vCustomer> db(){
            return cx().vCustomer;
        }
        public static vCustomer Find(string Mail){
            return db().Where(x => x.Mail == Mail).FirstOrDefault();
        }
        
        public vAuthUser vuser(){
            return vAuthUser.Find(this.Mail);
        }
    }
}