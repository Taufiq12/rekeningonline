using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using ProjectBYB.Models.Tables;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Views
{
    public class vAuthUser
    {
        [Key]
        public int ae_id { get; set; }
        public string Mail { get; set; }
        public string cif { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Title { get; set; }
        public string PlaceOfBirth { get; set; }
        public string DateOfBirth { get; set; }
        public string MotherName { get; set; }
        public string Country { get; set; }
        public string CountryOther { get; set; }
        public string Gender { get; set; }
        public string al_name { get; set; }
        public string al_access_menu { get; set; }
        public string al_access_module { get; set; }
        public Boolean ae_status { get; set; }
        public string Identity { get; set; }
        public string TypeIdentity { get; set; }
        public string StartDateIdentity { get; set; }
        public string EndDateIdentity { get; set; }
        public string AddressIdentity { get; set; }
        public string RTIdentity { get; set; }
        public string RWIdentity { get; set; }
        public string KelIdentity { get; set; }
        public string KecIdentity { get; set; }
        public string KabIdentity { get; set; }
        public string ProvinceIdentity { get; set; }
        public string PostalCodeIdentity { get; set; }
        public string ResidenceAddress { get; set; }
        public string RTResidence { get; set; }
        public string RWResidence { get; set; }
        public string KelResidence { get; set; }
        public string KecResidence { get; set; }
        public string KabResidence { get; set; }
        public string ProvinceResidence { get; set; }
        public string PostalCodeResidence { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Education { get; set; }
        public string religion { get; set; }
        public string MaterialStatus { get; set; }
        public int NumberOfDependents { get; set; }
        public string HomeStatus { get; set; }
        public int LongOccupyYear { get; set; }
        public int LongOccupyMonth { get; set; }
        public string Hobby { get; set; }
        public string NPWP { get; set; }
        public int? msg_id { get; set; }
        public string FamilyFullName { get; set; }
        public string FamilyRelationshipWithTheApplicant { get; set; }
        public string FamilyPhoneNumber { get; set; }
        public string FamilyAddress { get; set; }
        public string FamilyRT { get; set; }
        public string FamilyRW { get; set; }
        public string FamilyKel { get; set; }
        public string FamilyKec { get; set; }
        public string FamilyKab { get; set; }
        public string FamilyProvince { get; set; }
        public string FamilyPostalCode { get; set; }
        public string SourceOfIncome { get; set; }
        public string TotalIncome { get; set; }
        public string SourceOfFunds { get; set; }
        public string PurposeOfOpeningAccount { get; set; }
        public Boolean? AnotherAccount { get; set; }
        public string BankName { get; set; }
        public string Jobs { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string BusinessField { get; set; }
        public string CompanyAddress { get; set; }
        public string RTCompany { get; set; }
        public string RWCompany { get; set; }
        public string KelCompany { get; set; }
        public string KecCompany { get; set; }
        public string KabCompany { get; set; }
        public string ProvinceCompany { get; set; }
        public string PostalCodeCompany { get; set; }
        public string PhoneNumberCompany { get; set; }
        public string FaxNumberCompany { get; set; }
        public string ExtNumberCompany1 { get; set; }
        public string ExtNumberCompany2 { get; set; }
        public int LengthOfWorkYear { get; set; }
        public int LengthOfWorkMonth { get; set; }
        public Boolean IsDomisili { get; set; }
        public string KodeCabang { get; set; }
        public string Attachment1 { get; set; }
        public string Attachment2 { get; set; }
        public string Attachment3 { get; set; }
        public string SavingType { get; set; }
        public string TypeCard { get; set; }

        public static BYB cx(){
            return new BYB();
        }

        public static DbSet<vAuthUser> db(){
            return cx().vAuthUser;
        }

        public static vAuthUser Findal_Name(string al_Name){
            return db().Where(x => x.al_name == al_Name).FirstOrDefault();
        }

        public static vAuthUser Find(string Mail){
            var row = db().Where(x => x.Mail == Mail).FirstOrDefault();
            if(row == null){
                var cus = vAuthUser.Find(Mail);
                if(cus==null) return null;
                row = new vAuthUser();
                row.Mail = cus.Mail;
                row.FullName = cus.FullName;
                row.NickName = cus.NickName;
                row.Title = cus.Title;
                row.PlaceOfBirth = cus.PlaceOfBirth;
                row.DateOfBirth = cus.DateOfBirth;
                var al = AccessLevel.Find(2);
                row.al_name = al.al_name;
                row.al_access_menu = al.al_access_menu;
                row.al_access_module = al.al_access_module;
                row.ae_status = true;
            }
            return row;
        }

        public JObject GetData(string type = ""){

            var cif = Customer.FindMail(Mail);

            if(cif != null)
            { 
                var cRek = from s in PembukaanRekening.db()
                    where s.nomorNasabah == cif.cif && s.namaNasabah == cif.FullName
                    select s;

                int total_ = cRek.Count();
                if(total_ > 0)
                {
                
                    var query = (from c in CekSaldo.db()
                                where (c.cif == cif.cif)
                                select new {c.accountNumber, c.cif, c.accountCurrency, c.availableBalance});

                    return Gen.Combine(this, new {
                        id = this.ae_id,
                        al_access_menu = this.al_access_menu!=null?this.al_access_menu.Split(","):new string[0],
                        al_access_module = this.al_access_module!=null?this.al_access_module.Split(","):new string[0],
                        status = this.ae_status,
                        cus = type != "nocus" ? cus().GetData() : new JObject(),
                        rek = query,
                    });
                }else{
                    return Gen.Combine(this, new {
                        id = this.ae_id,
                        al_access_menu = this.al_access_menu!=null?this.al_access_menu.Split(","):new string[0],
                        al_access_module = this.al_access_module!=null?this.al_access_module.Split(","):new string[0],
                        status = this.ae_status,
                        cus = type != "nocus" ? cus().GetData() : new JObject(),
                    });
                }
            }else{
                
                return Gen.Combine(this, new {
                        id = this.ae_id,
                        al_access_menu = this.al_access_menu!=null?this.al_access_menu.Split(","):new string[0],
                        al_access_module = this.al_access_module!=null?this.al_access_module.Split(","):new string[0],
                        status = this.ae_status,
                        cus = type != "nocus" ? cus().GetData() : new JObject(),
                    });
            }
        }
        public Boolean isMenu(string menu){
            if(this.al_access_menu==null) return false;
            return Array.IndexOf(this.al_access_menu.Split(","), menu) > -1;
        }
        public Boolean isMenu(string[] menu){
            var access = false;
            menu.ToList().ForEach(v=>{
                if(this.isMenu(v)) access = true;
            });
            return access;
        }

        public Boolean isModule(string module){
            if(this.al_access_module==null) return false;
            return Array.IndexOf(this.al_access_module.Split(","), module) > -1;
        }
        public Boolean isModule(string[] module){
            var access = false;
            module.ToList().ForEach(v=>{
                if(this.isModule(v)) access = true;
            });
            return access;
        }
        public string serviceRole(){
            var role = "";
            vAuthUser.servicesRole.ToList().ForEach(val=>{ if(isModule(val)) role = val; });
            return role;
        }
        public static string[] servicesRole = new string[]{"Customer","Admin"};
        public Boolean isServicesRole(){
            return this.isModule(servicesRole);
        }
        
        public Customer cus(){
            var row = Customer.FindMail(Mail);
            return row == null ? new Customer() : row;
        }

        public static vAuthUser vuser(string module, int msg_id = 0){
            var row = vAuthUser.db().Where(x=>x.isModule(module)&&(msg_id==0?true:x.cus().msg_id==msg_id)).FirstOrDefault();
             return row;
        }
    }
}