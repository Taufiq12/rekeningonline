using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Helpers;

namespace ProjectBYB.Models.Views
{
    public class vCustomerCIF
    {
        [Key]

        public string agama { get; set; }
        public string alamatEmail { get; set; }
        public string alamatID1 { get; set; }
        public string alamatID2 { get; set; }
        public string anak { get; set; }
        public string apuDataProfileResiko { get; set; }
        public string apuIdentitasNasabah { get; set; }
        public string apuInformasiLain1 { get; set; }
        public string apuJumlahTransaksi { get; set; }
        public string apuKegiatanUsaha { get; set; }
        public string apuLokasiUsaha { get; set; }
        public string apuProfilNasabah { get; set; }
        public string apuResumeAkhir { get; set; }
        public string apuStrukturKepemilikan { get; set; }
        public string bidangUsaha { get; set; }
        public string golonganPemilik { get; set; }
        public string istri { get; set; }
        public string jenisIdentitas { get; set; }
        public string jenisKelamin { get; set; }
        public string jenisNasabah { get; set; }
        public string jenisPekerjaan { get; set; }
        public string jumlahTanggungan { get; set; }
        public string kecamatan { get; set; }
        public string kelurahan { get; set; }
        public string kodeAO { get; set; }
        public string kodeCabang { get; set; }
        public string kodeNomorTelepon { get; set; }
        public string kodePos { get; set; }
        public string kota { get; set; }
        public string lamaBekerja { get; set; }
        public string namaBadanUsaha { get; set; }
        public string namaIbuKandung { get; set; }
        public string namaNasabah { get; set; }
        public string negaraAsal { get; set; }
        public string noID { get; set; }
        public string nomorHP { get; set; }
        public string nomorTelepon { get; set; }
        public string npwp { get; set; }
        public string pendidikanTerakhir { get; set; }
        public int penghasilanPerBulan { get; set; }
        public string propinsi { get; set; }
        public string statusPerkawinan { get; set; }
        public string sumberPenghasilan { get; set; }
        public string tanggalBerakhirID { get; set; }
        public string tanggalLahir { get; set; }
        public string tanggalTerbitID { get; set; }
        public string tempatLahir { get; set; }
        
        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vCustomerCIF> db(){
            return cx().vCustomerCIF;
        }
        public static vCustomerCIF Find(string namaNasabah, string namaIbuKandung, string noID){
            return db().Where(x => x.namaNasabah == namaNasabah && x.namaIbuKandung == namaIbuKandung && x.noID == noID).FirstOrDefault();
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                agama = this.agama,
                alamatEmail = this.alamatEmail,
                alamatID1 = this.alamatID1,
                alamatID2 = this.alamatID2,
                anak = this.anak,
                apuDataProfileResiko = this.apuDataProfileResiko,
                apuIdentitasNasabah = this.apuIdentitasNasabah,
                apuInformasiLain1 = this.apuInformasiLain1,
                apuJumlahTransaksi = this.apuJumlahTransaksi,
                apuKegiatanUsaha = this.apuKegiatanUsaha,
                apuLokasiUsaha = this.apuLokasiUsaha,
                apuProfilNasabah = this.apuProfilNasabah,
                apuResumeAkhir = this.apuResumeAkhir,
                apuStrukturKepemilikan = this.apuStrukturKepemilikan,
                bidangUsaha = this.bidangUsaha,
                golonganPemilik = this.golonganPemilik,
                istri = this.istri,
                jenisIdentitas = this.jenisIdentitas,
                jenisKelamin = this.jenisKelamin,
                jenisNasabah = this.jenisNasabah,
                jenisPekerjaan = this.jenisPekerjaan,
                jumlahTanggungan = this.jumlahTanggungan,
                kecamatan = this.kecamatan,
                kelurahan = this.kelurahan,
                kodeAO = this.kodeAO,
                kodeCabang = this.kodeCabang,
                kodeNomorTelepon = this.kodeNomorTelepon,
                kodePos = this.kodePos,
                kota = this.kota,
                lamaBekerja = this.lamaBekerja,
                namaBadanUsaha = this.namaBadanUsaha,
                namaIbuKandung = this.namaIbuKandung,
                namaNasabah = this.namaNasabah,
                negaraAsal = this.negaraAsal,
                noID = this.noID,
                nomorHP = this.nomorHP,
                nomorTelepon = this.nomorTelepon,
                npwp = this.npwp,
                pendidikanTerakhir = this.pendidikanTerakhir,
                penghasilanPerBulan = this.penghasilanPerBulan,
                propinsi = this.propinsi,
                statusPerkawinan = this.statusPerkawinan,
                sumberPenghasilan = this.sumberPenghasilan,
                tanggalBerakhirID = this.tanggalBerakhirID,
                tanggalLahir = this.tanggalLahir,
                tanggalTerbitID = this.tanggalTerbitID,
                tempatLahir = this.tempatLahir
            });
        }
    }
}