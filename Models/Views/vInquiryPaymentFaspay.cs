using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Views
{
    public class vInquiryPaymentFaspay
    {
        [Key]
        public int id { get; set; }
        public string response { get; set; }
        public string trx_id { get; set; }
        public string Merchant_id { get; set; }
        public string Merchant { get; set; }
        public string bill_no { get; set; }
        public string payment_date { get; set; }
        public string status { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vInquiryPaymentFaspay> db(){
            return cx().vInquiryPaymentFaspay;
        }
        public static vInquiryPaymentFaspay Find(string trx_id, string bill_no){
            return db().Where(x => x.trx_id == trx_id && x.bill_no == bill_no).FirstOrDefault();
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.id,
                response = this.response,
                trx_id = this.trx_id,
                Merchant_id =this.Merchant_id,
                Merchant = this.Merchant,
                bill_no = this.bill_no,
                payment_date = this.payment_date,
                status = this.status
            });
        }
    }
}