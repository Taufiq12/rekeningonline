using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Views
{
    public class vHeaderInquiryPaymentFaspay
    {
        [Key]
        public string trx_id { get; set; }
        public string FullName { get; set; }
        public string bill_no { get; set; }
        public string Payment_Channel { get; set; }
        public string redirect_url { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vHeaderInquiryPaymentFaspay> db(){
            return cx().vHeaderInquiryPaymentFaspay;
        }
        public static vHeaderInquiryPaymentFaspay Find(string trx_id, string bill_no){
            return db().Where(x => x.trx_id == trx_id && x.bill_no == bill_no).FirstOrDefault();
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                trx_id = this.trx_id,
                FullName = this.trx_id,
                bill_no =this.bill_no,
                Payment_Channel = this.Payment_Channel,
                redirect_url = this.redirect_url
            });
        }
    }
}