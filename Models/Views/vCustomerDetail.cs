using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Helpers;

namespace ProjectBYB.Models.Views
{
    public class vCustomerDetail
    {
        [Key]
        public string cif { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Title { get; set; }
        public string PlaceOfBirth { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MotherName { get; set; }
        public string Country { get; set; }
        public string CountryOther { get; set; }
        public string Identity { get; set; }
        public string TypeIdentity { get; set; }
        public string StartDateIdentity { get; set; }
        public string EndDateIdentity { get; set; }
        public string AddressIdentity { get; set; }
        public string RTIdentity { get; set; }
        public string RWIdentity { get; set; }
        public string KelIdentity { get; set; }
        public string KecIdentity { get; set; }
        public string KabIdentity { get; set; }
        public string ProvinceIdentity { get; set; }
        public string PostalCodeIdentity { get; set; }
        public string ResidenceAddress { get; set; }
        public string RTResidence { get; set; }
        public string RWResidence { get; set; }
        public string KelResidence { get; set; }
        public string KecResidence { get; set; }
        public string KabResidence { get; set; }
        public string ProvinceResidence { get; set; }
        public string PostalCodeResidence { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Mail { get; set; }
        public string Education { get; set; }
        public string religion { get; set; }
        public string MaterialStatus { get; set; }
        public int NumberOfDependents { get; set; }
        public string HomeStatus { get; set; }
        public int LongOccupyYear { get; set; }
        public int LongOccupyMonth { get; set; }
        public string Hobby { get; set; }
        public string NPWP { get; set; }
        public int? msg_id { get; set; }
        public string FamilyFullName { get; set; }
        public string FamilyRelationshipWithTheApplicant { get; set; }
        public string FamilyPhoneNumber { get; set; }
        public string FamilyAddress { get; set; }
        public string FamilyRT { get; set; }
        public string FamilyRW { get; set; }
        public string FamilyKel { get; set; }
        public string FamilyKec { get; set; }
        public string FamilyKab { get; set; }
        public string FamilyProvince { get; set; }
        public string FamilyPostalCode { get; set; }
        public string SourceOfIncome { get; set; }
        public string TotalIncome { get; set; }
        public string SourceOfFunds { get; set; }
        public string PurposeOfOpeningAccount { get; set; }
        public Boolean? AnotherAccount { get; set; }
        public string BankName { get; set; }
        public string Jobs { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string BusinessField { get; set; }
        public string CompanyAddress { get; set; }
        public string RTCompany { get; set; }
        public string RWCompany { get; set; }
        public string KelCompany { get; set; }
        public string KecCompany { get; set; }
        public string KabCompany { get; set; }
        public string ProvinceCompany { get; set; }
        public string PostalCodeCompany { get; set; }
        public string PhoneNumberCompany { get; set; }
        public string FaxNumberCompany { get; set; }
        public string ExtNumberCompany1 { get; set; }
        public string ExtNumberCompany2 { get; set; }
        public int LengthOfWorkYear { get; set; }
        public int LengthOfWorkMonth { get; set; }
        public DateTime? Created_Date { get; set; }
        public Boolean? IsDomisili { get; set; } 
        public string KodeCabang { get; set; }
        public string NamaCabang { get; set; }
        public string Attachment1 { get; set; }
        public string Attachment2 { get; set; }
        public string Attachment3 { get; set; }
        public string SavingType { get; set; }
        public string TypeCard { get; set; }
        public string isname { get; set; }
        public string isbirthdate { get; set; }
        public string isbirthplace { get; set; }
        public string isaddressasliri { get; set; }
        public string isaddress { get; set; }
        public string isidentity_photo { get; set; }
        public string status { get; set; }
        public string selfie_photo { get; set; }
        public String isRequestApprove { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vCustomerDetail> db(){
            return cx().vCustomerDetail;
        }
        public static vCustomerDetail Find(string Mail){
            return db().Where(x => x.Mail == Mail).FirstOrDefault();
        }
        
        public vCustomerDetail vuser(){
            return vCustomerDetail.Find(this.Mail);
        }

        public vCustomerDetail toCus(string mail){
            var row = vCustomerDetail.Find(mail);
            return row == null ? new vCustomerDetail() : row;
        }

        public JObject GetData(){
            return Gen.Combine(this, new {
                cif = this.cif,
                FullName = this.FullName,
                NickName = this.NickName,
                Title = this.Title,
                PlaceOfBirth = this.PlaceOfBirth,
                DateOfBirth = this.DateOfBirth,
                Gender = this.Gender,
                MotherName = this.MotherName,
                Country = this.Country,
                CountryOther = this.CountryOther,
                Identity = this.Identity,
                TypeIdentity = this.TypeIdentity,
                StartDateIdentity = this.StartDateIdentity,
                EndDateIdentity = this.EndDateIdentity,
                AddressIdentity = this.AddressIdentity,
                RTIdentity = this.RTIdentity,
                RWIdentity = this.RWIdentity,
                KelIdentity = this.KelIdentity,
                KecIdentity = this.KecIdentity,
                KabIdentity = this.KabIdentity,
                ProvinceIdentity = this.ProvinceIdentity,
                PostalCodeIdentity = this.PostalCodeIdentity,
                ResidenceAddress = this.ResidenceAddress,
                RTResidence = this.RTResidence,
                RWResidence = this.RWResidence,
                KelResidence = this.KelResidence,
                KecResidence = this.KecResidence,
                KabResidence = this.KabResidence,
                ProvinceResidence = this.ProvinceResidence,
                PostalCodeResidence = this.PostalCodeResidence,
                PhoneNumber = this.PhoneNumber,
                MobileNumber = this.MobileNumber,
                FaxNumber = this.FaxNumber,
                Mail = this.Mail,
                Education = this.Education,
                religion = this.religion,
                MaterialStatus = this.MaterialStatus,
                NumberOfDependents = this.NumberOfDependents,
                HomeStatus = this.HomeStatus,
                LongOccupyYear = this.LongOccupyYear,
                LongOccupyMonth = this.LongOccupyMonth,
                Hobby = this.Hobby,
                NPWP = this.NPWP,
                msg_id = this.msg_id,
                FamilyFullName = this.FamilyFullName,
                FamilyRelationshipWithTheApplicant = this.FamilyRelationshipWithTheApplicant,
                FamilyPhoneNumber = this.FamilyPhoneNumber,
                FamilyAddress = this.FamilyAddress,
                FamilyRT = this.FamilyRT,
                FamilyRW = this.FamilyRW,
                FamilyKel = this.FamilyKel,
                FamilyKec = this.FamilyKec,
                FamilyKab = this.FamilyKab,
                FamilyProvince = this.FamilyProvince,
                FamilyPostalCode = this.FamilyPostalCode,
                SourceOfIncome = this.SourceOfIncome,
                TotalIncome = this.TotalIncome,
                SourceOfFunds = this.SourceOfFunds,
                PurposeOfOpeningAccount = this.PurposeOfOpeningAccount,
                AnotherAccount = this.AnotherAccount,
                BankName = this.BankName,
                Jobs = this.Jobs,
                CompanyName = this.CompanyName,
                Position = this.Position,
                BusinessField = this.BusinessField,
                CompanyAddress = this.CompanyAddress,
                RTCompany = this.RTCompany,
                RWCompany = this.RWCompany,
                KelCompany = this.KelCompany,
                KecCompany = this.KecCompany,
                KabCompany = this.KabCompany,
                ProvinceCompany = this.ProvinceCompany,
                PostalCodeCompany = this.PostalCodeCompany,
                PhoneNumberCompany = this.PhoneNumberCompany,
                FaxNumberCompany = this.FaxNumberCompany,
                ExtNumberCompany1 = this.ExtNumberCompany1,
                ExtNumberCompany2 = this.ExtNumberCompany2,
                LengthOfWorkYear = this.LengthOfWorkYear,
                LengthOfWorkMonth = this.LengthOfWorkMonth,
                IsDomisili = this.IsDomisili,
                KodeCabang = this.KodeCabang,
                NamaCabang = this.NamaCabang,
                Attachment1 = this.Attachment1,
                Attachment2 = this.Attachment2,
                Attachment3 = this.Attachment3,
                SavingType = this.SavingType,
                TypeCard = this.TypeCard,
                isname = this.isname,
                isbirthdate = this.isbirthdate,
                isbirthplace = this.isbirthplace,
                isaddressasliri = this.isaddressasliri,
                isaddress = this.isaddress,
                isidentity_photo = this.isidentity_photo,
                status = this.status,
                selfie_photo = this.selfie_photo,
                isRequestApprove = this.isRequestApprove
            });
        }

         /* Join Function */
        public vCustomerDetail aiData;
        public vCustomerDetail vcd(){
            if(aiData!=null) return aiData;
            aiData = vCustomerDetail.Find(Mail);
            return aiData == null ? new vCustomerDetail() : aiData;
        }
    }
}