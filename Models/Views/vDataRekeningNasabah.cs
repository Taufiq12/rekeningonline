using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Helpers;

namespace ProjectBYB.Models.Views
{
    public class vDataRekeningNasabah
    {
        [Key]
        public string CIF { get; set; }
        public string Rekening { get; set; }
        public string Virtual_Account { get; set; }
        public string NamaNasabah { get; set; }
        public string Title { get; set; }
        public string TempatLahir { get; set; }
        public string TanggalLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string NamaIbuKandung { get; set; }
        public string NoKTP { get; set; }
        public string NPWP { get; set; }
        public string TanggalTerbitKTP { get; set; }
        public string TanggalBerakhirKTP { get; set; }
        public string Province { get; set; }
        public string Kabupaten { get; set; }
        public string Kecamatan { get; set; }
        public string Kelurahan { get; set; }
        public string KodeAO { get; set; }
        public string NamaAO { get; set; }
        public string Kode_Referral { get; set; }

        
        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vDataRekeningNasabah> db(){
            return cx().vDataRekeningNasabah;
        }
        public static vDataRekeningNasabah Find(string namaNasabah, string namaIbuKandung, string noID){
            return db().Where(x => x.NamaNasabah == namaNasabah && x.NamaIbuKandung == namaIbuKandung && x.NoKTP == noID).FirstOrDefault();
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                CIF = this.CIF,
                Rekening = this.Rekening,
                Virtual_Account = this.Virtual_Account,
                NamaNasabah = this.NamaNasabah,
                Title = this.Title,
                TempatLahir = this.TempatLahir,
                TanggalLahir = this.TanggalLahir,
                JenisKelamin = this.JenisKelamin,
                NamaIbuKandung = this.NamaIbuKandung,
                NoKTP = this.NoKTP,
                NPWP = this.NPWP,
                TanggalTerbitKTP = this.TanggalTerbitKTP,
                TanggalBerakhirKTP = this.TanggalBerakhirKTP,
                Province = this.Province,
                Kabupaten = this.Kabupaten,
                Kecamatan = this.Kecamatan,
                Kelurahan = this.Kelurahan,
                KodeAO = this.KodeAO,
                NamaAO = this.NamaAO,
                Kode_Referral = this.Kode_Referral
            });
        }
    }
}