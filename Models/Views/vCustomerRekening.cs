using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Helpers;

namespace ProjectBYB.Models.Views
{
    public class vCustomerRekening
    {
        [Key]

        public string Mail { get; set; }
        public string jenisPenggunaan { get; set; }
        public string jenisRekening { get; set; }
        public string kodeCabang { get; set; }
        public string kodeMataUang { get; set; }
        public string kolektibilitas { get; set; }
        public string lokasi { get; set; }
        public string namaNasabah { get; set; }
        public string namaRekening { get; set; }
        public string nomorNasabah { get; set; }
        public string sektorEkonomi { get; set; }
        
        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vCustomerRekening> db(){
            return cx().vCustomerRekening;
        }
        public static vCustomerRekening Find(string namaNasabah, string nomorNasabah){
            return db().Where(x => x.namaNasabah == namaNasabah && x.nomorNasabah == nomorNasabah).FirstOrDefault();
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                Mail = this.Mail,
                jenisPenggunaan = this.jenisPenggunaan,
                jenisRekening = this.jenisRekening,
                kodeCabang = this.kodeCabang,
                kodeMataUang = this.kodeMataUang,
                kolektibilitas = this.kolektibilitas,
                lokasi = this.lokasi,
                namaNasabah = this.namaNasabah,
                namaRekening = this.namaRekening,
                nomorNasabah = this.nomorNasabah,
                sektorEkonomi = this.sektorEkonomi
            });
        }
    }
}