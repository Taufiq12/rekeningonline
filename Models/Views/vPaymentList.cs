using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using Microsoft.EntityFrameworkCore;
using NETCore.Encrypt;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ProjectBYB.Models.Views
{
    public class vPaymentList
    {
        [Key]
        public string  payment_id { get; set; }
        public string id { get; set; }
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public int merchant_code { get; set; }
        public string account_number { get; set; }
        public string name { get; set; }
        public string bank_code { get; set; }
        public decimal amount { get; set; }
        public string transaction_timestamp { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vPaymentList> db(){
            return cx().vPaymentList;
        }
        public static vPaymentList Find(string id, string owner_id, string external_id,string name, string bank_code){
            return db().Where(x => x.id == id && x.owner_id == owner_id && x.external_id == external_id && x.name == name && x.bank_code == bank_code).FirstOrDefault();
        }
        
        public JObject GetData(){
            return Gen.Combine(this, new {
                payment_id = this.payment_id,
                id = this.id,
                owner_id =this.owner_id,
                external_id = this.external_id,
                merchant_code = this.merchant_code,
                account_number = this.account_number,
                name = this.name,
                bank_code = this.bank_code,
                amount = this.amount,
                transaction_timestamp = this.transaction_timestamp
            });
        }
    }
}