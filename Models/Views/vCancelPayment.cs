using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Helpers;
using ProjectBYB.Models.Tables;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace ProjectBYB.Models.Views
{
    public class vCancelPayment
    {
        [Key]
        public string id { get; set; }
        public string mail { get; set; }
        public string account_number { get; set; }
        public string status { get; set; }
        public DateTime expiration_date { get; set; }

        public static BYB cx(){
            return new BYB();
        }
        public static DbSet<vCancelPayment> db(){
            return cx().vCancelPayment;
        }
        public static vCancelPayment Find(string account_number){
            return db().Where(x => x.account_number == account_number).FirstOrDefault();
        }

        
        public JObject GetData(){
            return Gen.Combine(this, new {
                id = this.id,
                account_number = this.account_number,
                status = this.status,
                expiration_date = this.expiration_date
            });
        }
    }
}