// var webpack = require('webpack');

module.exports = {
  // baseUrl: 'http://localhost/lumen-vue/public',
  // proxy API requests to Valet during development
  // devServer: {
  //   proxy: 'http://sumber.local'
  // },

  configureWebpack: {
    /* plugins: [
      new webpack.ProvidePlugin({
        Promise: 'es6-promise-promise',
      })
    ], */
    resolve: {
      alias: {
        "@root": "@/",
        "@assets": "@/assets",
        "@plugins": "@/plugins",
        "@components": "@/components",
        "@config": "@/config",
        "@forms": "@/forms",
        "@helper": "@/helper",
        "@layout": "@/layout",
        "@backend": "@/backend",
        "@frontend": "@/frontend"
      }
    },
  },
  
  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  outputDir: '../wwwroot',

  pages: {
    index: {
      entry: 'src/main.js',
      template: process.env.NODE_ENV === 'production' ? 'public/index.cshtml' : 'public/index.watch.html'
    }
  },
  // modify the location of the generated HTML file.
  // make sure to do this only in production.
  indexPath: '../wwwroot/index.cshtml'
}