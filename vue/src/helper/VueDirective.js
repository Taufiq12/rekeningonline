/* Directive */
var VueDirective = {
    "tooltip" : {
        bind: function (el, binding) {
            if(typeof $.fn.tooltip == "undefined") return;
            var text = typeof binding.value == "string" ? binding.value : binding.value.text;
            var options = {
                html: true,
                placement: 'left',
                title: text,
                trigger: 'hover',
            }
            if(typeof binding.value == "object") options = Object.assign(options, binding.value)
            $(el).tooltip(options);
        },
        componentUpdated: function (el, binding) {
            if(typeof $.fn.tooltip == "undefined") return;
            if(!$(el).length) return;
            $(el).tooltip('dispose')
            var text = typeof binding.value == "string" ? binding.value : binding.value.text;
            var options = {
                html: true,
                placement: 'left',
                title: text,
                trigger: 'hover',
            }
            if(typeof binding.value == "object") options = Object.assign(options, binding.value)
            $(el).tooltip(options)
        },
        unbind(el) {
            if(!$(el).length) return;
            $(".tooltip.fade.show").remove();
        },
    },
}

export default VueDirective