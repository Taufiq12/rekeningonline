import Gen from '@helper/Gen';
import Config from './../config/Config';
import EventBus from './EventBus';

const BOGen = {
  user(){
    return Gen.getCookie('user')
  },
  notLogin(){
    return !this.user() || !Gen.getCookie('botk')
  },
  apirest(opt={}){
    if(typeof arguments[0] != "object"){
      opt = {url:arguments[0],data:arguments[1],success:arguments[2],type:arguments[3]||'get'};
    }
    if(opt.url.indexOf('http')==-1) opt.url = Config.apiUrl+opt.url
    var addQuery = Object.assign({
      token: Gen.getCookie("botk")
    },Gen.queryToObject(opt.url))
    opt.type = opt.type.toUpperCase()
    if(opt.type=="GET"){
      opt.data = Object.assign(addQuery, opt.data)
    }else{
      opt.url = opt.url.split("?")[0]+"?"+Gen.objectToQuery(addQuery)
      opt.data = JSON.stringify(opt.data)
      opt.contentType = "application/json; charset=utf-8"
      opt.dataType = "json"
    }
    opt.success = (resp)=>{ arguments[2](null, resp) }
    opt.error = (err)=>{ 
      if(err.status == 401) return EventBus.$emit("logout", true)
      err.json = err.responseJSON
      arguments[2](err, null)
    }
    return $.ajax(opt)
  },
  // User Role
  isMenu(menu){
    var user = this.user(); if(!user) return false;
    return user.al_access_menu.indexOf(menu) > -1
  },
  isModule(module){
    var user = this.user(); if(!user) return false;
    if(typeof module == "object"){
      var access = false;
      module.forEach((v)=>{
        if(this.isModule(v)) access = true;
      });
      return access;
    }
    return user.al_access_module.indexOf(module) > -1
  },
  fsDiff(page, oldFs, newFs){
    var fs = []
    var mrf_id = (page.input||{}).mrf_id || page.row.ai_mrf_id
    page.mfsData.forEach((v)=>{
      var old = Gen.clone(oldFs[v.id]||{})
      var n = Gen.clone(newFs[v.id]||{})
      if(v.mfs_type=="sharefolder"){
        if(mrf_id==24){
          old.afs_answer = old.afs_answer.filter((v2)=>{
            var show = true
            n.afs_answer.forEach((v3)=>{
              if(v2.foldername==v3.foldername) show = false
            })
            return show
          })
        }else{
          n.afs_answer.forEach((v2)=>{
            if(v2.foldername) old.afs_answer.add(v2)
          })
        }
      }else if(v.mfs_type=="mobile_device"){
        if(n.afs_answer.brand) old.afs_answer = n.afs_answer
      }else if(Array.isArray(n.afs_answer)){
        if(Array.isArray(old.afs_answer)){
          if(mrf_id==24){
            old.afs_answer.remove(n.afs_answer)
          }else{
            old.afs_answer.add(n.afs_answer)
          }
        }else{
          if(mrf_id!=24) old.afs_answer = n.afs_answer
        }
      }else if(n.afs_answer&&n.afs_answer!="null"){
        if(v.mfs_type=="bit"){
          if(mrf_id==24){
            old.afs_answer = false
          }else{
            old.afs_answer = n.afs_answer
          }
        }else if(v.mfs_type=="textfield") old.afs_answer = n.afs_answer
      }
      if(n.afs_remarks) old.afs_remarks = (old.afs_remarks?old.afs_remarks+"\n":"")+n.afs_remarks
      if(!old.afs_id) old = n
      old.afs_answer = JSON.stringify(old.afs_answer)
      fs.push(old)
    })
    return fs
  },
  isLockAuthorization(){
    try{
      var conf = (Gen.getCookie("variable")||{}).lockAuthorization || {}
      var start = new global.moment(conf.startdate, "DD-MM-YYYY");
      var end = new global.moment(conf.enddate, "DD-MM-YYYY");
      return start.isBefore() && end.isAfter() && conf.lock
    }catch{
      return false
    }
  }
}
export default BOGen