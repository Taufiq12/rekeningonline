var Fields = {
  // General
  FullName: "Nama Lengkap",
  NickName: "Nama Singkat",
  Gender : "Gender",
  Title : "Title",
  PlaceOfBirth: "Tempat Lahir",
  DateOfBirth: "Tanggal Lahir",
  MotherName : "Nama Gadis Ibu Kandung",
  Country : "Kewarganegaraan",
  TypeIdentity : "Tipe Identitas",
  Identity : "Identitas Diri",
  CountryOther : "Keterangan",
  StartDateIdentity : "Tanggal Terbit",
  EndDateIdentity : "Tanggal Berakhir", 
  AddressIdentity : "Alamat Sesuai Identitas Diri",
  RTIdentity : "RT",
  RWIdentity : "RW",
  KelIdentity : "Kel",
  KecIdentity : "Kec",
  KabIdentity : "Kab./Kodya",
  ProvinceIdentity : "Provinsi",
  PostalCodeIdentity : "Kode Pos", 
  PhoneNumber : "Nomor Telepon",
  MobileNumber : "Nomor Ponsel",
  NumberOfDependents : "Jumlah Tanggungan" ,
  HomeStatus : "Status Rumah" ,
  LongOccupyYear : "Tahun" ,
  LongOccupyMonth : "Bulan" ,
  Hobby : "Hobi" ,
  NPWP : "  NPWP" ,
  FamilyFullName : "Nama Lengkap",
  FamilyRelationshipWithTheApplicant  : "Hubungan Dengan Pemohon",
  FamilyPhoneNumber  : "No Telepon",
  FamilyAddress  : "Alamat Domisili",
  FamilyRT  : "RT",
  FamilyRW  : "RW",
  FamilyKel  : "Kel",
  FamilyKec  : "Kec",
  FamilyKab  : "Kab./Kodya",
  FamilyProvince  : "Provinsi",
  FamilyPostalCode  : "Kode Pos",

  ResidenceAddress : "Alamat Domisili",
  RTResidence : "RT",
  RWResidence : "RW",
  KecResidence : "Kec",
  KelResidence : "Kel",
  KabResidence : "Kab./Kodya",
  ProvinceResidence : "Provinsi",
  PostalCodeResidence : "Kode Pos",
  FaxNumber : "Nomor Faksimili",
  Mail : "E-mail",
  Education : "Pendidikan",
  religion : "Agama",
  MaterialStatus : "Status Perkawinan",

  Jobs : "Pekerjaan Saat Ini",
  CompanyName : "Nama Perusahaan / Instansi",
  Position : "Jabatan",
  BusinessField : "Bidang Usaha",
  CompanyAddress : "Alamat Perusahaan",
  RTCompany : "RT",
  RWCompany : "RW",
  KelCompany : "Kel",
  KecCompany : "Kec",
  KabCompany : "Kab./Kodya",
  ProvinceCompany : "Provinsi",
  PostalCodeCompany : "Kode Pos",
  PhoneNumberCompany : "Nomor Telepon",
  FaxNumberCompany  : "Nomor Faximili",
  ExtNumberCompany1 : "Ext.",
  ExtNumberCompany2 : "Ext.",
  LengthOfWorkYear : "Tahun Lama Bekerja",
  LengthOfWorkMonth : "Bulan Lama Bekerja",

  SourceOfIncome : "Sumber Penghasilan",
  TotalIncome : "Total Penghasilan / Bulan",
  SourceOfFunds : "Sumber Dana",
  PurposeOfOpeningAccount : "Tujuan Pembukaan Rekening",
  AnotherAccount : "Mempunyai Rekening pada Bank Lain",
  BankName : "Nama Bank",

  Password : "Password",
  NewPassword : "New Password",
  ReEnterNewPassword : "Re-enter New Password",
  OTP: "Kode OTP",

  Mt_Slug: "Slug",
  Mt_Title: "Template Title",
  Mt_Template: "Template Body",
  
  Status: "Status",
  Gender_Desc: "Description",
  Gender_Type: "Type Jenis Kelamin",

  Country_ID: "ID Kewarganegaraan",
  Country_Desc: "Description",

  Identity_Type: "Type Identitas",
  Identity_Desc: "Description",
  
  Education_Type: "Type Pendidikan",
  Education_Desc: "Description",

  Religion_Type: "Type Agama",
  Religion_Desc: "Description",

  MaterialStatus_Type: "Type Perkawinan",
  MaterialStatus_Desc: "Description",

  HomeStatus_Type: "Type Kepemilikan Rumah",
  HomeStatus_Desc: "Description",

  Hobby_Type: "Type Hobby",
  Hobby_Desc: "Description",

  Jobs_Desc: "Description",

  SourceOfIncome_Desc: "Description",

  SourceOfFunds_Desc: "Description",

  Provinsi: "Provinsi",
  Kabupaten: "Kota/Kab",
  Kecamatan: "Kecamatan",
  Kelurahan: "Kelurahan",
  Kodepos: "Kode Pos",

  Created_Date: "Tanggal Request",
  Updated_Date : "Tanggal Update",

  KodeCabang: "Kode Cabang",
  NamaCabang: "Nama Cabang",
  AlamatCabang: "Alamat Cabang",

  IsDomisili: "Alamat Sesuai Domisili Sesuai KTP?",

  Attachment1: "KTP",
  Attachment2: "KTP & SELFIE",
  Attachment3: "TANDA TANGGAN",

  TypeName: "Kode Tabungan",
  SavingType: "Jenis Tabungan",
  Type: "Type",
  TypeCard : "Type Kartu",
  Nominal_Saldo: "Nominal Saldo",
  Terbilang: "Terbilang",
  NumberKTP: "No KTP",

  //va
  name: "Customer Name",
  bank_code: "Bank Name",
  account_number: "Virtual Number",
  status: "Status",

  //Kode AO Akulaku
  Name : "Name",
  Title : "Jobs Title",
  KTP : "KTP",
  Reporting : "Reporting",
  Kode_AO : "Kode AO",
  //Kode_Referral : "Kode Refferal",
  Kode_Referance: "Kode Referance",
  Kode_Referral: "Kode Group",
  
  startdate: "Start Date",
  enddate: "End Date",
  RoleAccess: "Access Role",
  al_name : "Role Name",
  al_id : "Role Id",

  Password_Dukcapil : "Password Login Dukcapil"
  
}

export default Fields