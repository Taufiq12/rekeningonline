const BoConfig = {
  mrAccessMenu:{
    Dashboard: "Dashboard",
    SignUp: "Sign Up",
    Register: "Register CIF",
    Inbox: "Inbox",
    Template: "Master Template",
    RecoverPassword: "Recover Password",
    Gender: "Master Jenis Kelamin",
    Country: "Master Kewarganegaraan",
    Identity: "Master Identitas",
    Education: "Master Pendidikan",
    Religion: "Master Agama",
    MaterialStatus: "Master Status Perkawinan",
    HomeStatus: "Master Status Rumah",
    Hobby: "Master Hobby",
    Jobs: "Master Pekerjaan",
    SourceOfIncome: "Master Sumber Penghasilan",
    TotalIncome : "Master Total Penghasilan",
    SourceOfFunds: "Master Sumber Dana",
    PurposeOfOpeningAccount: "Master Tujuan Pembukaan Rekening",
    PostalArea: "Master Kota",
    RegistrasiBackOffice : "Master Registrasi Back Office",
    Branch : "Master Cabang",
    AdjustProfile : "Update Data Profile",
    ListVirtualAccountReport : "Virtual Account Report",
    ListVirtualAccountFaspayReport : "ListVirtualAccountFaspayReport",
    ListPaymentReport : "Payment Report",
    ListNasabah: "Nasabah Report",
    ListAOAkulakuReport: "AO Akulaku Report",
    ManageAO_Akulaku: "Manage AO Akulaku",
    Settings: "Settings",
  },
  mrAccessModule:{
    ApprovalSTAFFBO: "Approval Staff Back Office",
    ApprovalSPVBO: "Approval SPV Back Office",
    Customer: "Customer",
    Admin: "Super Admin",
  },
  sidebar: [
    {name:"Customer Management",icon:"ti-user",childs:[
      {name:"Sign Up",link:"SignUp"},
      {name:"Register CIF",link:"Register"},
      {name:"Pemilihan Product Tabungan", link:"ProductSavings"},
      {name:"Recover Password",link:"RecoverPassword"},
    ]},
    {name:"Adjust Profile",link:"AdjustProfile",icon:"ti-info"},
    {name:"Inbox",link:"Inbox",icon:"ti-email"},
    {name:"Settings",icon:"ti-settings",childs:[
      {name:"Cabang",link:"Branch"},
      {name: "Register Back Office",link:"RegistrasiBackOffice"},
      {name:"Settings",link:"Settings"},
    ]},
    {name:"Master",icon:"ti-harddrives",childs:[
      {name:"Jenis Kelamin",link:"Gender"},
      {name:"Negara",link:"Country"},
      {name:"Identitas",link:"Identity"},
      {name:"Pendidikan",link:"Education"},
      {name:"Agama",Link:"Religion"},
      {name:"Status Perkawinan",link:"MaterialStatus"},
      {name:"Status Rumah",link:"HomeStatus"},
      {name:"Hobby",link:"Hobby"},
      {name:"Pekerjaan",link:"Jobs"},
      {name:"Sumber Penghasilan",link:"SourceOfIncome"},
      {name:"Total Penghasilan",link:"TotalIncome"},
      {name:"Sumber Dana",link:"SourceOfFunds"},
      {name:"Tujuan Pembukaan Rekening",link:"PurposeOfOpeningAccount"},
      {name:"Template",link:"Template"},
      {name:"Kota",link:"PostalArea"},
      {name:"Jenis Tabungan",link:"Saving_Type"},
      {name:"Manage AO Akulaku",link:"ManageAO_Akulaku"}
    ]},
    {name:"Report",icon:"icon-docs",childs:[
      {name:"Virtual Account Report",link:"ListVirtualAccountReport"},
      {name:"Virtual Account Faspay Report", link:"ListVirtualAccountFaspayReport"},
      {name:"Payment Report",link:"ListPaymentReport"},
      {name: "AO Akulaku Report", link: "ListAOAkulakuReport"},
      {name: "Nasabah Report", link: "ListNasabah"}
    ]}
  ],
  mrFacilityInputType:{
    checkbox: "Multi Checkbox",
    radio: "Input Radio",
    bit: "Checkbox",
    sharefolder: "Sharefolder",
    textfield: "Textfield",
    dropdown: "Dropdown",
    currency: "Currency",
    heading: "Heading",
    note: "Note",
    employee_info: "Employee Info",
    mobile_device: "Mobile Device",
    access_door: "Access Door",
  },
  mrCountry:{
    INA: "Indonesia",
    L: "Lainnya"
  },
  mrGender:{
    M: "Pria",
    F: "Wanita"
  },
  mrIdentity:{
    KTP: "KTP",
    Paspor: "Paspor",
    KITAS: "Kitas"
  },
  mrEducation:{
    SD: "SD",
    SLTP: "SLTP",
    Diploma: "Diploma",
    S1: "S1",
    Pasca: "Pascasarjana",
    Lainnya: "Lainnya"
  },
  mrReligion: {
    I: "Islam",
    P: "Kristen",
    K: "Katolik",
    H: "Hindu",
    B: "Budha",
    L: "Lainnya"
  },
  mrMaterialStatus:{
    L: "Lajang",
    M: "Menikah",
    D: "Duda",
    J: "Janda"
  },
  mrHomeStatus:{
    MS:	"Milik Sendiri",
    KS:	"Kontrak/Sewa",
    MK:	"Milik Keluarga",
    MI:	"Milik Instansi",
    L:	"Lainnya"
  },
  mrSourceOfIncome: {
    1: "Penghasilan Tetap",
    2: "Penghasilan Tidak Tetap",
    3: "Lainnya"
  },
  mrTotalIncome:{
    1: "< Rp.3 Jt",
    2: "Rp.3 Jt -< Rp.7 Jt",
    3: "Rp.7 Jt -< Rp.25 Jt",
    4: "Rp.25 Jt -< Rp.100 Jt",
    5: ">Rp.100 Jt"
  },
  mrSourceOfFunds:{
    1: "Gaji / Upah",
    2: "Hasil Usaha",
    3: "Lainnya"
  },
  mrPurposeOfOpeningAccount:{
    1: "Simpanan / Investasi",
    2: "Transaksi Usaha / Bisnis",
    3: "Penerimaan Gaji",
    4: "Lainnya"
  },
  //mrAccsessLevel Admin
  //,Register,Inbox,Template,Gender,Country,Identity,Education,Religion,MaterialStatus,HomeStatus,Hobby,Jobs,SourceOfIncome,TotalIncome,SourceOfFunds,PurposeOfOpeningAccount,PostalArea,RegistrasiBackOffice,Branch
  //
  loginTimeout: 1000 * 60 * 60,
  mrPageLimit: {
    50: "50 page limit",
    100: "100 page limit",
    200: "200 page limit",
  },
  mrTypeCard: {
    Silver: "Silver",
    Gold: "Gold",
    Platinum: "Platinum"

  },
  mrGroupAO: {
    AL: "AkuLaku",
    BYB: "Bank Yudha Bhakti"
  }
}

export default BoConfig