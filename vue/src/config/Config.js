import env from "./../env";

const Config = {
  apiUrl: env.baseUrl+'/api',
  yearcopy: 2019,
}

export default Config