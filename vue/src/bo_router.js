import Gen from '@helper/Gen';
import BOGen from '@helper/BOGen';

var bo_router = [
  {path: "", name: "Dashboard", component:()=> import("@backend/Dashboard.vue")},
  {path: "profile", name: "Profile", component:()=> import("@backend/Profile.vue")},
  // Apps
  {path: "SignUp", name: "SignUp", component:()=> import("@backend/SignUp.vue"), meta: {accessMenu:true}},
  {path: "Register", name: "Register", component:()=> import("@backend/Register.vue"), meta: {accessMenu:true}},
  // {path: "Recover-Password/:id", name: "RecoverPassword", component:()=> import("@backend/RecoverPassword.vue"), meta: {accessMenu:true}},
  {path: "Inbox", name: "Inbox", component:()=> import("@backend/Inbox.vue"), meta: {accessMenu:true}},
  // {path: "Approval-BackOffice/:id", name: "ApprovalBackOffice", component:()=> import("@backend/ApprovalBackOffice.vue"),  meta:  {module: ["ApprovalSTAFFBO","ApprovalSPVBO"]}},
  {path: "Approval-BackOffice2/:id", name: "ApprovalBackOffice2", component:()=> import("@backend/ApprovalBackOffice2.vue"),  meta:  {module: ["ApprovalSTAFFBO","ApprovalSPVBO"]}},

  // {path: "RegisterOpenRek/:id", name: "RegisterOpenRek", component:()=> import("@backend/RegisterOpenRek.vue"),  meta:  {module: ["Admin","Customer"]}},
  {path: "Register-Rekening/:id", name: "RegisterOpenRek", component:()=> import("@backend/RegisterOpenRek.vue"),  meta:  {accessMenu:true}},
  {path: "Adjust-Profile", name: "AdjustProfile", component:()=> import("@backend/AdjustProfile.vue"), meta: {accessMenu:true}},
  {path: "Product-Savings", name: "ProductSavings", component:()=> import("@backend/ProductSavings.vue"), meta: {accessMenu:true}},

  //Report
  {path: "List-Virtual-Account-Report", name: "ListVirtualAccountReport", component:()=> import("@backend/ListVirtualAccountReport.vue"), meta: {accessMenu:true}},
  {path: "List-Virtual-Account-Faspay-Report", name: "ListVirtualAccountFaspayReport", component:()=> import("@backend/ListVirtualAccountFaspayReport.vue"), meta: {accessMenu:true}},
  {path: "List-Payment-Report", name: "ListPaymentReport", component:()=> import("@backend/ListPaymentReport.vue"), meta: {accessMenu:true}},
  {path: "List-AO-Akulaku-Report", name: "ListAOAkulakuReport", component:()=> import("@backend/ListAOAkulakuReport.vue"), meta: {accessMenu:true}},
  {path: "List-Nasabah-Report", name: "ListNasabah", component:()=> import("@backend/ListNasabah.vue"), meta: {accessMenu:true}},

  //Master
  {path: "Gender/:id?", name: "Gender", component:()=> import("@backend/Gender.vue"), meta: {accessMenu:true}},
  {path: "Country/:id?", name: "Country", component:()=> import("@backend/Country.vue"), meta: {accessMenu:true}},
  {path: "Branch/:id?", name: "Branch", component:()=> import("@backend/Branch.vue"), meta: {accessMenu:true}},
  {path: "Identity/:id?", name: "Identity", component:()=> import("@backend/Identity.vue"), meta: {accessMenu:true}},
  {path: "Education/:id?", name: "Education", component:()=> import("@backend/Education.vue"), meta: {accessMenu:true}},
  {path: "Religion/:id?", name: "Religion", component:()=> import("@backend/Religion.vue"), meta: {accessMenu:true}},
  {path: "Material-Status/:id?", name: "MaterialStatus", component:()=> import("@backend/MaterialStatus.vue"), meta: {accessMenu:true}},
  {path: "Home-Status/:id?", name: "HomeStatus", component:()=> import("@backend/HomeStatus.vue"), meta: {accessMenu:true}},
  {path: "Hobby/:id?", name: "Hobby", component:()=> import("@backend/Hobby.vue"), meta: {accessMenu:true}},
  {path: "Jobs/:id?", name: "Jobs", component:()=> import("@backend/Jobs.vue"), meta: {accessMenu:true}},
  {path: "Source-Income/:id?", name: "SourceOfIncome", component:()=> import("@backend/SourceOfIncome.vue"), meta: {accessMenu:true}},
  {path: "Total-Income/:id?", name: "TotalIncome", component:()=> import("@backend/TotalIncome.vue"), meta: {accessMenu:true}},
  {path: "Source-Funds/:id?", name: "SourceOfFunds", component:()=> import("@backend/SourceOfFunds.vue"), meta: {accessMenu:true}},
  {path: "Purpose-Opening-Account/:id?", name: "PurposeOfOpeningAccount", component:()=> import("@backend/PurposeOfOpeningAccount.vue"), meta: {accessMenu:true}},
  {path: "Template/:id?", name: "Template", component:()=> import("@backend/Template.vue"), meta: {accessMenu:true}},
  {path: "Postal-Area/:id?", name: "PostalArea", component:()=> import("@backend/PostalArea.vue"), meta: {accessMenu:true}},
  {path: "Registrasi-BackOffice/:id?", name: "RegistrasiBackOffice", component:()=> import("@backend/RegistrasiBackOffice.vue"), meta: {accessMenu:true}},
  {path: "Saving-Type/:id?", name: "Saving_Type", component:()=> import("@backend/Saving_Type.vue"), meta: {accessMenu:true}},
  {path: "Manage-AO/:id?", name: "ManageAO_Akulaku", component:()=> import("@backend/ManageAO_Akulaku.vue"), meta: {accessMenu:true}},
  {path: "settings/:id?", name: "Settings", component:()=> import("@backend/Settings.vue"), meta: {accessMenu:true}},
  
  // 404
  {path: '*', redirect: {name:"Dashboard"}},
];

var menuBypassRole = ["Profile"]

bo_router.map((v)=>{
  v.beforeEnter = (to, _from, next)=>{
    var user = BOGen.user()
    if(!user || !Gen.getCookie('botk')){
      return next({name:"SignUp",query:{url:to.fullPath}})
    }
    if(menuBypassRole.indexOf(to.name)>-1) return next()
    if(user.ae_al_id==0) return next()
    if(to.meta.accessMenu){
      if(!BOGen.isMenu(to.name)) return next({name:"Dashboard"})
    }
    if(to.meta.withLock&&BOGen.isLockAuthorization()){
      return next({name:"Dashboard"})
    }
    if(to.meta.module){
      if(!BOGen.isModule(to.meta.module)) return next({name:"Dashboard"})
    }
    next()
  }
})

var data = [
  // Sign Up
  {
    name: "SignUp",
    path: "SignUp",
    component: () => import("@backend/SignUp.vue"),
    beforeEnter: (_to, _from, next)=>{
      var user = BOGen.user()
      if(user && Gen.getCookie('botk')) return next({name:"Login"})
      next()
    }
  },
  // Login
  {
    name: "Login",
    path: "login",
    component: () => import("@backend/Login.vue"),
    beforeEnter: (_to, _from, next)=>{
      var user = BOGen.user()
      if(user && Gen.getCookie('botk')) return next({name:"Dashboard"})
      next()
    }
  },
  // Logout
  {
    name: "Logout",
    path: "logout",
    beforeEnter:(_to, _form, next)=>{
      Gen.putCookie("user")
      Gen.putCookie("botk")
      next({name:"Login"})
    }
  },
  // Reset Password
  {
    name: "RecoverPassword",
    path: "Recover-Password/:id",
    component: () => import("@backend/RecoverPassword.vue"),
    beforeEnter: (_to, _from, next)=>{
      var user = BOGen.user()
      if(user && Gen.getCookie('botk')) return next({name:"Login"})
      next()
    }
  },
  // Forgot
  {
    name: "Forgot",
    path: "Forgot",
    component: () => import("@backend/Forgot.vue"),
    beforeEnter: (_to, _from, next)=>{
      var user = BOGen.user()
      if(user && Gen.getCookie('botk')) return next({name:"Login"})
      next()
    }
  },
  {
    path:"",
    component: () => import('@/layout/BoLayout.vue'),
    redirect: {name:'Dashboard'},
    children: bo_router
  },
]

export default data