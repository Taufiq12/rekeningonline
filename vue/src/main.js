import App from '@/App.vue';
import Router from 'vue-router'
// import '@helper/Global';
import Vue from 'vue';
import bo_router from './bo_router';
import VueDirective from '@helper/VueDirective';
import Fields from './config/Fields';
// import './registerServiceWorker';

Vue.config.productionTip = false
Vue.config.devtools = true

Vue.use(Router)

Object.keys(VueDirective).forEach((k)=>{
  Vue.directive(k, VueDirective[k])
})

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      component: () => import('@/layout/BoMain.vue'),
      redirect: {name:'Login'},
      children: bo_router
    }
  ]
})

router.beforeEach((to, from, next) => {
  $("body").attr("page", to.name)
  next()
})

global.App = new Vue({
  router,
  render: h => h(App),
  data(){
    return {
      base: {},
      baseUrl: "",
      var: {loadingOverlay: false,preloader: false},
      fields: Fields,
      page: {filter:{},menuRole(){}},
      user: {al_access_menu:[],al_access_module:[],emp:{}},
      width: $(window).width(),
      height: $(window).height(),
    }
  },
  computed:{
    isMobile(){ return this.width < 768 },
  },
  watch:{
    base(v){ global.Base = v },
    page(v){ global.Page = v },
  }
}).$mount('#app')