using System;
using System.Collections.Generic;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

namespace Helpers{
    public class Emails{
        /* Send Email */
        
        //Register
        public static Boolean RegisterLogin(Register ai){
            var toEmails = new List<string>();
            var mt = Template.Find("RegisterPassword");
            var emp = Register.FindMail(ai.Mail);
            var OTP = OTP_Customer.Find(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", emp.FullName + ",Registrasi Password");
            mt.Mt_Template = mt.Mt_Template.Replace(":baseUrl:", Config.BaseURL);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
            mt.Mt_Template = mt.Mt_Template.Replace(":OTP:", OTP.OTP);
            mt.Mt_Template = mt.Mt_Template.Replace(":Mobile_Number:", emp.MobileNumber);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Recover-Password/"+ai.Mail);
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        public static Boolean RegisterLoginBackOffice(Register ai){
            var toEmails = new List<string>();
            var mt = Template.Find("RegisterPasswordBackoffice");
            var emp = Register.FindMail(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", emp.FullName + ",Registrasi Password");
            mt.Mt_Template = mt.Mt_Template.Replace(":baseUrl:", Config.BaseURL);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
            mt.Mt_Template = mt.Mt_Template.Replace(":Password:", Encrypt.Decrypt(emp.Password));
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Reset Password
        public static Boolean ResetPassword(Register ai){
            var toEmails = new List<string>();
            var mt = Template.Find("ResetPassword");
            var emp = Register.FindMail(ai.Mail);
            var OTP = OTP_Customer.Find(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", emp.FullName + ", berikut adalah PIN Anda");
            mt.Mt_Template = mt.Mt_Template.Replace(":baseUrl:", Config.BaseURL);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
            mt.Mt_Template = mt.Mt_Template.Replace(":OTP:", OTP.OTP);
            mt.Mt_Template = mt.Mt_Template.Replace(":Mobile_Number:", emp.MobileNumber);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Recover-Password/"+ai.Mail);
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Buka Rekening
        public static Boolean OpenAccount(Register ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("OpenAccount");
            var emp = vCustomerDetail.Find(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Pembukaan Rekening");
            mt.Mt_Template = mt.Mt_Template.Replace(":baseUrl:", Config.BaseURL);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
            mt.Mt_Template = mt.Mt_Template.Replace(":Mobile_Number:", emp.MobileNumber);
             mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Request Transfer VA
        public static Boolean TransferOpenRekening(Register ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("Transfer");
            var emp = vCustomerDetail.Find(ai.Mail);
            var sav = Saving_Type.FindSavingType(emp.SavingType,emp.TypeCard);
            var va = UserVirtualAccount.FindMobile(emp.MobileNumber);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Transfer Rekening");
            mt.Mt_Template = mt.Mt_Template.Replace(":Nominal_Saldo:", sav.Saldo);
            mt.Mt_Template = mt.Mt_Template.Replace(":VA_Number:", va.account_number);
            mt.Mt_Template = mt.Mt_Template.Replace(":expiration_date:", Convert.ToDateTime(va.expiration_date).ToString("yyyy-MM-dd HH:mm:ss") );
            mt.Mt_Template = mt.Mt_Template.Replace(":Terbilang:", sav.Terbilang);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Reject Buka Rekening (BackOffice)
        public static Boolean TransferOpenRekeningReject(Register ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("TransferReject");
            var emp = vCustomerDetail.Find(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Reject Pembukaan Rekening");
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Sukses Transfer VA
        public static Boolean SuksesTransferVA(Register ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("SuccessTransfer");
            var emp = vCustomerDetail.Find(ai.Mail);
            var sav = Saving_Type.FindSavingType(emp.SavingType,emp.TypeCard);
            toEmails.Add(emp.Mail);
            mt = Template.Find("SuccessTransfer");
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Sukses Transfer Rekening");
            mt.Mt_Template = mt.Mt_Template.Replace(":Nominal_Saldo:", sav.Saldo);
            mt.Mt_Template = mt.Mt_Template.Replace(":Terbilang:", sav.Terbilang);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
            return true;
        }

        //Info CIF Customer
        public static Boolean InfoCIFCoreBanking(Customer ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("InfoCIFCoreBanking");
            var rek = PembukaanRekening.FindCIF(ai.cif);
            toEmails.Add(ai.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", ai.FullName);
            mt.Mt_Template = mt.Mt_Template.Replace(":cif:", rek.nomorNasabah);
            mt.Mt_Template = mt.Mt_Template.Replace(":norek:", rek.nomorRekening);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "No Rekening Online");
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Sukses Reset Password
        public static Boolean SuccessReset(Register ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("SuccessReset");
            var emp = vCustomerDetail.Find(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "kata sandi Anda berhasil di-reset");
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        //Non Aktive Account
        public static Boolean notActiveAccount(Register ai)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("notActiveAccount");
            var emp = vCustomerDetail.Find(ai.Mail);
            var Loc = LogActivity.FindMail(ai.Mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Notifikasi Akun");
            mt.Mt_Template = mt.Mt_Template.Replace(":IssueDate:", Loc.IssueDate);
            mt.Mt_Template = mt.Mt_Template.Replace(":SystemOperations:", Loc.SystemOperations);
            mt.Mt_Template = mt.Mt_Template.Replace(":Browser:", Loc.Browser);
            mt.Mt_Template = mt.Mt_Template.Replace(":Location:", Loc.Location);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        public static Boolean ForwardApproval(vRoleAccess ai, Customer a, string Notes)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("ForwardApproval");
            var app = vRoleAccess.Find(ai.Mail);
            var emp = Customer.FindMail(a.Mail);
            toEmails.Add(app.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", app.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Request Approval");
            mt.Mt_Template = mt.Mt_Template.Replace(":Nama_Request:", emp.FullName);
            mt.Mt_Template = mt.Mt_Template.Replace(":Alamat_Request:", emp.AddressIdentity);
            mt.Mt_Template = mt.Mt_Template.Replace(":No_Telp:", emp.MobileNumber);
            mt.Mt_Template = mt.Mt_Template.Replace(":Tgl_Lahir:", Convert.ToDateTime(emp.DateOfBirth).ToString("dd-MM-yyyy"));
            mt.Mt_Template = mt.Mt_Template.Replace(":Tmp_Lahir:", emp.PlaceOfBirth);
            mt.Mt_Template = mt.Mt_Template.Replace(":Nama_Ibu:", emp.MotherName);
            mt.Mt_Template = mt.Mt_Template.Replace(":Notes:", Notes);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

        public static Boolean CancelPayment(vCancelPayment a)
        {
            var toEmails = new List<string>();
            var mt = Template.Find("CancelPayment");
            var emp = Customer.FindMail(a.mail);
            toEmails.Add(emp.Mail);
            mt.Mt_Template = mt.Mt_Template.Replace(":Full_Name:", emp.FullName);
			mt.Mt_Title = mt.Mt_Title.Replace(":Request_For:", "Cancel Payment");
            mt.Mt_Template = mt.Mt_Template.Replace(":VA:", a.account_number);
            mt.Mt_Template = mt.Mt_Template.Replace(":link:", Config.BaseURL+"/Login/");
            return Gen.SendEmail(toEmails, mt.Mt_Title, mt.Mt_Template);
        }

    }
}