
using System;
using System.Linq;
using System.Net;
using ProjectBYB.Models.Tables;
using RestSharp;


namespace Helpers{
     class ApiGetLocation{

        public static async void LocationBrowser(string Email,string SystemOperations, string Browser){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client =  new RestClient("https://whoer.net/v2/geoip2-city");
                var request = new RestRequest(Method.GET);
                
                request.AddHeader("content-type", "application/json");
                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespLocation>(resp.Content);
                Console.Write(response);

                //Insert Responses
                var data = new LogActivity();
                var check = LogActivity.db().Where(x=>x.Email == Email).FirstOrDefault();
                if(check!=null) data = check;
                data.Email = Email;
                data.IssueDate = DateTime.Now.ToString("dd-MMM-yyy hh:mm tt");
                data.SystemOperations = SystemOperations;

                if (Browser.Contains("Firefox"))
                    data.Browser = "Firefox";

                if (Browser.Contains("KHTML, like Gecko"))
                    data.Browser = "Chrome";

                if (Browser.Contains("OPR"))
                    data.Browser = "Opera";

                if (Browser.Contains("Mobile"))
                    data.Browser = "Safari";

                if (Browser.Contains("Edge"))
                    data.Browser = "Edge";
                
                if (Browser.Contains("Trident"))
                    data.Browser = "IE";

                if (Browser.Contains("PostmanRuntime"))
                    data.Browser = "Postman";

                data.Location = response.city_name + ", ("+response.country_code+") " + response.country_name;

                if(check==null){
                    LogActivity.Insert(data);
                }else{
                    LogActivity.Update(data);
                }


            }catch(Exception e)
            {
                ErrorException.Logs(e);
            }
        }
    }

    public class RespLocation{
        public string city_name { get; set; } 
        public string continent_code { get; set; } 
        public string continent_name { get; set; } 
        public string country_code { get; set; } 
        public string country_name { get; set; } 
        public string subdivision1_code { get; set; } 
        public string subdivision1_name { get; set; } 
        public string subdivision2_code { get; set; } 
        public string subdivision2_name { get; set; } 
        public string time_zone { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }

    }
}