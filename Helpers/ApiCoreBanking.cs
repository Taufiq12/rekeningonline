
using System;
using System.Linq;
using System.Net;
using ProjectBYB.Models.Tables;
using RestSharp;

namespace Helpers{
    class ApiCoreBanking{

        //Status Verification Core Banking
        public static async void RequestStatusCoreBanking(string FullName,string MobileNumber,string MotherName,string Tr_Number){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client =  new RestClient(Config.ApiCoreBankingUrl+"/RequestStatusCoreBanking");
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                        FullName = FullName,
                        MobilNumber = MobileNumber,
                        MotherName = MotherName,
                        Tr_Number = Tr_Number
                };
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);
                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<GenResponseApproveTransfer>(resp.Content);
                Console.Write(response);

                //Update Confrim Transfer
                var ct = new Confrim_Transfer();
                var ctrans = Confrim_Transfer.db().Where(x=>x.Tf_Number==Tr_Number && x.FullName == FullName).FirstOrDefault();
            
                ct.Status = response.status;
                Confrim_Transfer.Update(ct);
                //------------------------------
                
                //Update CIF
                var cus = new Customer();
                var cif = Customer.db().Where(x=>x.FullName == FullName && x.MobileNumber == MobileNumber && x.MotherName == MotherName).FirstOrDefault();

                cif.cif = response.cif;
                Customer.Update(cif);
                //------------------------------


            }catch{}
        }
        //-------------------------------------------------------------------------------------------------

        //Check CIF Core Banking        
        public static async void RequestCoreBanking(string FullName,string MobileNumber,string Identity,string MotherName){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client =  new RestClient(Config.ApiCoreBankingUrl
                +"/RequestCoreBanking?FullName="+FullName
                +"&MobileNumber="+MobileNumber
                +"&Identity="+Identity
                +"&MotherName="+MotherName
                );
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                        FullName = FullName,
                        Identity = Identity,
                        MobilNumber = MobileNumber,
                        MotherName = MotherName
                };
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);
                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<GenResponseCoreBanking>(resp.Content);
                Console.Write(response);

                //Insert Log Response
                var ai = new Customer();
                var Cus = Customer.db().Where(x=>x.FullName==FullName && x.MobileNumber == MobileNumber && x.MotherName == MotherName).FirstOrDefault();
                
                if(Cus!=null) ai = Cus;
                ai.KodeCabang = response.KodeCabang;
                ai.cif = response.cif;
                ai.FullName = response.FullName;
                ai.NickName = response.NickName;
                ai.MotherName = response.MotherName;
                ai.PlaceOfBirth = response.PlaceOfBirth;
                ai.DateOfBirth = Convert.ToDateTime(response.DateOfBirth);
                ai.Gender = response.Gender;
                ai.TypeIdentity = response.TypeIdentity;
                ai.Identity = response.Identity;
                ai.AddressIdentity = response.AddressIdentity;
                ai.RTIdentity = response.RTIdentity;
                ai.RWIdentity = response.RWIdentity;
                ai.KelIdentity = response.KelIdentity;
                ai.KecIdentity = response.KecIdentity;
                ai.KabIdentity = response.KabIdentity;
                ai.ProvinceIdentity = response.ProvinceIdentity;
                ai.PostalCodeIdentity = response.PostalCodeIdentity;
                
                if(Cus==null){
                    Customer.Insert(ai);
                }else{
                    Customer.Update(ai);
                }

            }catch{}
        }
        //-------------------------------------------------------------------------------------------------
        
    }

    public class GenResponseApproveTransfer{
        public int status { get; set; } 
        public string cif { get; set; }
    }

    public class GenResponseCoreBanking {
        public string KodeCabang { get; set; }
        public string cif { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string MotherName { get; set; }
        public string PlaceOfBirth { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string TypeIdentity { get; set; }
        public string Identity { get; set; }
        public string AddressIdentity { get; set; }
        public string RTIdentity { get; set; }
        public string RWIdentity { get; set; }
        public string KelIdentity { get; set; }
        public string KecIdentity { get; set; }
        public string KabIdentity { get; set; }
        public string ProvinceIdentity { get; set; }
        public string PostalCodeIdentity { get; set; }
    }
}