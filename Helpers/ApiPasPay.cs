
using System;
using System.Linq;
using System.Net;
using RestSharp;
using ProjectBYB.Models.Tables;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


//------------------------------------------------------------------------------

namespace Helpers{
    class ApiPasPay{
        
        
        public static async void CreateRequestVirtualAccount(
            string request, string merchant_id,string merchant, string bill_no,string bill_reff,string bill_date,
            string bill_expired,string bill_desc,string bill_currency,decimal bill_total,string payment_channel,string pay_type,
            string cust_no,string cust_name,string msisdn,string email,int terminal,string product,Decimal amount,int qty,
            int payment_plan,string tenor,int merchant_id_item,string signature
            ){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiRegisterFaspayUrl);
                var req = new RestRequest(Method.POST);

                var Item_List = new Item_List();

                Item_List.product=product;
                Item_List.qty=qty;
                Item_List.amount=amount;
                Item_List.payment_plan=payment_plan;
                Item_List.merchant_id="99999";
                Item_List.tenor=tenor;


                var bodyObj = new{
                        request = request,
                        merchant_id = merchant_id,
                        merchant = merchant,
                        bill_no = bill_no,
                        bill_reff = bill_reff,
                        bill_date = bill_date,
                        bill_expired = bill_expired,
                        bill_desc = bill_desc,
                        bill_currency = bill_currency,
                        bill_total = bill_total,
                        cust_no = cust_no,
                        cust_name = cust_name,
                        payment_channel = payment_channel,
                        pay_type = pay_type,
                        msisdn = msisdn,
                        email = email,
                        terminal = terminal,
                        item = Item_List,
                        signature = signature
                };

                req.AddHeader("content-type", "application/json");
                req.AddJsonBody(bodyObj);

                IRestResponse resp = await client.ExecuteTaskAsync(req);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespFasPay>(resp.Content);
                //var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespFasPay>("{\"response\":\"Transmisi Info Detil Pembelian\",\"trx_id\":\"5000780200000398\",\"merchant_id\":\"50007\",\"merchant\":\"Faspay Trial 7\",\"bill_no\":\"2020010800001\",\"bill_items\":[{\"product\":\"Invoice No. 2020010800001\",\"amount\":\"50000\",\"qty\":\"1\",\"payment_plan\":\"1\",\"tenor\":\"00\",\"merchant_id\":\"99999\"}],\"response_code\":\"00\",\"response_desc\":\"Sukses\",\"redirect_url\":\"https:\\/\\/dev.faspay.co.id\\/pws\\/100003\\/0830000010100000\\/68124f78e67a1d5ba9710638d4c946687c62efcb?trx_id=5000780200000398&merchant_id=50007&bill_no=2020010800001\"}");
 
                Console.WriteLine(response);

                if (response.response_code == "00")
                {
                    
                    string pageurl = Config.ApiResponseRedirectFaspay+signature+"/?trx_id="+response.trx_id+"&merchant_id="+response.merchant_id+"&bill_no="+bill_no;

                    //Insert Log Response
                    var ai = new RequestVirtualAccountFasPay();
                    var Resp = RequestVirtualAccountFasPay.db().Where(x=>x.bill_no == bill_no && x.Email == email).FirstOrDefault();
                    if(Resp!=null) ai = Resp;
                    ai.Request = request;
                    ai.Merchant_id = merchant_id;
                    ai.Merchant = merchant;
                    ai.bill_no = bill_no;
                    ai.bill_reff = bill_reff;
                    ai.bill_date = bill_date;
                    ai.bill_expired = bill_expired;
                    ai.bill_desc = bill_desc;
                    ai.bill_currency = bill_currency;
                    ai.bill_total = bill_total;
                    ai.payment_channel = payment_channel;
                    ai.pay_type = pay_type;
                    ai.cust_no = cust_no;
                    ai.cust_name = cust_name;
                    ai.Msisdn = msisdn;
                    ai.Email = email;
                    ai.Terminal = terminal;
                    ai.Product = product;
                    ai.Amount = amount;
                    ai.Qty = qty;
                    ai.payment_plan = payment_plan;
                    ai.tenor = tenor;
                    ai.merchant_id_item = merchant_id_item;
                    ai.Signature = signature;

                    if(Resp==null){
                        RequestVirtualAccountFasPay.Insert(ai);
                    }else{
                        RequestVirtualAccountFasPay.Update(ai);
                    }
                    

                    //Insert Response
                    var vaFa = new UserVirtualAccountFasPay();
                    var Respva = UserVirtualAccountFasPay.db().Where(x=>x.bill_no == bill_no).FirstOrDefault();
                    if(Respva!=null) vaFa = Respva;
                    vaFa.response = response.response;
                    vaFa.Email = email;
                    vaFa.trx_id = response.trx_id;
                    vaFa.merchant_id = merchant_id;
                    vaFa.merchant = response.merchant;
                    vaFa.bill_no = response.bill_no;
                    vaFa.product = Item_List.product;
                    vaFa.amount = Item_List.amount;
                    vaFa.payment_plan = Item_List.payment_plan;
                    vaFa.tenor = Item_List.tenor;
                    vaFa.merchant_id = Item_List.merchant_id;
                    vaFa.response_code = response.response_code;
                    vaFa.response_desc = response.response_desc;
                    vaFa.redirect_url = pageurl;
                    vaFa.created_date = DateTime.Now;

                    if(Respva==null){
                        UserVirtualAccountFasPay.Insert(vaFa);
                    }else{
                        UserVirtualAccountFasPay.Update(vaFa);
                    }

                    //Prosess CallBack
                    ApiPasPay.ResponseRedirectProcess(signature,response.trx_id,response.merchant_id,response.bill_no);

                }

            }catch (Exception e){
                
                ErrorException.Logs(e);
            }
        }


        public static async void ResponseRedirectProcess(string Signature,string trx_id,string Merchant_id,string bill_no){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiResponseRedirectFaspay+Signature+"/?trx_id="+trx_id+"&merchant_id="+Merchant_id+"&bill_no="+bill_no);
                var request = new RestRequest(Method.POST);
               
                request.AddHeader("content-type", "application/json");
                IRestResponse resp = await client.ExecuteTaskAsync(request);

                string pageurl = Config.ApiResponseRedirectFaspay+Signature+"/?trx_id="+trx_id+"&merchant_id="+Merchant_id+"&bill_no="+bill_no;
                string script = string.Format("window.open('{0}');", pageurl);


             }catch (Exception e){
                
                ErrorException.Logs(e);
            }
        }

        public static async void InquiryRedirectProcess(string request, string trx_id, int merchant_id,string bill_no,string signature){
            try{

                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiInquiryPaymentStatusFaspay);
                var req = new RestRequest(Method.POST);

                var bodyObj = new{
                    request = request,
                    trx_id = trx_id,
                    merchant_id = merchant_id,
                    bill_no = bill_no,
                    signature = signature
                };
               
                req.AddHeader("content-type", "application/json");
                req.AddJsonBody(bodyObj);

                IRestResponse resp = await client.ExecuteTaskAsync(req);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseInquiry>(resp.Content);
                Console.Write(response);

                if(response.response_code == "00")
                {
                    var ai = new InquiryPaymentFaspay();
                    var Resp = InquiryPaymentFaspay.db().Where(x=>x.bill_no == bill_no && x.trx_id == trx_id && x.payment_status_desc == response.payment_status_desc).FirstOrDefault();
                    if(Resp!=null) ai = Resp;
                    ai.response = response.response;
                    ai.trx_id = response.trx_id;
                    ai.merchant_id = response.merchant_id;
                    ai.merchant = response.merchant;
                    ai.bill_no = response.bill_no;
                    ai.payment_reff = response.payment_reff;
                    ai.payment_date = response.payment_date;
                    ai.payment_status_code = response.payment_status_code;
                    ai.payment_status_desc = response.payment_status_desc;
                    ai.response_code = response.response_code;
                    ai.response_desc = response.response_desc;

                    if(Resp==null){
                        InquiryPaymentFaspay.Insert(ai);
                    }else{
                        InquiryPaymentFaspay.Update(ai);
                    }

                }

             }catch (Exception e){
                
                ErrorException.Logs(e);
            }
        }

        public static async void RequestCancelTransaction(string request, string trx_id, int merchant_id,string merchant,string bill_no,string payment_cancel,string signature){
            try{

                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiCancelPaymentTransactionFaspay);
                var req = new RestRequest(Method.POST);

                var bodyObj = new{
                    request = request,
                    trx_id = trx_id,
                    merchant_id = merchant_id,
                    merchant = merchant,
                    bill_no = bill_no,
                    payment_cancel = payment_cancel,
                    signature = signature
                };
               
                req.AddHeader("content-type", "application/json");
                req.AddJsonBody(bodyObj);

                IRestResponse resp = await client.ExecuteTaskAsync(req);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespinseCancelTrans>(resp.Content);
                Console.Write(response);

               
                var ai = new CancelPaymentFaspay();
                var Resp = CancelPaymentFaspay.db().Where(x=>x.bill_no == bill_no && x.trx_id == trx_id).FirstOrDefault();
                if(Resp!=null) ai = Resp;
                ai.response = response.response;
                ai.trx_id = response.trx_id;
                ai.merchant_id = response.merchant_id;
                ai.merchant = response.merchant;
                ai.bill_no = response.bill_no;
                ai.trx_status_code = response.trx_status_code;
                ai.trx_status_desc = response.trx_status_desc;
                ai.payment_status_code = response.payment_status_code;
                ai.payment_status_desc = response.payment_status_desc;
                ai.payment_cancel_msg = response.payment_cancel_msg;
                ai.payment_cancel_date = response.payment_cancel_date;
                ai.response_code = response.response_code;
                ai.response_desc = response.response_desc;

                if(Resp==null){
                    CancelPaymentFaspay.Insert(ai);
                }else{
                    CancelPaymentFaspay.Update(ai);
                }

                

             }catch (Exception e){
                
                ErrorException.Logs(e);
            }
        }

    }
    
    //Parameter
    public class ParamRequest {
        public string request { get; set; }
        public string merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public string bill_reff { get; set; }
        public string bill_date { get; set; }
        public string bill_expired { get; set; }
        public string bill_desc { get; set; }
        public string bill_currency { get; set; }
        public decimal bill_total { get; set; }
        public string payment_channel { get; set; }
        public string pay_type { get; set; }
        public string cust_no { get; set; }
        public string cust_name { get; set; }
        public string msisdn { get; set; }
        public string email { get; set; }
        public int terminal { get; set; }
        public Item_List item { get; set; }
        public string signature { get; set; }

    }

    public class Item_List{
        public string product { get; set; }
        public Decimal amount { get; set; }
        public int qty { get; set; }
        public int payment_plan { get; set; }
        public string tenor { get; set; }
        public string merchant_id { get; set; }
    }


    ///Response
    public class RespFasPay{
            public string response { get; set; }
            public string trx_id { get; set; }
            public string merchant_id { get; set; }
            public string merchant { get; set; }
            public string bill_no { get; set; }
            public string response_code { get; set; }
            public string response_desc { get; set; }
            public string redirect_url { get; set; }
            public List<RespDataBill_Item> bill_items { get; set; }
    }

    public class RespDataBill_Item {
        public string product { get; set; }
        public Decimal amount { get; set; }
        public int qty { get; set; }
        public int payment_plan { get; set; }
        public string tenor { get; set; }
        public string merchant_id { get; set; }
    }

    class GenResponse{
        string status { get; set; }
        string message { get; set; }
    }

    //Response Inquiry Status
    class ResponseInquiry{
        public string response { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public string payment_reff { get; set; }
        public DateTime? payment_date { get; set; }
        public int payment_status_code { get; set; }
        public string payment_status_desc { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set; }
    }

    class RespinseCancelTrans{
        public string response { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public int trx_status_code { get; set; }
        public string trx_status_desc { get; set; }
        public int payment_status_code { get; set; }
        public string payment_status_desc { get; set; }
        public string payment_cancel_msg { get; set; }
        public DateTime? payment_cancel_date { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set ;}
    }

}