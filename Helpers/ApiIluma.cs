
using System;
using System.Linq;
using System.Net;
using ProjectBYB.Models.Tables;
using RestSharp;

namespace Helpers{
     class ApiIluma{
        //POST Payment------------------------------------------------------------------
        public static async void ValidasiPhoneNumber(string phone_number, string given_name, string surname){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiIlumaPhoneValidator);
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                    phone_number=phone_number,
                    given_name=given_name,
                    surname=surname
                 };

                //Credentials
                string username = Config.ApiAuthIluma;
                string password = "";
                string svcCredentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                request.AddHeader("Authorization", "Basic " + svcCredentials);
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);

                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseValidasiPhone>(resp.Content);
 
                Console.WriteLine(response);
                
                

            }catch (Exception e){
                ErrorException.Logs(e);
            }
        }

    }


    //Response---------------------------------------------------
    public class ResponseValidasiPhone {
        public string phone_number { get; set; }
        public string status { get; set; }
        public string given_name { get; set; }
        public string surname { get; set; }
        public string name_match { get; set; }
        public string created { get; set; }
        public string updated { get; set; }
        public string id { get; set; }

    }
    
}