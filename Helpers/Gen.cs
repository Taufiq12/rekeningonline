using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;


namespace Helpers
{
    public class Gen{
        public static IPAddress GetCurrentIP(){
            var network = NetworkInterface.GetAllNetworkInterfaces()
                .Where(x => x.Name == "Wi-Fi")
                .FirstOrDefault();
            if(network==null) return null;
            var props = network.GetIPProperties();
            var ipAddress = props.UnicastAddresses
                .Where(x => x.Address.AddressFamily == AddressFamily.InterNetwork)
                .Select(x => x.Address).FirstOrDefault();
            return ipAddress;
        }

        public static JObject Combine(dynamic item1, dynamic item2){
            var o1 = JObject.FromObject(item1);
            var o2 = JObject.FromObject(item2);

            o1.Merge(o2, new JsonMergeSettings{
                // union array values together to avoid duplicates
                MergeArrayHandling = MergeArrayHandling.Union
            });
            return o1;
        }

        public static Object Pagination(int total, dynamic data, ListTableDto q){
            return new {
                current_page = q.page,
                data = data,
                // from = 16,
                last_page = Math.Ceiling((double)total/q.limit),
                per_page = q.limit,
                // to = 23,
                total = total,
            };
        }

        public static bool SendEmail(List<string> toEmails, string title, string body, List<string> attachement = null, List<Attachment> attachements = null, List<string> CC = null){
            SmtpClient client = new SmtpClient(Config.SMTPServer);
            if(Config.SMTPPassword!="") client.Credentials = new NetworkCredential(Config.SMTPUsername, Config.SMTPPassword);
            if(Config.SMTPPort!=0) client.EnableSsl = true;
            if(Config.SMTPPort!=0) client.Port = Config.SMTPPort;
            
            try{
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(Config.SMTPEmail);
                if(Config.ToEmail != ""){
                    mailMessage.To.Add(Config.ToEmail);
                    title = title+" "+JsonConvert.SerializeObject(toEmails);
                }else{
                    toEmails.ForEach(email=>{ if(!String.IsNullOrEmpty(email)) mailMessage.To.Add(email); });
                }
                if(attachement != null){
                    attachement.ForEach(file=>{
                        var attachFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", file);
                        mailMessage.Attachments.Add(new Attachment(attachFile));
                    });
                }
                if(attachements != null){
                    attachements.ForEach(attachFile=>{
                        mailMessage.Attachments.Add(attachFile);
                    });
                }
                if(CC != null){
                    if(Config.ToEmail != ""){
                        title += " + CC: "+JsonConvert.SerializeObject(CC);
                        mailMessage.CC.Add("taufik.nur832@gmail.com");
                    }else{
                        CC.ForEach(email=>{ mailMessage.CC.Add(email); });
                    }
                }
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = body;
                mailMessage.Subject = title;
                client.SendAsync(mailMessage, "sendemail");
                return true;
            }catch (Exception e){
                return false;
            }
        }
        public static bool SendEmail(string email, string title, string body, List<string> attachement = null, List<Attachment> attachements = null){
            var toEmails = new List<string>(); toEmails.Add(email);
            return SendEmail(toEmails, title, body, attachement, attachements);
        }
    }
}