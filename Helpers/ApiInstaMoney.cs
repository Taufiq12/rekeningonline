
using System;
using System.Linq;
using System.Net;
using ProjectBYB.Models.Tables;
using RestSharp;

namespace Helpers{
     class ApiInstaMoney{
        //POST Payment------------------------------------------------------------------
        public static async void CreateRequestVirtualAccount(string Mail,
            string external_id, string bank_code, string name, string virtual_account_number,decimal suggested_amount, string expiration_date){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiCreateVirtualAccountInstaMoney);
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                    external_id=external_id,
                    bank_code=bank_code,
                    name=name,
                    account_number=virtual_account_number,
                    suggested_amount=suggested_amount,
                    expected_amount=suggested_amount,
                    is_closed=true,
                    is_single_use=true,
                    expiration_date=expiration_date
                 };

                //Credentials
                string username = Config.ApiAuthInstaMoney;
                string password = "";
                string svcCredentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                request.AddHeader("Authorization", "Basic " + svcCredentials);
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);

                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespCreateVA>(resp.Content);
 
                Console.WriteLine(response);
                
                if(response.status == "PENDING" || response.status == "ACTIVE")
                {
                
                    //Insert Log Response
                    var ai = new UserVirtualAccount();
                    var Resp = UserVirtualAccount.db().Where(x=>x.account_number == response.account_number && x.external_id == external_id && x.bank_code == bank_code && x.name == name).FirstOrDefault();
                    if(Resp!=null) ai = Resp;
                    ai.owner_id = response.owner_id;
                    ai.external_id = response.external_id;
                    ai.Mail = Mail;
                    ai.bank_code = response.bank_code;
                    ai.merchant_code = Convert.ToInt32(response.merchant_code);
                    ai.name = response.name;
                    ai.account_number = response.account_number;
                    ai.is_closed = response.is_closed;
                    ai.id = response.id;
                    ai.is_single_use = response.is_single_use; 
                    ai.status = response.status;
                    ai.expiration_date = response.expiration_date;
                    ai.suggested_amount = suggested_amount;

                    if(Resp==null){
                        UserVirtualAccount.Insert(ai);
                    }else{
                        UserVirtualAccount.Update(ai);
                    }

                    //Send Mail
                    var al = Register.Find(Mail);
                    Emails.TransferOpenRekening(al);

                }else{

                    var cus = Customer.db().Where(a=>a.Mail == Mail).FirstOrDefault();
                    var va = UserVirtualAccount.db().Where(x=>x.external_id == cus.MobileNumber).FirstOrDefault();
                    var lpa = new LogPayment();
                    var data = LogPayment.db().Where(x=> x.external_id == va.external_id).FirstOrDefault();
                    if(data!=null) lpa = data;
                    lpa.external_id = va.external_id;
                    lpa.status = "Gagal Pembuatan VA";
                    lpa.created = DateTime.Now;

                    LogPayment.Insert(lpa);
                }
                

            }catch (Exception e){
                ErrorException.Logs(e);
            }
        }

        public static async void UpdateRequestVirtualAccount(){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiVirtualAccountInstaMoney);
                var request = new RestRequest(Method.PATCH);
                
                //Credentials
                string username = Config.ApiAuthInstaMoney;
                string password = "";
                string svcCredentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                request.AddHeader("Authorization", "Basic " + svcCredentials);
                request.AddHeader("content-type", "application/json");

                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespUpdateVA>(resp.Content);
                Console.WriteLine(response);

                if(response.status == "PENDING" || response.status == "ACTIVE")
                {
                    //Insert Log Response
                    var ai = new Temp_UpdateVirutalAccountInstaMoney();
                    var Resp = Temp_UpdateVirutalAccountInstaMoney.db().Where(x=>x.external_id == "external_id" && x.bank_code == "bank_code" && x.name == "name").FirstOrDefault();
                    if(Resp!=null) ai = Resp;
                    ai.owner_id = response.owner_id;
                    ai.external_id = response.external_id;
                    ai.bank_code = response.bank_code;
                    ai.merchant_code = response.merchant_code;
                    ai.name = response.name;
                    ai.account_number = response.account_number;
                    ai.suggested_amount = response.suggested_amount;
                    ai.is_closed = response.is_closed;
                    ai.id = response.id;
                    ai.is_single_use = response.is_single_use;
                    ai.status = response.status;

                    if(Resp==null){
                        Temp_UpdateVirutalAccountInstaMoney.Insert(ai);
                    }else{
                        Temp_UpdateVirutalAccountInstaMoney.Update(ai);
                    }
                }
                

            }catch (Exception e){
                ErrorException.Logs(e);
            }
        }

        public static async void CallbackPyment(string external_id,decimal amount){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient("https://api.instamoney.co/callback_virtual_accounts/external_id="+external_id+"/simulate_payment");
                var request = new RestRequest(Method.POST);

                var bodyObj = new{
                    amount=amount
                 };
                //Credentials
                string username = Config.ApiAuthInstaMoney;
                string password = "";
                string svcCredentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                request.AddHeader("Authorization", "Basic " + svcCredentials);
                request.AddHeader("content-type", "application/json");

                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(resp.Content);
 
                Console.WriteLine(response);
                                

            }catch (Exception e){
                ErrorException.Logs(e);
            }
        }

        //------------------------------------------------------------------------------
    }


    //Response---------------------------------------------------

    public class Response{
        public string messages { get; set; }
    }

    public class RespCreateVA{
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string bank_code { get; set; }
        public string merchant_code { get; set; }
        public string name { get; set; }
        public string account_number { get; set; }
        public bool is_single_use { get; set; }
        public string status { get; set; }
        public DateTime expiration_date { get; set; }
        public bool is_closed { get; set; }
        public string id { get; set; }
        public decimal suggested_amount { get; set; }
    }

    public class RespUpdateVA{
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string bank_code { get; set; }
        public int merchant_code { get; set; }
        public string name { get; set; }
        public string account_number { get; set; }
        public decimal suggested_amount { get; set; }
        public Boolean is_closed {get; set; }
        public string id { get; set; }
        public Boolean is_single_use { get; set; }
        public string status { get; set; }
    }

}