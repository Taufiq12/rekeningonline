
using System;
using System.Linq;
using System.Net;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using RestSharp;

namespace Helpers{
     class ApiSwagger{

            //Endpoint CIF 
            public static async void RequestCIF(
                string namaNasabah, string kodeCabang, string alamatID1, string alamatID2,   
                string kelurahan, string kecamatan, string kota, string kodePos,   
                string propinsi, string kodeNomorTelepon, string nomorTelepon, string nomorHP,   
                string alamatEmail, string kodeAO, string npwp, string jenisNasabah,   
                string jenisKelamin, string agama, string statusPerkawinan, string pendidikanTerakhir,   
                string jumlahTanggungan, string anak, string istri, string tempatLahir,   
                string tanggalLahir, string negaraAsal, string jenisIdentitas, string noID,   
                string tanggalTerbitID, string tanggalBerakhirID, string jenisPekerjaan, string sumberPenghasilan,   
                int penghasilanPerBulan, string namaBadanUsaha, string bidangUsaha, string namaIbuKandung,   
                string golonganPemilik, string lamaBekerja, string apuDataProfileResiko, string apuIdentitasNasabah,   
                string apuLokasiUsaha, string apuProfilNasabah, string apuJumlahTransaksi, string apuKegiatanUsaha,   
                string apuStrukturKepemilikan, string apuInformasiLain1, string apuResumeAkhir
            ){
                try{
                    var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                    var client = new RestClient(Config.ApiswaggerNasabah);
                    var request = new RestRequest(Method.POST);
                    var bodyObj = new{
                            nomorNasabah="",
                            namaNasabah=namaNasabah,   
                            kodeCabang=kodeCabang,   
                            alamatID1=alamatID1,   
                            alamatID2=alamatID2,   
                            kelurahan=kelurahan,   
                            kecamatan=kecamatan,   
                            kota=kota,   
                            kodePos=kodePos,   
                            propinsi=propinsi,   
                            kodeNomorTelepon=kodeNomorTelepon,   
                            nomorTelepon=nomorTelepon,   
                            nomorHP=nomorHP,   
                            alamatEmail=alamatEmail,   
                            kodeAO=kodeAO,   
                            npwp=npwp,   
                            jenisNasabah=jenisNasabah,   
                            jenisKelamin=jenisKelamin,   
                            agama=agama,   
                            statusPerkawinan=statusPerkawinan,   
                            pendidikanTerakhir=pendidikanTerakhir,   
                            jumlahTanggungan=jumlahTanggungan,   
                            anak=anak,   
                            istri=istri,   
                            tempatLahir=tempatLahir,   
                            tanggalLahir=tanggalLahir,   
                            negaraAsal=negaraAsal,   
                            jenisIdentitas=jenisIdentitas,   
                            noID=noID,   
                            tanggalTerbitID=tanggalTerbitID,   
                            tanggalBerakhirID=tanggalBerakhirID,   
                            jenisPekerjaan=jenisPekerjaan,   
                            sumberPenghasilan=sumberPenghasilan,   
                            penghasilanPerBulan=penghasilanPerBulan,   
                            namaBadanUsaha=namaBadanUsaha,   
                            bidangUsaha=bidangUsaha,   
                            namaIbuKandung=namaIbuKandung,   
                            golonganPemilik=golonganPemilik,   
                            lamaBekerja=lamaBekerja,   
                            apuDataProfileResiko=apuDataProfileResiko,   
                            apuIdentitasNasabah=apuIdentitasNasabah,   
                            apuLokasiUsaha=apuLokasiUsaha,   
                            apuProfilNasabah=apuProfilNasabah,   
                            apuJumlahTransaksi=apuJumlahTransaksi, 
                            apuKegiatanUsaha=apuKegiatanUsaha,   
                            apuStrukturKepemilikan=apuStrukturKepemilikan,   
                            apuInformasiLain1=apuInformasiLain1,   
                            apuResumeAkhir=apuResumeAkhir

                    };
                    
                    request.AddHeader("content-type", "application/json");
                    request.AddJsonBody(bodyObj);
                    IRestResponse resp = await client.ExecuteTaskAsync(request);
                    var response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponCIFNasabah>(resp.Content);
                    Console.WriteLine(response);

                    if(response.nomorNasabah != "" || response.nomorNasabah != null)
                    {

                        //Log Payment 
                        var va = UserVirtualAccount.db().Where(x=>x.external_id == nomorHP && x.name == namaNasabah).FirstOrDefault();
                        var lpa = new LogPayment();
                        var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id).FirstOrDefault();
                       
                        lpa.id = va.id;
                        lpa.owner_id = va.owner_id;
                        lpa.external_id = va.external_id;
                        lpa.status = "Sukses Pembukaan Nasabah";
                        lpa.created = DateTime.Now;

                        LogPayment.Insert(lpa);


                        //Insert Log
                        var ai = new PembukaanNasabah();
                        var Resp = PembukaanNasabah.db().Where(x=>x.noID == noID && x.namaNasabah == namaNasabah && x.namaIbuKandung == namaIbuKandung).FirstOrDefault();
                        if(Resp!=null) ai = Resp;
                        ai.agama = response.agama;
                        ai.alamatEmail = response.alamatEmail;
                        ai.alamatID1 = response.alamatID1;
                        ai.alamatID2 = response.alamatID2;
                        ai.anak = response.anak;
                        ai.apuDataProfileResiko = response.apuDataProfileResiko;
                        ai.apuIdentitasNasabah = response.apuIdentitasNasabah;
                        ai.apuInformasiLain1 = response.apuInformasiLain1;
                        ai.apuJumlahTransaksi = response.apuJumlahTransaksi;
                        ai.apuKegiatanUsaha = response.apuKegiatanUsaha;
                        ai.apuLokasiUsaha = response.apuLokasiUsaha;
                        ai.apuProfilNasabah = response.apuProfilNasabah;
                        ai.apuResumeAkhir = response.apuResumeAkhir;
                        ai.apuStrukturKepemilikan = response.apuStrukturKepemilikan;
                        ai.bidangUsaha = response.bidangUsaha;
                        ai.golonganPemilik = response.golonganPemilik;
                        ai.istri = response.istri;
                        ai.jenisIdentitas = response.jenisIdentitas;
                        ai.jenisKelamin = response.jenisKelamin;
                        ai.jenisNasabah = response.jenisNasabah;
                        ai.jenisPekerjaan = response.jenisPekerjaan;
                        ai.jumlahTanggungan = response.jumlahTanggungan;
                        ai.kecamatan = response.kecamatan;
                        ai.kelurahan = response.kelurahan;
                        ai.kodeAO = response.kodeAO;
                        ai.kodeCabang = response.kodeCabang;
                        ai.kodeNomorTelepon = response.kodeNomorTelepon;
                        ai.kodePos = response.kodePos;
                        ai.kota = response.kota;
                        ai.lamaBekerja = response.lamaBekerja;
                        ai.namaBadanUsaha = response.namaBadanUsaha;
                        ai.namaIbuKandung = response.namaIbuKandung;
                        ai.namaNasabah = response.namaNasabah;
                        ai.negaraAsal = response.negaraAsal;
                        ai.noID = response.noID;
                        ai.nomorHP = response.nomorHP;
                        ai.nomorNasabah = response.nomorNasabah;
                        ai.nomorTelepon = response.nomorTelepon;
                        ai.npwp = response.npwp;
                        ai.pendidikanTerakhir = response.pendidikanTerakhir;
                        ai.penghasilanPerBulan = response.penghasilanPerBulan;
                        ai.propinsi = response.propinsi;
                        ai.request = response.request;
                        ai.response = response.response;
                        ai.statusPerkawinan = response.statusPerkawinan;
                        ai.sumberPenghasilan = response.sumberPenghasilan;
                        ai.tanggalBerakhirID = response.tanggalBerakhirID;
                        ai.tanggalLahir = response.tanggalLahir;
                        ai.tanggalTerbitID = response.tanggalTerbitID;
                        ai.tempatLahir = response.tempatLahir;

                        if(Resp==null){
                            PembukaanNasabah.Insert(ai);
                        }else{
                            PembukaanNasabah.Update(ai);
                        }

                        var rek = vCustomerRekening.db().Where(x=>x.namaNasabah == namaNasabah && x.namaRekening == namaNasabah && x.Mail == alamatEmail).FirstOrDefault();
                        //Sementara----------------------------------------------------------
                        //string tanggalBuka = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
                        var EndDate = EndOfDay.db().FirstOrDefault();
                        DateTime currentTime = EndDate.eod_Date;
                        string eod = currentTime.ToString("yyyy-MM-ddTHH:mm:ssZ");

                        string date;
                        if (EndDate.is_eod == true)
                        {
                            date = eod;
                        }else{ 
                            date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
                        }
                        //date = "2019-06-05T09:06:15.565Z";
                        string tanggalBuka = date;
                        //-------------------------------------------------------------------
                        ApiSwagger.RequestPembukaanRekening(alamatEmail.Trim().ToUpper(),rek.jenisPenggunaan.Trim().ToUpper(),rek.jenisRekening.Trim().ToUpper(),rek.kodeCabang.Trim().ToUpper(),
                        rek.kodeMataUang.Trim().ToUpper(),rek.kolektibilitas.Trim().ToUpper(),rek.lokasi.Trim().ToUpper(),rek.namaNasabah.Trim().ToUpper(),rek.namaNasabah.Trim().ToUpper(),
                        rek.nomorNasabah.Trim().ToUpper(),rek.sektorEkonomi.Trim().ToUpper(),tanggalBuka.Trim().ToUpper());

                        
                    }else{
                         //Log Payment 
                        var va = UserVirtualAccount.db().Where(x=>x.external_id == nomorHP && x.name == namaNasabah).FirstOrDefault();
                        var lpa = new LogPayment();
                        var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id).FirstOrDefault();
                        if(data!=null) lpa = data;
                        lpa.id = va.id;
                        lpa.owner_id = va.owner_id;
                        lpa.external_id = va.external_id;
                        lpa.status = "Gagal Pembukaan Nasabah";
                        lpa.created = DateTime.Now;

                        LogPayment.Insert(lpa);
                    }
             
                }catch (Exception e){
                    //Log Payment 
                    var va = UserVirtualAccount.db().Where(x=>x.external_id == nomorHP && x.name == namaNasabah).FirstOrDefault();
                    var lpa = new LogPayment();
                    var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id).FirstOrDefault();
                    if(data!=null) lpa = data;
                    lpa.id = va.id;
                    lpa.owner_id = va.owner_id;
                    lpa.external_id = va.external_id;
                    lpa.status = "Gagal Pembukaan Nasabah";
                    lpa.created = DateTime.Now;

                    LogPayment.Insert(lpa);

                    ErrorException.Logs(e);
                }
            }

            //Endpoint Pembukaan Rekening
            public static async void RequestPembukaanRekening(string Mail,string jenisPenggunaan,string jenisRekening,string kodeCabang,
            string kodeMataUang,string kolektibilitas,string lokasi,string namaNasabah,string namaRekening,
            string nomorNasabah,string sektorEkonomi, string tanggalBuka){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiswaggerRekening);
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                        jenisPenggunaan = jenisPenggunaan,
                        jenisRekening= jenisRekening,
                        kodeCabang= kodeCabang,
                        kodeMataUang= kodeMataUang,
                        kolektibilitas= kolektibilitas,
                        lokasi= lokasi,
                        namaNasabah= namaNasabah,
                        namaRekening= namaRekening,
                        nomorNasabah= nomorNasabah,
                        sektorEkonomi= sektorEkonomi,
                        tanggalBuka= tanggalBuka
                };
                
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);
                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespSwaggerRekening>(resp.Content);
                Console.WriteLine(response);
                
                if(response.nomorRekening != "" || response.nomorRekening != null)
                {

                    //Update Table Customer CIF
                    var cus = Customer.db().Where(a=>a.Mail == Mail && a.FullName == namaNasabah).FirstOrDefault();
                   
                    var va = UserVirtualAccount.db().Where(x=>x.external_id == cus.MobileNumber && x.name == namaNasabah).FirstOrDefault();
                    var lpa = new LogPayment();
                    var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id).FirstOrDefault();
                                      
                    lpa.id = va.id;
                    lpa.owner_id = va.owner_id;
                    lpa.external_id = va.external_id;
                    lpa.status = "Sukses Pembukaan Rekening";
                    lpa.created = DateTime.Now;

                    LogPayment.Insert(lpa);

                    //Insert Log
                    var pmRek = new PembukaanRekening();
                    var dataRek = PembukaanRekening.db().Where(x=>x.namaNasabah == namaRekening && x.nomorNasabah == nomorNasabah).FirstOrDefault();
                    if(dataRek!=null) pmRek = dataRek;
                    pmRek.jenisPenggunaan=response.jenisPenggunaan;
                    pmRek.jenisRekening=response.jenisRekening;
                    pmRek.kodeCabang=response.kodeCabang;
                    pmRek.kodeMataUang=response.kodeMataUang;
                    pmRek.kolektibilitas=response.kolektibilitas;
                    pmRek.lokasi=response.lokasi;
                    pmRek.namaNasabah=response.namaNasabah;
                    pmRek.namaRekening=namaRekening;
                    pmRek.nomorNasabah=response.nomorNasabah;
                    pmRek.nomorRekening=response.nomorRekening;
                    pmRek.request=response.request;
                    pmRek.response=response.response;
                    pmRek.sektorEkonomi=response.sektorEkonomi;
                    pmRek.tanggalBuka=response.tanggalBuka;

                    PembukaanRekening.Insert(pmRek);
                  

                    //Send Mail Notif CIF
                    Emails.InfoCIFCoreBanking(cus);
                }else{
                    //Log Payment 
                    var cus = Customer.db().Where(a=>a.Mail == Mail && a.FullName == namaNasabah).FirstOrDefault();
                    var va = UserVirtualAccount.db().Where(x=>x.external_id == cus.MobileNumber && x.name == namaNasabah).FirstOrDefault();
                    var lpa = new LogPayment();
                    var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id).FirstOrDefault();
                    if(data!=null) lpa = data;
                    lpa.id = va.id;
                    lpa.owner_id = va.owner_id;
                    lpa.external_id = va.external_id;
                    lpa.status = "Gagal Pembukaan Rekening";
                    lpa.created = DateTime.Now;

                    LogPayment.Insert(lpa);
                }

            }catch (Exception e){
                //Log Payment 
                var cus = Customer.db().Where(a=>a.Mail == Mail && a.FullName == namaNasabah).FirstOrDefault();
                var va = UserVirtualAccount.db().Where(x=>x.external_id == cus.MobileNumber && x.name == namaNasabah).FirstOrDefault();
                var lpa = new LogPayment();
                var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id).FirstOrDefault();
                if(data!=null) lpa = data;
                lpa.id = va.id;
                lpa.owner_id = va.owner_id;
                lpa.external_id = va.external_id;
                lpa.status = "Gagal Pembukaan Rekening";
                lpa.created = DateTime.Now;

                LogPayment.Insert(lpa);

                ErrorException.Logs(e);
            }
        }


        public static async void RequestCekSaldo(string cif,string nomorRekening){
            try{
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var client = new RestClient(Config.ApiswaggerCekSaldo);
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                    accountNumber = nomorRekening
                };
                
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);
                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<RespCekSaldo>(resp.Content);
                Console.WriteLine(response);

                //Insert Log
                var ai = new CekSaldo();
                var Resp = CekSaldo.db().Where(x=>x.cif == cif && x.accountNumber == nomorRekening).FirstOrDefault();
                if(Resp!=null) ai = Resp;
                ai.cif = cif;
                ai.accountNumber = nomorRekening;
                ai.accountName = response.accountName;
                ai.accountStatus = response.accountStatus;
                ai.accountType = response.accountType;
                ai.productType = response.productType;
                ai.accountCurrency = response.accountCurrency;
                ai.branch = response.branch;
                ai.plafond = response.plafond;
                ai.plafondSign = response.plafondSign;
                ai.blockedBalance = response.blockedBalance;
                ai.blockedBalanceSign = response.blockedBalanceSign;
                ai.minimumBalance = response.minimumBalance;
                ai.minimumBalanceSign = response.minimumBalanceSign;
                ai.clearingSettlement = response.clearingSettlement;
                ai.clearingSettlementSign = response.clearingSettlementSign;
                ai.availableBalance = response.availableBalance;
                ai.availableSign = response.availableSign;
                ai.ledgerBalance = response.ledgerBalance;
                ai.ledgerBalanceSign = response.ledgerBalanceSign;
                
                if(Resp==null){
                    CekSaldo.Insert(ai);
                }else{
                    CekSaldo.Update(ai);
                }

            }catch (Exception e){
            
                ErrorException.Logs(e);
            }
        }

     }

    //Response Pembukaan Nasabah   
    public class ResponCIFNasabah {
        public string agama { get; set; }
        public string alamatEmail { get; set; }
        public string alamatID1 { get; set; }
        public string alamatID2 { get; set; }
        public int anak { get; set; }
        public string apuDataProfileResiko { get; set; }
        public string apuIdentitasNasabah { get; set; }
        public string apuInformasiLain1 { get; set; }
        public string apuJumlahTransaksi { get; set; }
        public string apuKegiatanUsaha { get; set; }
        public string apuLokasiUsaha { get; set; }
        public string apuProfilNasabah { get; set; }
        public string apuResumeAkhir { get; set; }
        public string apuStrukturKepemilikan { get; set; }
        public string bidangUsaha { get; set; }
        public string golonganPemilik { get; set; }
        public int istri { get; set; }
        public string jenisIdentitas { get; set; }
        public string jenisKelamin { get; set; }
        public string jenisNasabah { get; set; }
        public string jenisPekerjaan { get; set; }
        public int jumlahTanggungan { get; set; }
        public string kecamatan { get; set; }
        public string kelurahan { get; set; }
        public string kodeAO { get; set; }
        public string kodeCabang { get; set; }
        public string kodeNomorTelepon { get; set; }
        public string kodePos { get; set; }
        public string kota { get; set; }
        public string lamaBekerja { get; set; }
        public string namaBadanUsaha { get; set; }
        public string namaIbuKandung { get; set; }
        public string namaNasabah { get; set; }
        public string negaraAsal { get; set; }
        public string noID { get; set; }
        public string nomorHP { get; set; }
        public string nomorNasabah { get; set; }
        public string nomorTelepon { get; set; }
        public string npwp { get; set; }
        public string pendidikanTerakhir { get; set; }
        public string penghasilanPerBulan { get; set; }
        public string propinsi { get; set; }
        public string request { get; set; }
        public string response { get; set; }
        public string statusPerkawinan { get; set; }
        public string sumberPenghasilan { get; set; }
        public DateTime tanggalBerakhirID { get; set; }
        public DateTime tanggalLahir { get; set; }
        public DateTime tanggalTerbitID { get; set; }
        public string tempatLahir { get; set; }
    }
     
    //Response Pembukaan Rekening
    public class RespSwaggerRekening {
        public string jenisPenggunaan { get; set; }
        public string jenisRekening { get; set; }
        public string kodeCabang { get; set; }
        public string kodeMataUang { get; set; }
        public string kolektibilitas { get; set; }
        public string lokasi { get; set; }
        public string namaNasabah { get; set; }
        public string namaRekening { get; set; }
        public string nomorNasabah { get; set; }
        public string nomorRekening { get; set; }
        public string request { get; set; }
        public string response { get; set; }
        public string sektorEkonomi { get; set; }
        public DateTime tanggalBuka { get; set; }
    }

    public class RespCekSaldo {
        public string accountName { get; set; }
        public string accountStatus { get; set; }
        public string accountType { get; set; }
        public string productType { get; set; }
        public string accountCurrency { get; set; }
        public string branch { get; set; }
        public decimal plafond { get; set; }
        public string plafondSign { get; set; }
        public decimal blockedBalance { get; set; }
        public string blockedBalanceSign { get; set; }
        public decimal minimumBalance { get; set; }
        public string minimumBalanceSign { get; set; }
        public decimal clearingSettlement { get; set; }
        public string clearingSettlementSign { get; set; }
        public decimal availableBalance { get; set; }
        public string availableSign { get; set; }
        public decimal ledgerBalance { get; set; }
        public string ledgerBalanceSign { get; set; }
    }

    
    
}