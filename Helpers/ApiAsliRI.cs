
using System;
using System.Linq;
using System.Net;
using ProjectBYB.Models.Tables;
using RestSharp;

namespace Helpers{
     class ApiAsliRI{
        public static async void ValidasiAsliRI(
            string nik,string name,string birthdate,string birthplace,string address,string identity_photo,string selfie_photo){
            try{

                //Sementara
                var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
                // var client = new RestClient(Config.ApiAsliRIUrl
                // +"/verify_profesional?nik="+nik+"&name="+name+"&birthdate="+birthdate+"&birthplace="+birthplace
                // +"&address="+address+"&identity_photo="+identity_photo+"&selfie_photo="+selfie_photo);
                var client = new RestClient(Config.ApiAsliRIUrl+"/verify_profesional");
                var request = new RestRequest(Method.POST);
                var bodyObj = new{
                    nik = nik,
                    name = name,
                    birthdate = birthdate,
                    birthplace = birthplace,
                    address = address,
                    identity_photo = identity_photo,
                    selfie_photo = selfie_photo
                };
                request.AddHeader("Token",Config.TokenASLIRI);
                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(bodyObj);
                IRestResponse resp = await client.ExecuteTaskAsync(request);
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<Resp>(resp.Content);

                //var response = Newtonsoft.Json.JsonConvert.DeserializeObject<Resp>("{\"timestamp\":1574150023,\"status\":200,\"errors\":{\"identity_photo\":\"invalid\"},\"data\":{\"name\":true,\"birthdate\":true,\"birthplace\":true,\"address\":\"JL.C*MP*K* * N*.80 B*M* *D*P*R*\",\"selfie_photo\":70}}");
                
                Console.WriteLine(response);
                
                //Insert Log Response
                var ai = new Verification_AsliRI();
                var AsliRI = Verification_AsliRI.db().Where(x=>x.Nik==nik).FirstOrDefault();
                if(AsliRI!=null) ai = AsliRI;
                ai.Nik = nik;
                ai.identity_photo = response.data.identity_photo != null ? response.data.identity_photo : "false";
                ai.Name= response.data.name != null ? response.data.name : "false";          
                ai.birthdate= response.data.birthdate != null ? response.data.birthdate : "false";      
                ai.birthplace= response.data.birthplace != null ? response.data.birthplace : "false";
                ai.address= response.data.address;
                ai.addressasliri = response.data.address != null ? "true" : "false";
                ai.status = response.status;
                ai.selfie_photo = response.data.selfie_photo;
                ai.Created_Date = DateTime.Now;
                if(AsliRI==null){
                    Verification_AsliRI.Insert(ai);
                }else{
                    Verification_AsliRI.Update(ai);
                }

            }catch (Exception e){
                ErrorException.Logs(e);
            }
        }

    }

    //------------------------------------------------------------

    public class Resp{
        public string timestamp { get; set; } 
        public string status { get; set; }
        public RespError errors { get; set; }
        public RespData data { get; set; }
    }

    public class RespError{
        public string selfie_photo { get; set; }
        public string identity_photo { get; set; }
    }

    public class RespData {
        public string name { get; set; }
        public string birthdate { get; set; }
        public string birthplace { get; set; }
        public string addressasliri { get; set; }
        public string address { get; set; }
        public string identity_photo { get; set; }
        public string selfie_photo { get; set; }
    }
}