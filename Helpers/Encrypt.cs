//Don't forget the using System.Security.Cryptography; statement when you add this class
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
public static class Encrypt
{
  // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
  // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
  private const string initVector = "pemgail9uzpgzl88";
  // This constant is used to determine the keysize of the encryption algorithm
  private const int keysize = 256;

    public static string Crypt(string encryptString)
    {
        string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
        0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
    });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                encryptString = Convert.ToBase64String(ms.ToArray());
            }
        }
        return encryptString;
    }

    public static string Decrypt(string cipherText)
    {
        string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        cipherText = cipherText.Replace(" ", "+");

        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
        0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
    });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }


  // public static string Crypt(this string text)
  // {
  //     string result = null;

  //     if (!String.IsNullOrEmpty(text))
  //     {
  //         byte[] plaintextBytes = Encoding.Unicode.GetBytes(text);

  //         SymmetricAlgorithm symmetricAlgorithm = DES.Create();
  //         symmetricAlgorithm.Key = new byte[8] {1, 2, 3, 4, 5, 6, 7, 8};
  //         using (MemoryStream memoryStream = new MemoryStream())
  //         {
  //             using (CryptoStream cryptoStream = new CryptoStream(memoryStream, symmetricAlgorithm.CreateEncryptor(), CryptoStreamMode.Write))
  //             {
  //                 cryptoStream.Write(plaintextBytes, 0, plaintextBytes.Length);
  //             }

  //             result = Encoding.Unicode.GetString(memoryStream.ToArray());
  //         }
  //     }

  //     return result;
  // }

  // public static string Decrypt(this string text)
  // {
  //     string result = null;

  //     if (!String.IsNullOrEmpty(text))
  //     {
  //         byte[] encryptedBytes = Encoding.Unicode.GetBytes(text);

  //         SymmetricAlgorithm symmetricAlgorithm = DES.Create();
  //         symmetricAlgorithm.Key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
  //         using (MemoryStream memoryStream = new MemoryStream(encryptedBytes))
  //         {
  //             using (CryptoStream cryptoStream = new CryptoStream(memoryStream, symmetricAlgorithm.CreateDecryptor(), CryptoStreamMode.Read))
  //             {
  //                 byte[] decryptedBytes = new byte[encryptedBytes.Length];
  //                 cryptoStream.Read(decryptedBytes, 0, decryptedBytes.Length);
  //                 result = Encoding.Unicode.GetString(decryptedBytes);
  //             }
  //         }
  //     }

  //     return result;
  // }

  // //Encrypt
  // public static string EncryptString(string plainText, string passPhrase)
  // {
  //   byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
  //   byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
  //   PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
  //   byte[] keyBytes = password.GetBytes(keysize / 8);
  //   RijndaelManaged symmetricKey = new RijndaelManaged();
  //   symmetricKey.Mode = CipherMode.CBC;
  //   ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
  //   MemoryStream memoryStream = new MemoryStream();
  //   CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
  //   cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
  //   cryptoStream.FlushFinalBlock();
  //   byte[] cipherTextBytes = memoryStream.ToArray();
  //   memoryStream.Close();
  //   cryptoStream.Close();
  //   return Convert.ToBase64String(cipherTextBytes);
  // }
  // //Decrypt
  // public static string DecryptString(string cipherText, string passPhrase){
  //   try{
  //     byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
  //     byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
  //     PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
  //     byte[] keyBytes = password.GetBytes(keysize / 8);
  //     RijndaelManaged symmetricKey = new RijndaelManaged();
  //     symmetricKey.Mode = CipherMode.CBC;
  //     ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
  //     MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
  //     CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
  //     byte[] plainTextBytes = new byte[cipherTextBytes.Length];
  //     int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
  //     memoryStream.Close();
  //     cryptoStream.Close();
  //     return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
  //   }catch{
  //     return "";
  //   }
  // }
}