using ASPNETCoreScheduler.BackgroundService;
using Microsoft.Extensions.DependencyInjection;
using NCrontab;
using System;
using System.Threading;
using System.Threading.Tasks;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Helpers;

namespace ASPNETCoreScheduler.Scheduler
{
    public abstract class ScheduledProcessor : ScopedProcessor
    {
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        protected abstract string Schedule { get; }
        public ScheduledProcessor(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
            _schedule = CrontabSchedule.Parse(Schedule);
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            do
            {
                var now = DateTime.Now;
                var nextrun = _schedule.GetNextOccurrence(now);

                //Check Payment
                foreach (var cP in vCancelPayment.db())
                {
                    var cus = Customer.Find(cP.mail);
                    string MobileNumber = cus.MobileNumber;
                    string CustomerName = cus.FullName;

                    //Update Status
                    cus.StatusType = "REJECT";
                    cus.Status = "Pembayaran telah Expired";

                    //Send Mail
                    var ai = Register.Find(cP.mail);
                    Customer.Update(cus);

                    //Insert Approval Log
                    var aal = new ApprovalLogs();
                    aal.Mail = cP.mail;
                    aal.Status = "Pembayaran telah Expired";
                    
                    aal.Remarks = "Batas pembayaran anda telah expired.";
                    aal.Created_By = "System";
                    aal.Created_Date = DateTime.Now;
                    ApprovalLogs.Insert(aal);

                    //Update VA
                    var VA = UserVirtualAccount.FindID(cP.id);
                    VA.status = "NOT ACTIVE";
                    VA.updated = DateTime.Now;
                    UserVirtualAccount.Update(VA);

                    Emails.CancelPayment(cP);
                }

                if (now > _nextRun)
                {
                    await Process();
                    _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);
        }
    }
}