using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;


public class ForwardApprovalDto{
    [Required]
    public string Mail { get; set; }
    public string Remarks { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class ForwardApprovalConroller : ProjectBYBController
    {
        public ForwardApprovalConroller(IConfiguration config){}

        [HttpPost("ForwardApproval")]
        public IActionResult ApprovalBackOffice([FromBody] ForwardApprovalDto b){
            if(!ModelState.IsValid) return BadRequest(ModelState);

			var cus = Customer.Find(b.Mail);
            string MobileNumber = cus.MobileNumber;
            string CustomerName = cus.FullName;

            cus.Status = "Menunggu Verifikasi";
            cus.StatusType = "BOSPV";
            Customer.Update(cus);

            //Insert Approval Log
            var aal = new ApprovalLogs();
            aal.Mail = b.Mail;
        
            aal.Status = "Menunggu Verifikasi";
            
            aal.Remarks = b.Remarks;
            aal.Created_By = authUser.FullName;
            aal.Created_Date = DateTime.Now;
            ApprovalLogs.Update(aal);


            var ai = vRoleAccess.db().Where(x=>x.al_name == "SPV Back Office").First();
            var a = Customer.db().Where(x=>x.Mail == b.Mail).First();
            
            Emails.ForwardApproval(ai,a,b.Remarks);

            return Ok(new {
				message = "Data has been approved",
			});

            
		}
    }
}