using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using System.IO;
using OfficeOpenXml;

public class SettingsCrudDto{
    public int id { get; set; }
    [Required]
    public dynamic s_values { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Settings")]
    [Route("/api")]
    public class SettingsController : ProjectBYBController
    {
        public SettingsController(IConfiguration config){}

        [HttpGet("Settings")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Settings.db()
                where EF.Functions.Like(s.s_name, $"%{q.search}%") || EF.Functions.Like(s.s_slug, $"%{q.search}%")
                select s;

            // Clear Temp
            this.deleteTemp();

            var total = query.Count();
            var data = new List<JObject>();
            query.OrderBy(x=>x.s_id)
                .Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Settings/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Settings.db().Find(id);
            return Ok(new {
                row = row == null ? new {} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Settings")]
        public IActionResult Crud([FromBody] SettingsCrudDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var fill = Settings.Find(b.id);
            if(fill==null) return BadRequest(new {message="Not found settings"});
            fill.s_values = JsonConvert.SerializeObject(b.s_values);
            fill.s_updated_at = DateTime.Now;
            fill.s_updated_by = authUser.NickName;
            Settings.Update(fill);
            return Ok(new {
                message = "Update settings successful",
            });
        }

        void deleteTemp(){
            // Delete all files in a directory    
            string tempPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", "temp");
            string[] files = Directory.GetFiles(tempPath);    
            foreach (string filename in files){
                if(filename.IndexOf(".gitignore")>-1) continue;
                FileInfo file = new FileInfo(filename);
                file.Delete();
                Console.WriteLine($"{filename} is deleted.");    
            }
        }
    }
}