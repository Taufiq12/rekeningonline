using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;

public class TemplateDto{
    public int id { get; set; }
    public int Mt_Type { get; set; }
    public string Mt_Slug { get; set; }
    [Required]
    public string Mt_Title { get; set; }
    public string Mt_Template { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Template")]
    [Route("/api")]
    public class TemplateController : ProjectBYBController
    {
        public TemplateController(IConfiguration config){}

        [HttpGet("Template")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Template.db()
                where
				(EF.Functions.Like(s.Mt_Title, $"%{q.search}%") || EF.Functions.Like(s.Mt_Template, $"%{q.search}%"))
                select s;

            var total = query.Count();
            var data = new List<JObject>();
            query.OrderBy(x=>x.Mt_Id)
                .Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Template/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Template.db().Where(x=>x.Mt_Id==id).FirstOrDefault();
            if(row!=null){
                row.Mt_Template = row.Mt_Template.Replace(":baseUrl:", Config.BaseURL);
            }
            return Ok(new {
                row = row == null ? new {mbg_country_id="ID",status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Template")]
        public IActionResult Crud([FromBody] TemplateDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Template.Find(b.id);

            var fill = new Template();
            fill.Mt_Type = b.Mt_Type!=0?b.Mt_Type:1;
            fill.Mt_Slug = b.Mt_Slug;
            fill.Mt_Title = b.Mt_Title;
            fill.Mt_Template = b.Mt_Template.Replace(Config.BaseURL, ":baseUrl:");
            if(first==null){
                return noPermissionResponse();
            }
            fill.Mt_Id = b.id;
            fill.Mt_Updated_Date = DateTime.Now;
            Template.Update(fill);
            return Ok(new {
                message = "Update master template successful",
            });
        }
    }
}