using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using ProjectBYB.Models.Tables;
using System.Text;  
using System.Security.Cryptography;  

public class FaspayNotification {
	    public string request { get; set; }
        public string trx_id { get; set; }
        public int merchant_id { get; set; }
        public string merchant { get; set; }
        public string bill_no { get; set; }
        public string payment_reff { get; set; }
        public DateTime payment_date { get; set; }
        public string payment_status_code { get; set; }
        public string payment_status_desc { get; set; }
        public decimal bill_total { get; set; }
        public decimal payment_total { get; set; }
        public int payment_channel_uid { get; set; }
        public string payment_channel { get; set; }
        public string signature { get; set; }

       
}

public class ParamResponseFaspayNotification{
    public string trx_id { get; set; }
    public int merchant_id { get; set; }
    public string bill_no { get; set; }
    public string response_code { get; set; }
    public string response_desc { get; set; }
    public DateTime response_date { get; set; }


}


namespace ProjectBYB.Controllers
{
    [Route("/api/billing")]
    public class FaspayNotificationController : ProjectBYBController
    {
        public FaspayNotificationController(IConfiguration config){}

        [HttpPost("Request")]
        public IActionResult RequestCallbackURL([FromBody] FaspayNotification b){

            string userid = Config.User_Merchant_Id;
            string password = Config.User_Merchant_Password;
            string bill = b.bill_no;

            string testString = userid+password+bill;
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(testString);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string signatuerMD5 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            string signatuerSH1;
            byte[] temp;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(signatuerMD5));

            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
                 {
                    sb.Append(temp[i].ToString("x2"));
                  }

            signatuerSH1 = sb.ToString(); 

            if(signatuerSH1 == b.signature)
            {
                var ReqPayNot = new RequestPaymentNotification();
                var Resp = RequestPaymentNotification.db().Where(x=>x.bill_no == b.bill_no && x.trx_id == b.trx_id).FirstOrDefault();
                if(Resp!=null) ReqPayNot = Resp;
                ReqPayNot.request = b.request;
                ReqPayNot.trx_id = b.trx_id;
                ReqPayNot.merchant_id = b.merchant_id;
                ReqPayNot.merchant = b.merchant;
                ReqPayNot.bill_no = b.bill_no;
                ReqPayNot.payment_reff = b.payment_reff;
                ReqPayNot.payment_date = b.payment_date;
                ReqPayNot.payment_status_code = b.payment_status_code;
                ReqPayNot.payment_status_desc = b.payment_status_desc;
                ReqPayNot.bill_total = b.bill_total;
                ReqPayNot.payment_total = b.payment_total;
                ReqPayNot.payment_channel_uid = b.payment_channel_uid;
                ReqPayNot.payment_channel = b.payment_channel; 
                ReqPayNot.signature = b.signature;

                if(Resp==null){
                    RequestPaymentNotification.Insert(ReqPayNot);
                }else{
                    RequestPaymentNotification.Update(ReqPayNot);
                }

                var ResPayNot = new ResponsePaymentNotification();
                var Resp1 = ResponsePaymentNotification.db().Where(x=>x.bill_no == b.bill_no && x.trx_id == b.trx_id).FirstOrDefault();
                if(Resp1!=null) ResPayNot = Resp1;
                ResPayNot.trx_id = b.trx_id;
                ResPayNot.merchant_id = b.merchant_id;
                ResPayNot.bill_no = b.bill_no;
                ResPayNot.response_code = "00";
                ResPayNot.response_desc = "Sukses";
                ResPayNot.response_date = DateTime.Now;

                if(Resp1==null){
                    ResponsePaymentNotification.Insert(ResPayNot);
                }else{
                    ResponsePaymentNotification.Update(ResPayNot);
                }
                
                return Ok(new {
                    response = "Payment Notification",
                    trx_id = b.trx_id,
                    merchant_id = b.merchant_id,
                    merchant = b.merchant,
                    bill_no = b.bill_no,
                    response_code = "00",
                    response_desc = "Sukses",
                    response_date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                    
                });

            }else{

                return Ok(new {
                    response_code = "55",
                    response_desc = "Otentifikasi tidak benar (SIGNATURE)"
                });
            }
            
        }

        [HttpPost("Response")]
        public IActionResult ResponseCallbackURL([FromBody] ParamResponseFaspayNotification b){


            var CheckPay = RequestPaymentNotification.db().Where(x=>x.trx_id == b.trx_id && x.merchant_id == b.merchant_id && x.bill_no == b.bill_no).FirstOrDefault();
           
            if(CheckPay != null )
            {
                var ResPayNot = new ResponsePaymentNotification();
                var Resp = ResponsePaymentNotification.db().Where(x=>x.bill_no == b.bill_no && x.trx_id == b.trx_id).FirstOrDefault();
                if(Resp!=null) ResPayNot = Resp;
                ResPayNot.trx_id = b.trx_id;
                ResPayNot.merchant_id = b.merchant_id;
                ResPayNot.bill_no = b.bill_no;
                ResPayNot.response_code = b.response_code;
                ResPayNot.response_desc = b.response_desc;
                ResPayNot.response_date = b.response_date;

                if(Resp==null){
                    ResponsePaymentNotification.Insert(ResPayNot);
                }else{
                    ResponsePaymentNotification.Update(ResPayNot);
                }
                
                return Ok(new {
                    response = "Payment Notification",
                    trx_id = CheckPay.trx_id,
                    merchant_id = CheckPay.merchant_id,
                    merchant = CheckPay.merchant,
                    bill_no = CheckPay.bill_no,
                    response_code = "00",
                    response_desc = "Sukses",
                    response_date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                    
                });
            }else{

                return Ok(new {
                    response_code = "55",
                    response_desc = "Data tidak benar"
                });
            }
        }

    }
}