using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

namespace ProjectBYB.Controllers
{
    public class ProjectBYBController : Controller
    {
        public vCustomer authUser;
        public vAuthUser vuser;
        public List<string> toEmails;
        public string accessMenu = null;
        
        public BadRequestObjectResult noPermissionResponse(){
            return BadRequest(new {
                message = "No Permission",
            });
        }
    }
}