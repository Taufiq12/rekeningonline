using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class HomeStatusDto{
    public int id { get; set; }
	[Required]
    public string HomeStatus_Type { get; set; }
    public string HomeStatus_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("HomeStatus")]
    [Route("/api")]
    public class HomeStatusController : ProjectBYBController
    {
        public HomeStatusController(IConfiguration config){}

        [HttpGet("HomeStatus")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in HomeStatus.db()
                where
				// Like
				EF.Functions.Like(s.HomeStatus_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "HomeStatus_desc") query = query.OrderByDescending(x=>x.HomeStatus_Desc);
            else query = query.OrderBy(x=>x.HomeStatus_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("HomeStatus/{id}")]
        public IActionResult Detail(int id)
        {
            var row = HomeStatus.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("HomeStatus")]
        public IActionResult Crud([FromBody] HomeStatusDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = HomeStatus.Find(b.id);
            if(first==null){
                if(HomeStatus.db().Where(x=>x.HomeStatus_Desc==b.HomeStatus_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "HomeStatus already exists"});
            }else{
                if(HomeStatus.db().Where(x=>x.id!=b.id&&x.HomeStatus_Desc==b.HomeStatus_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "HomeStatus already exists"});
            }

            var fill = new HomeStatus();
            fill.HomeStatus_Desc = b.HomeStatus_Desc;
            fill.HomeStatus_Type = b.HomeStatus_Type;
            if(first==null){
                HomeStatus.Insert(fill);
                return Ok(new {
                    message = "Add HomeStatus successful",
                });
            }
            fill.id = b.id;
            HomeStatus.Update(fill);
            return Ok(new {
                message = "Update HomeStatus successful",
            });
        }

        [HttpPost("HomeStatus/change-status")]
        public IActionResult ChangeStatus([FromBody] HomeStatusDto b){
            var row = HomeStatus.Find(b.id);
            HomeStatus.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("HomeStatus/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = HomeStatus.Find(b.id);
            HomeStatus.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}