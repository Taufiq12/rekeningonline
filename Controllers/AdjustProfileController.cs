using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Helpers;

public class AdjustProfileDto {
	public int Id { get; set; }
	public string cif { get; set; }
	public string FullName { get; set; }
	public string NickName { get; set; }
	public string Title { get; set; }
	public string PlaceOfBirth { get; set; }
	public DateTime? DateOfBirth { get; set; }
	public string Gender { get; set; }
	public string MotherName { get; set; }
	public string Country { get; set; }
	public string CountryOther { get; set; }
	public string Identity { get; set; }
	public string TypeIdentity { get; set; }
	public DateTime? StartDateIdentity { get; set; }
	//public DateTime? EndDateIdentity { get; set; }
	public string AddressIdentity { get; set; }
	public string RTIdentity { get; set; }
	public string RWIdentity { get; set; }
	public string KelIdentity { get; set; }
	public string KecIdentity { get; set; }
	public string KabIdentity { get; set; }
	public string ProvinceIdentity { get; set; }
	public string PostalCodeIdentity { get; set; }
	public string ResidenceAddress { get; set; }
	public string RTResidence { get; set; }
	public string RWResidence { get; set; }
	public string KelResidence { get; set; }
	public string KecResidence { get; set; }
	public string KabResidence { get; set; }
	public string ProvinceResidence { get; set; }
	public string PostalCodeResidence { get; set; }
	public string PhoneNumber { get; set; }
	public string MobileNumber { get; set; }
	public string FaxNumber { get; set; }
	public string Mail { get; set; }
	public string Education{ get; set; }
	public string religion { get; set; }
	public string MaterialStatus { get; set; }
	public int NumberOfDependents { get; set; }
	public string HomeStatus { get; set; }
	public int LongOccupyYear { get; set; }
	public int LongOccupyMonth { get; set; }
	public string Hobby{ get; set; }
	public string NPWP{ get; set; }
	public int? msg_id { get; set; }

	//Customer Family
	public string FamilyFullName { get; set; }
	public string FamilyRelationshipWithTheApplicant { get; set; }
	public string FamilyPhoneNumber { get; set; }
	public string FamilyAddress { get; set; }
	public string FamilyRT { get; set; }
	public string FamilyRW { get; set; }
	public string FamilyKel { get; set; }
	public string FamilyKec { get; set; }
	public string FamilyKab { get; set; }
	public string FamilyProvince { get; set; }
	public string FamilyPostalCode { get; set; }

	// //Customer Financial
	public string SourceOfIncome { get; set; }
	public string TotalIncome { get; set; }
	public string SourceOfFunds { get; set; }
	public string PurposeOfOpeningAccount { get; set; }
	public Boolean? AnotherAccount { get; set; }
	public string BankName { get; set; }

	// //Customer Jobs
	public string Jobs { get; set; }
	public string CompanyName { get; set; }
	public string Position { get; set; }
	public string BusinessField { get; set; }
	public string CompanyAddress { get; set; }
	public string RTCompany { get; set; }
	public string RWCompany { get; set; }
	public string KelCompany { get; set; }
	public string KecCompany { get; set; }
	public string KabCompany { get; set; }
	public string ProvinceCompany { get; set; }
	public string PostalCodeCompany { get; set; }
	public string PhoneNumberCompany { get; set; }
	public string FaxNumberCompany { get; set; }
	public string ExtNumberCompany1 { get; set; }
	public string ExtNumberCompany2 { get; set; }
	public int LengthOfWorkYear { get; set; }
	public int LengthOfWorkMonth { get; set; }
	public Boolean? IsDomisili { get; set; }
	public string Attachment1 { get; set; }
	public string Attachment2 { get; set; }
	public string Attachment3 { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("AdjustProfile")]
    [Route("/api")]
    public class AdjustProfileController : ProjectBYBController
    {
        public AdjustProfileController(IConfiguration config){}

        [HttpPost("AdjustProfile")]
        public IActionResult AdjustProfile([FromBody] AdjustProfileDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);
			if(!vuser.isMenu("AdjustProfile")) return noPermissionResponse();

			 //Validasi Email
            if (!RegexUtilities.IsValidEmail(b.Mail)) return BadRequest(new {message = "Mail is Invalid..!!"}); 

			var ai = new Customer();
			var first = Customer.db().Where(x=>x.Mail==authUser.Mail).FirstOrDefault();
			if(first!=null) ai = first;
			ai.FullName = authUser.FullName;
            ai.NickName = authUser.NickName;
            ai.Title = b.Title;
            ai.PlaceOfBirth = b.PlaceOfBirth;
            ai.DateOfBirth = b.DateOfBirth; 
            ai.Gender = b.Gender;
            ai.MotherName = b.MotherName;
            ai.Country = b.Country;
            ai.CountryOther = b.CountryOther;
            ai.Identity = b.Identity;
            ai.TypeIdentity = b.TypeIdentity;
            ai.StartDateIdentity = b.StartDateIdentity; 
            ai.EndDateIdentity = Convert.ToDateTime("9999-01-01"); //b.EndDateIdentity; 
            ai.AddressIdentity = b.AddressIdentity;
            ai.RTIdentity = b.RTIdentity;
            ai.RWIdentity = b.RWIdentity;
            ai.KelIdentity = b.KelIdentity;
            ai.KecIdentity = b.KecIdentity;
            ai.KabIdentity = b.KabIdentity;
            ai.ProvinceIdentity = b.ProvinceIdentity;
            ai.PostalCodeIdentity = b.PostalCodeIdentity;
            ai.ResidenceAddress = b.ResidenceAddress;
            ai.RTResidence = b.RTResidence;
            ai.RWResidence = b.RWResidence;
            ai.KelResidence = b.KelResidence;
            ai.KecResidence = b.KecResidence;
            ai.KabResidence = b.KabResidence;
            ai.ProvinceResidence = b.ProvinceResidence;
            ai.PostalCodeResidence = b.ProvinceResidence;
            ai.PhoneNumber = b.PhoneNumber;
            ai.MobileNumber = b.MobileNumber;
            ai.FaxNumber = b.FaxNumber;
            ai.Mail = authUser.Mail;
            ai.Education =b.Education;
            ai.religion = b.religion;
            ai.MaterialStatus = b.MaterialStatus;
            ai.NumberOfDependents = b.NumberOfDependents;
            ai.HomeStatus = b.HomeStatus;
            ai.LongOccupyYear = b.LongOccupyYear;
            ai.LongOccupyMonth = b.LongOccupyMonth;
            ai.Hobby = b.Hobby;
            ai.NPWP = b.NPWP;
			ai.IsDomisili = b.IsDomisili;
			ai.Created_Date = DateTime.Now;
			
			if(first==null){
				Customer.Insert(ai);
			}else{
				Customer.Update(ai);
			}

            //Customer Family
			var af = new CustomerFamily();
			var firstFam = CustomerFamily.db().Where(x=>x.Mail==authUser.Mail).FirstOrDefault();
			if(firstFam!=null) af = firstFam;
            af.Mail = authUser.Mail;
            af.FamilyFullName = b.FamilyFullName;
			af.FamilyRelationshipWithTheApplicant = b.FamilyRelationshipWithTheApplicant;
            af.FamilyPhoneNumber = b.FamilyPhoneNumber;
            af.FamilyAddress = b.FamilyAddress;
            af.FamilyRT = b.FamilyRT;
            af.FamilyRW = b.FamilyRW;
			af.FamilyKel = b.FamilyKel;
			af.FamilyKec = b.FamilyKec;
			af.FamilyKab = b.FamilyKab;
 			af.FamilyProvince = b.FamilyProvince;
			af.FamilyPostalCode = b.FamilyPostalCode;
			af.Created_Date = DateTime.Now;

			if(first==null){
				CustomerFamily.Insert(af);
			}else{
				CustomerFamily.Update(af);
			}

            //Customer Financial
            var afin = new CustomerFinancial();
			var firstFan = CustomerFinancial.db().Where(x=>x.Mail==authUser.Mail).FirstOrDefault();
			if(firstFan!=null) afin = firstFan;
            afin.Mail = authUser.Mail;
			afin.SourceOfIncome = b.SourceOfIncome;
			afin.TotalIncome = b.TotalIncome;
			afin.SourceOfFunds = b.SourceOfFunds;
			afin.PurposeOfOpeningAccount = b.PurposeOfOpeningAccount;
			afin.AnotherAccount = b.AnotherAccount;
			afin.BankName = b.BankName;
			afin.Created_Date = DateTime.Now;

			if(first==null){
				CustomerFinancial.Insert(afin);
			}else{
				CustomerFinancial.Update(afin);
			}

            //Customer Jobs
            var ajob = new CustomerJobs();
			var firstJob = CustomerJobs.db().Where(x=>x.Mail==authUser.Mail).FirstOrDefault();
			if(firstJob!=null) ajob = firstJob;
            ajob.Mail = authUser.Mail;
			ajob.Jobs = b.Jobs;
			ajob.CompanyName = b.CompanyName;
			ajob.Position = b.Position;
			ajob.BusinessField = b.BusinessField;
			ajob.CompanyAddress = b.CompanyAddress;
			ajob.RTCompany = b.RTCompany;
			ajob.RWCompany = b.RWCompany;
			ajob.KelCompany = b.KelCompany;
			ajob.KecCompany = b.KecCompany;
			ajob.KabCompany = b.KabCompany;
			ajob.ProvinceCompany = b.ProvinceCompany;
			ajob.PostalCodeCompany = b.PostalCodeCompany;
			ajob.PhoneNumberCompany = b.PhoneNumberCompany;
			ajob.FaxNumberCompany = b.FaxNumberCompany;
			ajob.ExtNumberCompany1 = b.ExtNumberCompany1;
			ajob.ExtNumberCompany2 = b.ExtNumberCompany2;
			ajob.LengthOfWorkYear = b.LengthOfWorkYear;
			ajob.LengthOfWorkMonth = b.LengthOfWorkMonth;
			ajob.Created_Date = DateTime.Now;

			if(first==null){
				CustomerJobs.Insert(ajob);
			}else{
				CustomerJobs.Update(ajob);
			}

			//Attachment
            var att = new Attachment();
			var firstAtt = Attachment.db().Where(x=>x.Mail==authUser.Mail).FirstOrDefault();
			if(firstAtt!=null) att = firstAtt;
            att.Mail = authUser.Mail;
			att.Attachment1 = b.Attachment1;
			att.Attachment2 = b.Attachment2;
			att.Attachment3 = b.Attachment3;

			att.Upload_Date = DateTime.Now;

			if(first==null){
				Attachment.Insert(att);
			}else{
				Attachment.Update(att);
			}

			//Register
			var reg = new Register();
			var firstReg = Register.db().Where(x=>x.Mail==authUser.Mail).FirstOrDefault();
			if(firstReg!=null) reg = firstReg;
            reg.Mail = authUser.Mail;
			reg.FullName = b.FullName;
			reg.NickName = b.NickName;
			reg.MobileNumber = b.MobileNumber;
			reg.DateOfBirth = b.DateOfBirth;
			reg.MotherName = b.MotherName;
			reg.Identity = b.Identity;
			reg.TypeIdentity = b.TypeIdentity;
			reg.id = firstReg.id;
			if(first==null){
				Register.Insert(reg);
			}else{
				Register.Update(reg);
			}

			return Ok(new {
				message = "Data has been submitted",
			});
        }

        [HttpGet("AdjustProfileDetail")]
        public IActionResult AdjustProfileDetail([FromQuery] string Mail){
			var row = vCustomerDetail.Find(Mail);
			if(row==null) return BadRequest(new {message = "Customer Detail"});

			var ai = row.vcd();

			return Ok(new {
				data=ai.GetData(),
			});
		}

    }
}