using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class MaterialStatusDto{
    public int id { get; set; }
	[Required]
    public string MaterialStatus_Type { get; set; }
    public string MaterialStatus_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("MaterialStatus")]
    [Route("/api")]
    public class MaterialStatusController : ProjectBYBController
    {
        public MaterialStatusController(IConfiguration config){}

        [HttpGet("MaterialStatus")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in MaterialStatus.db()
                where
				// Like
				EF.Functions.Like(s.MaterialStatus_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "MaterialStatus_desc") query = query.OrderByDescending(x=>x.MaterialStatus_Desc);
            else query = query.OrderBy(x=>x.MaterialStatus_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("MaterialStatus/{id}")]
        public IActionResult Detail(int id)
        {
            var row = MaterialStatus.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("MaterialStatus")]
        public IActionResult Crud([FromBody] MaterialStatusDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = MaterialStatus.Find(b.id);
            if(first==null){
                if(MaterialStatus.db().Where(x=>x.MaterialStatus_Desc==b.MaterialStatus_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "MaterialStatus already exists"});
            }else{
                if(MaterialStatus.db().Where(x=>x.id!=b.id&&x.MaterialStatus_Desc==b.MaterialStatus_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "MaterialStatus already exists"});
            }

            var fill = new MaterialStatus();
            fill.MaterialStatus_Desc = b.MaterialStatus_Desc;
            fill.MaterialStatus_Type = b.MaterialStatus_Type;
            if(first==null){
                MaterialStatus.Insert(fill);
                return Ok(new {
                    message = "Add MaterialStatus successful",
                });
            }
            fill.id = b.id;
            MaterialStatus.Update(fill);
            return Ok(new {
                message = "Update MaterialStatus successful",
            });
        }

        [HttpPost("MaterialStatus/change-status")]
        public IActionResult ChangeStatus([FromBody] MaterialStatusDto b){
            var row = MaterialStatus.Find(b.id);
            MaterialStatus.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("MaterialStatus/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = MaterialStatus.Find(b.id);
            MaterialStatus.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}