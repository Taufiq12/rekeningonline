using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using System.IO;
using OfficeOpenXml;
using System.Net.Http.Headers;

public class ManageAO_AkulakuDto{
    
    public int id { get; set; }

	[Required]   
    public string Name { get; set; }
    public string Title { get; set; }
    public string KTP { get; set; }
    public string Reporting  { get; set; }
    public string Kode_AO { get; set; }
    public string Kode_Referral { get; set; }
    public DateTime? Created_Date { get; set; }
    public DateTime? Updated_Date { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("ManageAO_Akulaku")]
    [Route("/api")]
    public class ManageAO_AkulakuController : ProjectBYBController
    {
        public ManageAO_AkulakuController(IConfiguration config){}

        [HttpGet("ManageAO_Akulaku")]
        public IActionResult ManageAO_Akulaku([FromQuery] ListTableDto q)
        {
            var query = from s in KodeReferance.db()
                where (EF.Functions.Like(s.Name, $"%{q.search}%") || EF.Functions.Like(s.Title, $"%{q.search}%") || EF.Functions.Like(s.KTP, $"%{q.search}%") || EF.Functions.Like(s.Reporting, $"%{q.search}%"))
                select s;
			
            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Kode_AO") query = query.OrderBy(x=>x.Kode_AO);
            else query = query.OrderBy(x=>x.Kode_AO);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("ManageAO_Akulaku/{id}")]
        public IActionResult Detail(int Id)
        {
            var row = KodeReferance.db().Where(x=>x.id==Id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("ManageAO_Akulaku")]
        public IActionResult Crud([FromBody] ManageAO_AkulakuDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = KodeReferance.Find(b.id);
            if(first==null){
                if(KodeReferance.db().Where(x=>x.Name==b.Name).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Name already exists"});
            }else{
                if(KodeReferance.db().Where(x=>x.id!=b.id&&x.Kode_AO==b.Kode_AO).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Kode AO already exists"});
            }

            //Generate Code Refferal
            // var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            // var random = new Random();
            // var result = new string(
            //     Enumerable.Repeat(chars, 8)
            //             .Select(s => s[random.Next(s.Length)])
            //             .ToArray());

            var fill = new KodeReferance();
            fill.Name = b.Name;
            fill.Title = b.Title;
            fill.KTP = b.KTP;
            fill.Reporting = b.Reporting;
            fill.Kode_AO = b.Kode_AO; 
            fill.Kode_Referral = b.Kode_Referral;
            if(first==null){
                fill.Created_Date = DateTime.Now;
                KodeReferance.Insert(fill);
                return Ok(new {
                    message = "Add Kode AO successful",
                });
            }
            fill.id = b.id;
            fill.Updated_Date = DateTime.Now;
            KodeReferance.Update(fill);
            return Ok(new {
                message = "Update Kode AO successful",
            });
        }

        [HttpPost("ManageAO_Akulaku/change-status")]
        public IActionResult ChangeStatus([FromBody] ManageAO_AkulakuDto b){
            var row = KodeReferance.Find(b.id);
            KodeReferance.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("ManageAO_Akulaku/delete")]
        public IActionResult Delete([FromBody] ManageAO_AkulakuDto b){
            var row = KodeReferance.Find(b.id);
            KodeReferance.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}