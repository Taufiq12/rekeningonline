using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Helpers;
using NETCore.Encrypt;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
public class TestAPIDto {
	    public string Mail { get; set; }
}

public class nasabahAPIDto {
	    public string nomorNasabah { get; set; }
}

public class IlumaPhoneNumber {
    public string phone_number { get; set; }
    public string given_name { get; set; }
    public string surname { get; set; }
}

public class TestAPIHMACDto {
	    public string text { get; set; }
        public string key { get; set; }
}

public class TelephoneNoDto {
	    public string TelephoneNo { get; set; }
}

public class TestAPIInquiryDto {
	    public string Mail { get; set; }
        public string trx_id { get; set; }
        public string bill_no { get; set; }
}

public class TestNumber {
    public string external_id { get; set; }
}

public class TestPaymentDto {
    public string Mail { get; set; }
    public string payment_id { get; set; }
    public string callback_virtual_account_id { get; set; }
    public DateTime transaction_timestamp { get; set; }
    public DateTime updated { get; set; }
    public DateTime created { get; set; }
}


namespace ProjectBYB.Controllers
{
    [Route("/Test")]
    public class TestController : ProjectBYBController
    {
        public TestController(IConfiguration config){}

        //Step 1

        [HttpPost("InstaMoneyCreateUser")]
        public IActionResult InstaMoneyCreateUser([FromBody] TestAPIDto b){

            var cus = Customer.db().Where(x=>x.Mail == b.Mail).FirstOrDefault();

            string MobileNumber = cus.MobileNumber;
            string CustomerName = cus.FullName;

            
            var time = Settings.db().Where(x=>x.s_slug =="time_payment").FirstOrDefault().s_values;

            int times =Convert.ToInt32(time.Replace("\"",""));

            DateTime currentTime = DateTime.Now;
            DateTime x5HoursLater = currentTime.AddMinutes(times);
            string Expirated_Date = x5HoursLater.ToString("yyyy-MM-ddTHH:mm:ssZ");
                
            var vCusDetail = vCustomerDetail.Find(b.Mail);
            decimal amount = Saving_Type.FindSavingType(vCusDetail.SavingType, vCusDetail.TypeCard).Nominal_Saldo;

            //Endpoint Create Virtual Account InstaMoney
            ApiInstaMoney.CreateRequestVirtualAccount(b.Mail, MobileNumber,"MANDIRI",CustomerName,MobileNumber,amount,Expirated_Date);

            //Update Status User Virtual Account
            // var db = new UserVirtualAccount();
            // var va = UserVirtualAccount.db().Where(a=>a.name == CustomerName && a.external_id == MobileNumber).FirstOrDefault();

            // db.owner_id = va.owner_id;
            // db.external_id = va.external_id;
            // db.bank_code = va.bank_code;
            // db.merchant_code = va.merchant_code;
            // db.name = va.name;
            // db.account_number = va.account_number;
            // db.id = va.id;
            // db.status = "ACTIVE";
            // db.expiration_date = DateTime.Now;
            // db.updated = DateTime.Now;
            // UserVirtualAccount.Update(db);

            return Ok(new {
                message = "Sukses"
            });
        }

        //Step 2

        [HttpPost("InstaMoneyPayment")]
        public IActionResult InstaMoneyPayment([FromBody] TestPaymentDto b){

            var cus = Customer.db().Where(x=>x.Mail == b.Mail).FirstOrDefault();
            var va = UserVirtualAccount.db().Where(a=>a.external_id == cus.MobileNumber && a.name == cus.FullName).FirstOrDefault();
            var emp = vCustomerDetail.Find(b.Mail);
            var sav = Saving_Type.FindSavingType(emp.SavingType,emp.TypeCard);

            var pva = new PaymentVirtualAccount();
                var first = PaymentVirtualAccount.db().Where(x=>x.id==va.id && x.owner_id == va.owner_id && x.external_id == va.external_id && x.account_number == va.account_number && x.bank_code == va.bank_code).FirstOrDefault();

                if(first!=null) pva = first;
                pva.id = va.id;
                pva.payment_id = b.payment_id;
                pva.callback_virtual_account_id = b.callback_virtual_account_id;
                pva.owner_id = va.owner_id;
                pva.external_id = va.external_id;
                pva.account_number = va.account_number;
                pva.bank_code = va.bank_code;
                pva.amount = sav.Nominal_Saldo;
                pva.transaction_timestamp = b.transaction_timestamp;
                pva.merchant_code = Convert.ToInt32(va.merchant_code);
                pva.updated = b.updated;
                pva.created = b.created;

                if(first==null){
                    PaymentVirtualAccount.Insert(pva);
                }else{
                    PaymentVirtualAccount.Update(pva);
                }

                //Log Payment
                var lpa = new LogPayment();
                var data = LogPayment.db().Where(x=>x.id == va.id && x.owner_id == va.owner_id && x.external_id == va.external_id && x.status == "Pembayaran sudah Diverifikasi").FirstOrDefault();
                if(data!=null) lpa = data;
                lpa.id = va.id;
                lpa.owner_id = va.owner_id;
                lpa.external_id = va.external_id;
                lpa.status = "Pembayaran sudah Diverifikasi";
                lpa.created = DateTime.Now;

                LogPayment.Insert(lpa);
                

                //Update Status
                var cstmr = Customer.db().Where(x=>x.MobileNumber == va.external_id).FirstOrDefault();
                cstmr.Status = "Pembayaran sudah Diverifikasi";
                Customer.Update(cus);

                //Send Mail
                var ai = Register.db().Where(x => x.MobileNumber == va.external_id).FirstOrDefault();
                Emails.SuksesTransferVA(ai);


            return Ok(new {
                message = "Sukses"
            });
        }

        //sTEP 3
        [HttpPost("Swagger")]
        public IActionResult VirtualAccountPaidCallbackURL([FromBody] TestAPIDto b){

            var cus = Customer.db().Where(x=>x.Mail == b.Mail).FirstOrDefault();
            //Proses API Swagger----------------------------------------------------------
            //Step 1
            //Enpoint Buat CIF
            // var cif = vCustomerCIF.db().Where(x=>x.namaNasabah == cus.FullName && x.noID == cus.Identity && x.namaIbuKandung == cus.MotherName && x.tanggalLahir == Convert.ToDateTime(cus.DateOfBirth).ToString("yyyy-MM-dd") && x.tempatLahir == cus.PlaceOfBirth).FirstOrDefault();
            // ApiSwagger.RequestCIF(
            //     cif.namaNasabah.Trim().ToUpper(), cif.kodeCabang.Trim().ToUpper(), cif.alamatID1.Trim().ToUpper(), cif.alamatID2.Trim().ToUpper(),
            //     cif.kelurahan.Trim().ToUpper(), cif.kecamatan.Trim().ToUpper(), cif.kota.Trim().ToUpper(), cif.kodePos.Trim().ToUpper(),
            //     cif.propinsi.Trim().ToUpper(), cif.kodeNomorTelepon.Trim().ToUpper(), cif.nomorTelepon.Trim().ToUpper(), cif.nomorHP.Trim().ToUpper(),
            //     cif.alamatEmail.Trim().ToUpper(), cif.kodeAO.Trim().ToUpper(), cif.npwp.Trim().ToUpper(), cif.jenisNasabah.Trim().ToUpper(),
            //     cif.jenisKelamin.Trim().ToUpper(), cif.agama.Trim().ToUpper(), cif.statusPerkawinan.Trim().ToUpper(), cif.pendidikanTerakhir.Trim().ToUpper(),
            //     cif.jumlahTanggungan.Trim().ToUpper(), cif.anak.Trim().ToUpper(), cif.istri.Trim().ToUpper(), cif.tempatLahir.Trim().ToUpper(),
            //     cif.tanggalLahir.Trim().ToUpper(), cif.negaraAsal.Trim().ToUpper(), cif.jenisIdentitas.Trim().ToUpper(), cif.noID.Trim().ToUpper(),
            //     cif.tanggalTerbitID.Trim().ToUpper(), cif.tanggalBerakhirID.Trim().ToUpper(), cif.jenisPekerjaan.Trim().ToUpper(), cif.sumberPenghasilan.Trim().ToUpper(),
            //     cif.penghasilanPerBulan, cif.namaBadanUsaha.Trim().ToUpper(), cif.bidangUsaha.Trim().ToUpper(), cif.namaIbuKandung.Trim().ToUpper(),
            //     cif.golonganPemilik.Trim().ToUpper(), cif.lamaBekerja.Trim().ToUpper(), cif.apuDataProfileResiko.Trim().ToUpper(), cif.apuIdentitasNasabah.Trim().ToUpper(),
            //     cif.apuLokasiUsaha.Trim().ToUpper(), cif.apuProfilNasabah.Trim().ToUpper(), cif.apuJumlahTransaksi.Trim().ToUpper(), cif.apuKegiatanUsaha.Trim().ToUpper(),
            //     cif.apuStrukturKepemilikan, cif.apuInformasiLain1.Trim().ToUpper(), cif.apuResumeAkhir.Trim().ToUpper()
            // );

            //Step 2
            //Endpoint Pembukaan Rekening
            var rek = vCustomerRekening.db().Where(x=>x.namaNasabah == cus.FullName && x.namaRekening == cus.FullName && x.Mail == cus.Mail).FirstOrDefault();
            //Sementara----------------------------------------------------------
            //string tanggalBuka = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");

            var EndDate = EndOfDay.db().FirstOrDefault();
            DateTime currentTime = EndDate.eod_Date;
            string eod = currentTime.ToString("yyyy-MM-ddTHH:mm:ssZ");

            string date;
            if (EndDate.is_eod == true)
            {
                date = eod;
            }else{ 
                date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
            }
            //date = "2019-06-05T09:06:15.565Z";
            string tanggalBuka = date;
            //-------------------------------------------------------------------
            ApiSwagger.RequestPembukaanRekening(cus.Mail.Trim().ToUpper(),rek.jenisPenggunaan.Trim().ToUpper(),rek.jenisRekening.Trim().ToUpper(),rek.kodeCabang.Trim().ToUpper(),
            rek.kodeMataUang.Trim().ToUpper(),rek.kolektibilitas.Trim().ToUpper(),rek.lokasi.Trim().ToUpper(),rek.namaNasabah.Trim().ToUpper(),rek.namaNasabah.Trim().ToUpper(),
            rek.nomorNasabah.Trim().ToUpper(),rek.sektorEkonomi.Trim().ToUpper(),tanggalBuka.Trim().ToUpper());
            //----------------------------------------------------------------------------

            return Ok(new {
                message = "Sukses"
            });
        }

        //Step Testing Email

        [HttpPost("Mail")]
        public IActionResult Mail([FromBody] TestAPIDto b){

            var cus = Customer.db().Where(x=>x.Mail == b.Mail).FirstOrDefault();

            Emails.InfoCIFCoreBanking(cus);

            return Ok(new {
                message = "Sukses"
            });
        }

        [HttpPost("OTP")]
        public IActionResult OTP([FromBody] TestAPIDto b){

            Random rnd = new Random();
            string randomNumber = (rnd.Next(100000,999999)).ToString();

            return Ok(new {
                message = randomNumber
            });
        }

        [HttpPost("Signature")]
        public IActionResult Signature([FromBody] TestAPIDto b){

            string userid = Config.User_Merchant_Id;
            string password = Config.User_Merchant_Password;
            string bill = "98765123456790";

            string testString = userid+password+bill;
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(testString);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string signatuerMD5 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            string signatuerSH1;
            byte[] temp;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(signatuerMD5));

            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
                 {
                    sb.Append(temp[i].ToString("x2"));
                  }

            signatuerSH1 = sb.ToString();

            ApiPasPay.CreateRequestVirtualAccount (
                "Transmisi Info Detil Pembelian", "50007","Faspay Trial 1", bill,"12345678",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),"Pembayaran #12345678","IDR",1000000,"302","1",
            "122","Taufik Nur","6281289026666","taufik.nur832@gmail.com",10,"Invoice No. inv-985/2017-03/1234567891",1000000,1,
            1,"00",99999,signatuerSH1
            );

            return Ok(new {
                signatuerSH1 = signatuerSH1,
                signatuerMD5 = signatuerMD5
            });
        }


        [HttpPost("EncodeFoto")]

        public IActionResult Encode([FromBody] TestAPIDto b){

            var cus = Customer.Find(b.Mail);

            //Get Attachment FILE
            var attachment = Attachment.FindMail(b.Mail);

            var attachFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", attachment.Attachment1);
            //------------------------------------------------------

            //Convert Base64encode
            byte[] fileBit = System.Text.ASCIIEncoding.ASCII.GetBytes(attachFile);
            string selfie_photo = System.Convert.ToBase64String(fileBit);

            return Ok(new {
                selfie_photo = selfie_photo
            });
        }

        [HttpPost("CallbackPayment")]

        public IActionResult CallbackPayment([FromBody] TestNumber b){

            decimal amount = UserVirtualAccount.db().Where(x=>x.external_id == b.external_id).FirstOrDefault().suggested_amount;

            ApiInstaMoney.CallbackPyment(b.external_id,amount);

            return Ok(new {
                messages = "messages"
            });
        }

        //Create VA FastPay
        [HttpPost("CreateRequestVirtualAccount")]
        public IActionResult CreateVAFaspay([FromBody] TestAPIDto b){

            string userid = Config.User_Merchant_Id;
            string password = Config.User_Merchant_Password;

            var maxWidth = 5;
            var theDate = DateTime.Now;
            var billNumber = UserVirtualAccountFasPay.db().Where(x=>x.created_date.ToString("dd-MM-yyyy") == theDate.ToString("dd-MM-yyyy")).Count() + 1;
            var bill = theDate.ToString("yyyyMMdd") + billNumber.ToString().PadLeft(maxWidth, '0');
            var bill_ref = billNumber.ToString().PadLeft(maxWidth, '0');
            var channel_MandiriVA = "802";
            var Pay_Type = "1";
                            //1: Full Settlement
                            //2: Installment
                            //3: Mixed 1 & 2
            int terminal = 10;
                            // 10. Web
                            // 20. MobApp Blackberry
                            // 21. MobApp Android
                            // 22. MobAppiOS
                            // 23. MobApp Windows
                            // 24. MobApp Symbian
                            // 30. TabApp BlackBerry
                            // 31. TabApp Android
                            // 32. TabAppiOS
                            // 33. TabApp Windows
            int payment_plan = 1;
                            // 1: Full Settlement
                            // 2: Installement
            var tenor = "00";
                            // 00: Full Payment
                            // 03: 3 months
                            // 06: 6 months
                            // 12: 12 months
            var time = Settings.db().Where(x=>x.s_slug =="time_payment").FirstOrDefault().s_values;

            int times =Convert.ToInt32(time.Replace("\"",""));

            DateTime currentTime = DateTime.Now;
            DateTime x5HoursLater = currentTime.AddMinutes(times);

            string testString = userid+password+bill;
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(testString);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string signatuerMD5 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            string signatuerSH1;
            byte[] temp;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(signatuerMD5));

            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
                 {
                    sb.Append(temp[i].ToString("x2"));
                  }

            signatuerSH1 = sb.ToString();

            var Reg = vCustomerDetail.Find(b.Mail);
            decimal amount = Saving_Type.FindSavingType(Reg.SavingType, Reg.TypeCard).Nominal_Saldo;

            ApiPasPay.CreateRequestVirtualAccount (
            "Transmisi Info Detil Pembelian",
            Config.Merchant_Id,
            Config.Merchant,
            bill,bill_ref,
            currentTime.ToString("yyyy-MM-dd HH:mm:ss"),
            x5HoursLater.ToString("yyyy-MM-dd HH:mm:ss"),
            "Pembayaran #"+bill,
            "IDR",
            amount,
            channel_MandiriVA,
            Pay_Type,
            "122",
            Reg.FullName,
            Reg.MobileNumber,
            b.Mail,
            terminal,
            "Invoice No. " + bill,
            amount,
            1,
            payment_plan,
            tenor,
            Convert.ToInt32(Config.Merchant_Id),
            signatuerSH1
            );

            return Ok(new {
                burlAddressill = "Sukses"
            });
        }


        [HttpPost("InquiryRedirectProcess")]
        public IActionResult InquiryPaymentStatus([FromBody] TestAPIInquiryDto b){

            string userid = Config.User_Merchant_Id;
            string password = Config.User_Merchant_Password;

            var Inq = UserVirtualAccountFasPay.db().Where(a=>a.Email == b.Mail && a.trx_id == b.trx_id && a.bill_no == b.bill_no).FirstOrDefault();

            string testString = userid+password+Inq.bill_no;
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(testString);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string signatuerMD5 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            string signatuerSH1;
            byte[] temp;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(signatuerMD5));

            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
                 {
                    sb.Append(temp[i].ToString("x2"));
                  }

            signatuerSH1 = sb.ToString();

            ApiPasPay.InquiryRedirectProcess("Pengecekan Status Pembayaran",Inq.trx_id,Convert.ToInt32(Config.Merchant_Id),Inq.bill_no,signatuerSH1);
            
            return Ok(new {
                message = "Sukses"
            });
        }


        [HttpPost("CancelTransaction")]
        public IActionResult CancelTransaction([FromBody] TestAPIInquiryDto b){

            string userid = Config.User_Merchant_Id;
            string password = Config.User_Merchant_Password;

            var Inq = UserVirtualAccountFasPay.db().Where(a=>a.Email == b.Mail && a.trx_id == b.trx_id && a.bill_no == b.bill_no).FirstOrDefault();

            string testString = userid+password+Inq.bill_no;
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(testString);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string signatuerMD5 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            string signatuerSH1;
            byte[] temp;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(signatuerMD5));

            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
                 {
                    sb.Append(temp[i].ToString("x2"));
                  }

            signatuerSH1 = sb.ToString();


            ApiPasPay.RequestCancelTransaction("Canceling Payment",Inq.trx_id,Convert.ToInt32(Config.Merchant_Id),Config.Merchant,Inq.bill_no,"Di Tolak Pengajuan",signatuerSH1);

            return Ok(new {
                message = "Sukses"
            });
        }

        [HttpPost("ValidasiEmail")]
        public IActionResult ValidasiEmail([FromBody] TestAPIDto b){
            string message;

            if (RegexUtilities.IsValidEmail(b.Mail))
                message = "Valid";
            else
                message = "Invalid";


            return Ok(new {
                message = message
            });
        }

        [HttpPost("validTelephoneNo")]
        public IActionResult validTelephoneNo([FromBody] TelephoneNoDto b){
            string message;

            string[] operatorCodes = new[] {"62", "628"};

            string number = b.TelephoneNo;

            if (operatorCodes.Any(number.StartsWith))
            {
                message = "Valid mobile number";

            }else{

                message = "Invalid mobile number";

            }

            return Ok(new {
                message = message
            });
        }

        [HttpPost("ValidasiAsliRI")]
        public IActionResult ValidasiAsliRI([FromBody] TestAPIDto b){
        var cus = Customer.Find(b.Mail);

            //Get Attachment FILE
            var attachment = Attachment.FindMail(b.Mail); 

            var attachFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", attachment.Attachment2);
            //------------------------------------------------------
            
            //Convert Base64encode
            byte[] fileBit = System.Text.ASCIIEncoding.ASCII.GetBytes(attachFile);
            string selfie_photo = System.Convert.ToBase64String(fileBit);
            //------------------------------------------------------
            string nik = cus.Identity;
            string name = cus.FullName;
            string birthdate = Convert.ToDateTime(cus.DateOfBirth).ToString("dd-MM-yyyy");
            string birthplace = cus.PlaceOfBirth;
            string address = cus.AddressIdentity;

            //Endpoint ASLI RI Validation E-KTP
            ApiAsliRI.ValidasiAsliRI(nik.Trim(),name.Trim(),birthdate.Trim(),birthplace.Trim(),address.Trim(),"",selfie_photo.Trim());

            return Ok(new {
                message = "Sukses"
            });

        }

        [HttpPost("IPAddress")]
        public IActionResult IPAddress([FromBody] TestAPIDto b){

            string ua = Request.Headers["User-Agent"].ToString();

            if (ua.Contains("Android"))
                ApiGetLocation.LocationBrowser(b.Mail, "Android", ua);

            if (ua.Contains("iPad"))
                ApiGetLocation.LocationBrowser(b.Mail, "iOS", ua);

            if (ua.Contains("iPhone"))
                ApiGetLocation.LocationBrowser(b.Mail, "iOS", ua);

            if (ua.Contains("Linux") && ua.Contains("KFAPWI"))
                ApiGetLocation.LocationBrowser(b.Mail, "Kindle Fire", ua);

            if (ua.Contains("RIM Tablet") || (ua.Contains("BB") && ua.Contains("Mobile")))
                ApiGetLocation.LocationBrowser(b.Mail, "Black Berry", ua);

            if (ua.Contains("Windows Phone"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows Phone", ua);

            if (ua.Contains("Mac OS"))
                ApiGetLocation.LocationBrowser(b.Mail, "Mac OS", ua);

            if (ua.Contains("Windows NT 5.1") || ua.Contains("Windows NT 5.2"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows XP", ua);

            if (ua.Contains("Windows NT 6.0"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows Vista", ua);

            if (ua.Contains("Windows NT 6.1"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows 7", ua);

            if (ua.Contains("Windows NT 6.2"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows 8", ua);

            if (ua.Contains("Windows NT 6.3"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows 8.1", ua);

            if (ua.Contains("Windows NT 10"))
                ApiGetLocation.LocationBrowser(b.Mail, "Windows 10", ua);

            if (ua.Contains("PostmanRuntime"))
                ApiGetLocation.LocationBrowser(b.Mail, "Postman", ua);
            
            return Ok(new {
                ip = "Sukses"
            });

        }
        
        [HttpPost("HMACSHA256")]
        public IActionResult HMACSHA256([FromBody] TestAPIHMACDto b){

            ASCIIEncoding encoding = new ASCIIEncoding();

            Byte[] textBytes = encoding.GetBytes(b.text);
            Byte[] keyBytes = encoding.GetBytes(b.key);

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
                hashBytes = hash.ComputeHash(textBytes);

            return Ok(new {
                HMACSHA256 =  BitConverter.ToString(hashBytes).Replace("-", "").ToLower()
            });

        }

        [HttpPost("AES256")]
        public IActionResult AES256([FromBody] TestAPIHMACDto b){

            Encoding encoding = Encoding.UTF8;

            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;

            aes.Key = encoding.GetBytes(b.key);
            aes.GenerateIV();

            ICryptoTransform AESEncrypt = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] buffer = encoding.GetBytes(b.text);

            string encryptedText = Convert.ToBase64String(AESEncrypt.TransformFinalBlock(buffer, 0, buffer.Length));

            String mac = "";

            //mac = BitConverter.ToString(HmacSHA256(Convert.ToBase64String(aes.IV) + encryptedText, b.key)).Replace("-", "").ToLower();

            var keyValues = new Dictionary<string, object>
            {
                { "iv", Convert.ToBase64String(aes.IV) },
                { "value", encryptedText },
                { "mac", mac },
            };

            return Ok(new {
                HMACSHA256 =  Convert.ToBase64String(encoding.GetBytes(JsonConvert.SerializeObject(keyValues)))
            });

        }

        [HttpPost("Settings")]
        public IActionResult Settings_([FromBody] TestAPIDto b){

            var time = Settings.db().Where(x=>x.s_slug =="time_payment").FirstOrDefault().s_values;
            int times =Convert.ToInt32(time.Replace("\"",""));

            var EndDate = EndOfDay.db().FirstOrDefault();
            DateTime currentTime = EndDate.eod_Date;
            string eod = currentTime.ToString("yyyy-MM-ddTHH:mm:ssZ");

            string date;
            if (EndDate.is_eod == true)
            {
                date = eod;
            }else{ 
                date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
            }
            //date = "2019-06-05T09:06:15.565Z";
            string tanggalBuka = date;

            DateTime currentDate = DateTime.Now;
            DateTime x5HoursLater = currentDate.AddMinutes(times);
            string Expirated_Date = x5HoursLater.ToString("yyyy-MM-dd hh:mm:ss");

            var chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$&";
            var randompass = new Random();
            var result = new string(Enumerable.Repeat(chars,12).Select(s=>s[randompass.Next(s.Length)]).ToArray());

            return Ok(new {
                Expirated_Date =  Expirated_Date,
                Lock = EndDate.is_eod,
                startdate = date,
                genpass = result
            });
        }

        
        [HttpPost("ValidasiPhoneNumber")]
        public IActionResult ValidasiPhoneNumber([FromBody] IlumaPhoneNumber b){

            ApiIluma.ValidasiPhoneNumber(b.phone_number,b.given_name,b.surname);

            return Ok(new {
                message = "Sukses"
            });
        }

        [HttpPost("InfoCIFCoreBanking")]
        public IActionResult InfoCIFCoreBanking([FromBody] TestAPIDto b){

             //Send Mail
            var ai = Customer.db().Where(x => x.Mail == b.Mail).FirstOrDefault();
            Emails.InfoCIFCoreBanking(ai);
            return Ok(new {
                message = "Sukses"
            });
        }

        [HttpPost("PembukaanCIF")]
        public IActionResult PembukaanCIF([FromBody] TestAPIDto b){

            var data = PembukaanNasabah.db().Where(x=>x.alamatEmail == b.Mail).FirstOrDefault();

            return Ok(new {
                agama = data.agama,
                alamatEmail = data.alamatEmail,
                alamatID1 = data.alamatID1,
                alamatID2 = data.alamatID2,
                anak = data.anak,
                apuDataProfileResiko = data.apuDataProfileResiko,
                apuIdentitasNasabah = data.apuIdentitasNasabah,
                apuInformasiLain1 = data.apuInformasiLain1,
                apuJumlahTransaksi = data.apuJumlahTransaksi,
                apuKegiatanUsaha = data.apuKegiatanUsaha,
                apuLokasiUsaha = data.apuLokasiUsaha,
                apuProfilNasabah = data.apuProfilNasabah,
                apuResumeAkhir = data.apuResumeAkhir,
                apuStrukturKepemilikan = data.apuStrukturKepemilikan,
                bidangUsaha = data.bidangUsaha,
                golonganPemilik = data.golonganPemilik,
                istri = data.istri,
                jenisIdentitas = data.jenisIdentitas,
                jenisKelamin = data.jenisKelamin,
                jenisNasabah = data.jenisNasabah,
                jenisPekerjaan = data.jenisPekerjaan,
                jumlahTanggungan = data.jumlahTanggungan,
                kecamatan = data.kecamatan,
                kelurahan = data.kelurahan,
                kodeAO = data.kodeAO,
                kodeCabang = data.kodeCabang,
                kodeNomorTelepon = data.kodeNomorTelepon,
                kodePos = data.kodePos,
                kota = data.kota,
                lamaBekerja = data.lamaBekerja,
                namaBadanUsaha = data.namaBadanUsaha,
                namaIbuKandung = data.namaIbuKandung,
                namaNasabah  = data.namaNasabah,
                negaraAsal = data.negaraAsal,
                noID = data.noID,
                nomorHP = data.nomorHP,
                nomorNasabah = data.nomorNasabah,
                nomorTelepon = data.nomorTelepon,
                npwp = data.npwp,
                pendidikanTerakhir = data.pendidikanTerakhir,
                penghasilanPerBulan = data.penghasilanPerBulan,
                propinsi = data.propinsi,
                request = data.request,
                response = data.response,
                statusPerkawinan = data.statusPerkawinan,
                sumberPenghasilan = data.sumberPenghasilan,
                tanggalBerakhirID = data.tanggalBerakhirID,
                tanggalLahir = data.tanggalLahir,
                tanggalTerbitID = data.tanggalTerbitID,
                tempatLahir = data.tempatLahir
            });
        }

        [HttpPost("PembukaanRekening")]
        public IActionResult Pembukaan_Rekening([FromBody] nasabahAPIDto b){

            var data = PembukaanRekening.db().Where(x=>x.nomorNasabah == b.nomorNasabah).FirstOrDefault();

            return Ok(new {

                jenisPenggunaan = data.jenisPenggunaan,
                jenisRekening = data.jenisRekening,
                kodeCabang = data.kodeCabang,
                kodeMataUang = data.kodeMataUang,
                kolektibilitas = data.kolektibilitas,
                lokasi = data.lokasi,
                namaNasabah = data.namaNasabah,
                namaRekening = data.namaRekening,
                nomorNasabah = data.nomorNasabah,
                nomorRekening = data.nomorRekening,
                request = data.request,
                response = data.response,
                sektorEkonomi = data.sektorEkonomi,
                tanggalBuka = data.tanggalBuka
                
            });
        }

        [HttpPost("CekSaldo")]
        public IActionResult CekSaldo(){

            ApiSwagger.RequestCekSaldo("EPZZ1S91","0100000099");


            return Ok(new{
                message = "Sukses"
            });
        }

    }
}