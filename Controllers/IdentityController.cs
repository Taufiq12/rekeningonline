using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class IdentityDto{
    public int id { get; set; }
	[Required]
    public string Identity_Type { get; set; }
    public string Identity_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Identity")]
    [Route("/api")]
    public class IdentityController : ProjectBYBController
    {
        public IdentityController(IConfiguration config){}

        [HttpGet("Identity")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Identity.db()
                where
				// Like
				EF.Functions.Like(s.Identity_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Identity_desc") query = query.OrderByDescending(x=>x.Identity_Desc);
            else query = query.OrderBy(x=>x.Identity_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Identity/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Identity.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Identity")]
        public IActionResult Crud([FromBody] IdentityDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Identity.Find(b.id);
            if(first==null){
                if(Identity.db().Where(x=>x.Identity_Desc==b.Identity_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Identity already exists"});
            }else{
                if(Identity.db().Where(x=>x.id!=b.id&&x.Identity_Desc==b.Identity_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Identity already exists"});
            }

            var fill = new Identity();
            fill.Identity_Desc = b.Identity_Desc;
            fill.Identity_Type = b.Identity_Type;
            if(first==null){
                Identity.Insert(fill);
                return Ok(new {
                    message = "Add Identity successful",
                });
            }
            fill.id = b.id;
            Identity.Update(fill);
            return Ok(new {
                message = "Update Identity successful",
            });
        }

        [HttpPost("Identity/change-status")]
        public IActionResult ChangeStatus([FromBody] IdentityDto b){
            var row = Identity.Find(b.id);
            Identity.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Identity/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Identity.Find(b.id);
            Identity.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}