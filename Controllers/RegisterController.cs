using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;

public class RegisterDto{
    public string Mail { get; set; }
    //public string Password { get; set; }
    public string FullName { get; set; }
    public string NickName { get; set; }
    public string MobileNumber { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public string MotherName { get; set; }
    public string Identity { get; set; }
    public string Kode_AO { get; set; }
}


namespace ProjectBYB.Controllers
{
   [Route("/api")]
    public class RegisterController : ProjectBYBController
    {
        public RegisterController(IConfiguration config){}

        [HttpPost("SignUp")]
    
        public IActionResult SignUp([FromBody] RegisterDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var query = from s in Register.db()
                select s;

            int first = query.Where(s=>s.Mail == b.Mail).Count();
            if(first!=0){
                return BadRequest(new {message = "Mail is already exists..!!"});
            }

            int firstMobile = query.Where(s=>s.MobileNumber == b.MobileNumber).Count();
            if(firstMobile!=0){
                return BadRequest(new {message = "Mobile Phone is already exists..!!"});
            }

            //Check Data Nasabah
            var checkData = Register.db().Where(a=>a.FullName == b.FullName && a.Identity == b.Identity && a.MotherName == b.MotherName && a.DateOfBirth == b.DateOfBirth).FirstOrDefault();
             if(checkData!=null){
                return BadRequest(new {message = "Data Customer is already exists..!!"});
            }

            //Check Kode Referance
            var kodeRef = KodeReferance.db().Where(x=>x.Kode_AO == b.Kode_AO).FirstOrDefault();
             if(kodeRef==null){
                return BadRequest(new {message = "Your AO Code is Wrong..!!"});
            }

            //Validasi Email
            if (!RegexUtilities.IsValidEmail(b.Mail)) return BadRequest(new {message = "Mail is Invalid..!!"}); 

            //Validasi Phone Number
            // string[] operatorCodes = new[] {"62", "628"};

            // if(b.MobileNumber!=null){
            // 	if (!operatorCodes.Any(b.MobileNumber.StartsWith)) return BadRequest(new {message = "Please Input format Nomor Ponsel: 62875949375939..!!"}); 
			// } 

            //Generate Password
            var chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$&";
            var randompass = new Random();
            var result = new string(Enumerable.Repeat(chars,12).Select(s=>s[randompass.Next(s.Length)]).ToArray());
           
            var fill = new Register();
            fill.Mail = b.Mail;
            fill.Password = Encrypt.Crypt(result);
            fill.FullName = b.FullName;
            fill.NickName = b.NickName;
            fill.MobileNumber = b.MobileNumber;
            fill.DateOfBirth = b.DateOfBirth;
            fill.MotherName = b.MotherName;
            fill.AccessLevel_Id = 1;
            fill.Identity = b.Identity;
            fill.TypeIdentity = "KTP";
            fill.Kode_Referance = b.Kode_AO;
            fill.isActive = 0;

            var AccessUser = new AccessCustomer();
                AccessUser.ae_mail = b.Mail;
                AccessUser.ae_al_id = 1;
                AccessUser.ae_status = true;

            //Mapping Core Banking
            //ApiCoreBanking.RequestCoreBanking(b.FullName,b.MobileNumber,b.Identity,b.MotherName);

            Random rnd = new Random();
            string randomNumber = (rnd.Next(100000,999999)).ToString();

            //Generate OTP
            var OTP = new OTP_Customer();
			var firstAtt = OTP_Customer.Find(b.Mail);
			if(firstAtt!=null) OTP = firstAtt;
            OTP.Mail = b.Mail;
			OTP.FullName = b.FullName;
			OTP.OTP = randomNumber;
			OTP.ctime_Stamp = DateTime.Now;
            OTP.Status = 1;

			if(firstAtt==null){
				OTP_Customer.Insert(OTP);
			}else{
				OTP_Customer.Update(OTP);
			}
            
            Register.Insert(fill);
            AccessCustomer.Insert(AccessUser);
            Emails.RegisterLogin(fill);
            return Ok(new {
                message = "Submit Success",
            });
        }

        [HttpPost("SignUp/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
			var exists = Register.db().Where(x=>x.Mail==b.Mail).FirstOrDefault();
			if(exists!=null) return BadRequest(new {
				message = "Can't remove, because related with Register"
			});
            var row = Register.Find(b.Mail);
            Register.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}