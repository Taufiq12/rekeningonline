using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProjectBYB.Middleware;
using Helpers;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net;
using System.Xml;
using Helpers;

public class TestDto{
    public string name { get; set; }
}

public class BrowserClient{
    public string Browser { get; set; }
}


public class IP2Location_IP_API
    {
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        
    }
    

namespace ProjectBYB.Controllers
{
    [Route("/api")]
    public class AuthController : ProjectBYBController
    {
        public AuthController(IConfiguration config){
        }


        [HttpPost("auth")]
        public IActionResult Logincustomer([FromBody] LoginDto x){
            if (!ModelState.IsValid) return BadRequest(ModelState);
           
            var query = from s in Register.db()
                where s.Mail == x.Mail
                select s;

            int total = query.Count();

            if (total == 0)
            {
                return BadRequest(new {message = "e-Mail or Password is wrong."});
            }

            string PasswordLogin = Register.Find(x.Mail).Password;
            var register = Register.Find(x.Mail);
            var userLogin = vAuthUser.Find(x.Mail);
            string pass = Encrypt.Decrypt(PasswordLogin);

            if(Config.ByPass != true)
            {
                //Check Password
                if(Encrypt.Decrypt(PasswordLogin) != x.password && register.isActive == 1){

                    var fill = new Register();
                    fill.Mail = x.Mail;
                    fill.id = register.id;
                    fill.Password = register.Password;
                    fill.FullName = register.FullName;
                    fill.NickName = register.NickName;
                    fill.MobileNumber = register.MobileNumber;
                    fill.DateOfBirth = register.DateOfBirth;
                    fill.MotherName = register.MotherName;
                    fill.TypeIdentity = register.TypeIdentity;
                    fill.Identity = register.Identity;
                    fill.AccessLevel_Id = register.AccessLevel_Id;
                    fill.Kode_Referance = register.Kode_Referance;
                    fill.isActive = register.isActive+1;
                    Register.Update(fill);

                    return BadRequest(new {message = "e-Mail or Password is wrong."});

                }else if (Encrypt.Decrypt(PasswordLogin) != x.password && register.isActive == 2) {

                    var fill = new Register();
                    fill.Mail = x.Mail;
                    fill.id = register.id;
                    fill.Password = register.Password;
                    fill.FullName = register.FullName;
                    fill.NickName = register.NickName;
                    fill.MobileNumber = register.MobileNumber;
                    fill.DateOfBirth = register.DateOfBirth;
                    fill.MotherName = register.MotherName;
                    fill.TypeIdentity = register.TypeIdentity;
                    fill.Identity = register.Identity;
                    fill.AccessLevel_Id = register.AccessLevel_Id;
                    fill.Kode_Referance = register.Kode_Referance;
                    fill.isActive = register.isActive+1;
                    Register.Update(fill);

                    return BadRequest(new {message = "e-Mail or Password is wrong."});

                }else if (Encrypt.Decrypt(PasswordLogin) != x.password && register.isActive == 3) {

                    var fill = new Register();
                    fill.Mail = x.Mail;
                    fill.id = register.id;
                    fill.Password = register.Password;
                    fill.FullName = register.FullName;
                    fill.NickName = register.NickName;
                    fill.MobileNumber = register.MobileNumber;
                    fill.DateOfBirth = register.DateOfBirth;
                    fill.MotherName = register.MotherName;
                    fill.TypeIdentity = register.TypeIdentity;
                    fill.Identity = register.Identity;
                    fill.AccessLevel_Id = register.AccessLevel_Id;
                    fill.Kode_Referance = register.Kode_Referance;
                    fill.isActive = register.isActive+1;
                    Register.Update(fill);

                    return BadRequest(new {message = "e-Mail or Password is wrong."});
                     
                }else if (register.isActive == 4 || register.isActive == 0){

                    var fill = new Register();
                    fill.Mail = x.Mail;
                    fill.id = register.id;
                    fill.Password = register.Password;
                    fill.FullName = register.FullName;
                    fill.NickName = register.NickName;
                    fill.MobileNumber = register.MobileNumber;
                    fill.DateOfBirth = register.DateOfBirth;
                    fill.MotherName = register.MotherName;
                    fill.TypeIdentity = register.TypeIdentity;
                    fill.Identity = register.Identity;
                    fill.AccessLevel_Id = register.AccessLevel_Id;
                    fill.Kode_Referance = register.Kode_Referance;
                    fill.isActive = 0;
                    Register.Update(fill);                    

                    string ua = Request.Headers["User-Agent"].ToString();

                    if (ua.Contains("Android"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Android", ua);

                    if (ua.Contains("iPad"))
                        ApiGetLocation.LocationBrowser(x.Mail, "iOS", ua);

                    if (ua.Contains("iPhone"))
                        ApiGetLocation.LocationBrowser(x.Mail, "iOS", ua);

                    if (ua.Contains("Linux") && ua.Contains("KFAPWI"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Kindle Fire", ua);

                    if (ua.Contains("RIM Tablet") || (ua.Contains("BB") && ua.Contains("Mobile")))
                        ApiGetLocation.LocationBrowser(x.Mail, "Black Berry", ua);

                    if (ua.Contains("Windows Phone"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows Phone", ua);

                    if (ua.Contains("Mac OS"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Mac OS", ua);

                    if (ua.Contains("Windows NT 5.1") || ua.Contains("Windows NT 5.2"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows XP", ua);

                    if (ua.Contains("Windows NT 6.0"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows Vista", ua);

                    if (ua.Contains("Windows NT 6.1"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows 7", ua);

                    if (ua.Contains("Windows NT 6.2"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows 8", ua);

                    if (ua.Contains("Windows NT 6.3"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows 8.1", ua);

                    if (ua.Contains("Windows NT 10"))
                        ApiGetLocation.LocationBrowser(x.Mail, "Windows 10", ua);

                    //Send Mail
                    Emails.notActiveAccount(register);

                    return BadRequest(new {message = "Account is not active."});
                    
                }else if (Encrypt.Decrypt(PasswordLogin) == x.password) {

                    var fill = new Register();
                    fill.Mail = x.Mail;
                    fill.id = register.id;
                    fill.Password = register.Password;
                    fill.FullName = register.FullName;
                    fill.NickName = register.NickName;
                    fill.MobileNumber = register.MobileNumber;
                    fill.DateOfBirth = register.DateOfBirth;
                    fill.MotherName = register.MotherName;
                    fill.TypeIdentity = register.TypeIdentity;
                    fill.Identity = register.Identity;
                    fill.AccessLevel_Id = register.AccessLevel_Id;
                    fill.Kode_Referance = register.Kode_Referance;
                    fill.isActive = 1;
                    Register.Update(fill);
                }
            }
            
            var token = "";
            token = AuthUser.GenerateToken(x.Mail, Request.Headers["User-Agent"].ToString());
            
            return Ok(new {
                message = "Login Success",
                token = token,
                user = userLogin.GetData(),
            });
        }

        [CustomerAuth]
        [HttpGet("profile")]
        public IActionResult profile([FromQuery] string fields)
        {
            var cif = Customer.FindMail(authUser.Mail);

            if(cif != null)
            { 
                   
            var cRek = from s in PembukaanRekening.db()
                where s.nomorNasabah == cif.cif && s.namaNasabah == cif.FullName
                select s;

            int total_ = cRek.Count();
            if(total_ > 0)
            {

            var Query =
                    from c in PembukaanRekening.db()
                    where c.nomorNasabah == cif.cif 
                    group c by new
                    {
                        c.nomorRekening,
                        c.nomorNasabah,
                    } into gcs
                    select new PembukaanRekening()
                    {
                        nomorRekening = gcs.Key.nomorRekening,
                        nomorNasabah = gcs.Key.nomorNasabah,
                    };

                    foreach (var item in Query){

                        ApiSwagger.RequestCekSaldo(item.nomorNasabah,item.nomorRekening);
                    }

                }
            }


            var respons = new JObject();
            var splitFields = fields != null ? fields.Split(",") : new string[0];
            var vuser = vAuthUser.Find(authUser.Mail);
            respons.Add("user", vuser.GetData());
            return Ok(respons);
        }

        [HttpGet("ErrorLogs")]
        public IActionResult ErrorLogs([FromQuery] ListTableDto q){
            var query = from s in ErrorException.db()
                where EF.Functions.Like(s.Source, $"%{q.search}%") || EF.Functions.Like(s.Message, $"%{q.search}%")
                    || EF.Functions.Like(s.InnerSource, $"%{q.search}%") || EF.Functions.Like(s.InnerStackTrace, $"%{q.search}%")
                select s;

            var data = query.OrderByDescending(x=>x.ErrorAt)
                .Skip(q.skip())
                .Take(q.limit)
                .ToList();

            return Ok(data);
        }

        [HttpGet("variable")]
        public IActionResult variable(){
            
            return Ok(new {
                idleTimeout = Settings.Find("idle_timeout").values()
                // lockAuthorization = Settings.Find("lock_authorization").values(),
            });
        }
        
    }
}