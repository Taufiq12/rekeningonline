using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class JobsDto{
    public int id { get; set; }
	[Required]
    public string Jobs_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Jobs")]
    [Route("/api")]
    public class JobsController : ProjectBYBController
    {
        public JobsController(IConfiguration config){}

        [HttpGet("Jobs")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Jobs.db()
                where
				// Like
				EF.Functions.Like(s.Jobs_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Jobs_desc") query = query.OrderByDescending(x=>x.Jobs_Desc);
            else query = query.OrderBy(x=>x.Jobs_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Jobs/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Jobs.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Jobs")]
        public IActionResult Crud([FromBody] JobsDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Jobs.Find(b.id);
            if(first==null){
                if(Jobs.db().Where(x=>x.Jobs_Desc==b.Jobs_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Jobs already exists"});
            }else{
                if(Jobs.db().Where(x=>x.id!=b.id&&x.Jobs_Desc==b.Jobs_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Jobs already exists"});
            }

            var fill = new Jobs();
            fill.Jobs_Desc = b.Jobs_Desc;
            if(first==null){
                Jobs.Insert(fill);
                return Ok(new {
                    message = "Add Jobs successful",
                });
            }
            fill.id = b.id;
            Jobs.Update(fill);
            return Ok(new {
                message = "Update Jobs successful",
            });
        }

        [HttpPost("Jobs/change-status")]
        public IActionResult ChangeStatus([FromBody] JobsDto b){
            var row = Jobs.Find(b.id);
            Jobs.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Jobs/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Jobs.Find(b.id);
            Jobs.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}