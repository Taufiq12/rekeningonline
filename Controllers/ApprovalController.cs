using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class ApprovalOpenAccountDto{
    [Required]
    public string Mail { get; set; }
    public string Remarks { get; set; }
    public string Status { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class ApprovalController : ProjectBYBController
    {
        public ApprovalController(IConfiguration config){}

    
        [HttpPost("ApprovalBackOffice")]
        public IActionResult ApprovalBackOffice([FromBody] ApprovalOpenAccountDto b){
            if(!ModelState.IsValid) return BadRequest(ModelState);

			var cus = Customer.Find(b.Mail);
            string MobileNumber = cus.MobileNumber;
            string CustomerName = cus.FullName;

            //Update Status
            if(b.Status == "A")
            {
                cus.Status = "Menunggu Pembayaran";
                cus.StatusType = "DONE";
                
                //Get Amount Transfer
                var time = Settings.db().Where(x=>x.s_slug =="time_payment").FirstOrDefault().s_values;

                int times =Convert.ToInt32(time.Replace("\"",""));

                DateTime currentTime = DateTime.Now;
                DateTime x5HoursLater = currentTime.AddMinutes(times);
                string Expirated_Date = x5HoursLater.ToString("yyyy-MM-ddTHH:mm:ssZ");
                
                var vCusDetail = vCustomerDetail.Find(b.Mail);
                decimal amount = Saving_Type.FindSavingType(vCusDetail.SavingType, vCusDetail.TypeCard).Nominal_Saldo;

                //Endpoint Create Virtual Account InstaMoney
                ApiInstaMoney.CreateRequestVirtualAccount(b.Mail,MobileNumber,"MANDIRI",CustomerName,MobileNumber,amount,Expirated_Date);
                
            }else{

                cus.StatusType = "REJECT";
                cus.Status = "Tidak Disetujui";

                //Send Mail
                var ai = Register.Find(b.Mail);
                Emails.TransferOpenRekeningReject(ai);
            }
            Customer.Update(cus);

            //Insert Approval Log
            var aal = new ApprovalLogs();
            aal.Mail = b.Mail;
            
            if(b.Status == "A")
            {
                aal.Status = "Menunggu Pembayaran";
            }else{
                aal.Status = "Reject";
            }

            aal.Remarks = b.Remarks;
            aal.Created_By = authUser.FullName;
            aal.Created_Date = DateTime.Now;
            ApprovalLogs.Insert(aal);

            return Ok(new {
				message = "Data has been approved",
			});

            
		}
    }
}