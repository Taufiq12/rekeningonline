using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;

public class RecoverPasswordDto{
    public string Mail { get; set; }
    public string OTP { get; set; }
    public string NewPassword { get; set; }
    public string ReEnterNewPassword { get; set; }
}

namespace ProjectBYB.Controllers
{
    [Route("/api")]
    public class RecoverPasswordController : ProjectBYBController
    {
        public RecoverPasswordController(IConfiguration config){}

        [HttpPost("RecoverPassword")]
    
        public IActionResult RecoverPassword([FromBody] RecoverPasswordDto b){
            
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var OTP = OTP_Customer.db().Where(x=>x.Mail==b.Mail && x.Status==1).FirstOrDefault();
            if (OTP != null)
            {
                var OTP_Active = OTP_Customer.db().Where(x=>x.Mail==b.Mail && x.Status==1).FirstOrDefault();
                if(OTP_Active.OTP!=b.OTP) return BadRequest(new {
                    message = "Code OTP invalid..!"
                });
            }else{
                var OTP_NotActive = OTP_Customer.db().Where(x=>x.Mail==b.Mail && x.Status==0).FirstOrDefault();
                if(OTP_NotActive.OTP==b.OTP || OTP_NotActive.OTP !=b.OTP) return BadRequest(new {
                    message = "Code OTP is Not Active..!"
                });
            }

            if(b.ReEnterNewPassword!=b.ReEnterNewPassword) return BadRequest(new {
				message = "Please check the password again.!"
			});

            var first = Register.Find(b.Mail);
            
            int id = first.id;
            string FullName = first.FullName;
            string NickName = first.NickName;
            string  MobileNumber = first.MobileNumber;
            DateTime DateOfBirth = Convert.ToDateTime(first.DateOfBirth);
            string MotherName = first.MotherName;
            string TypeIdentity = first.TypeIdentity;
            string Identity = first.Identity;
            int AccessLevel_Id = first.AccessLevel_Id;
            string Kode_Referance = first.Kode_Referance;

            var fill = new Register();
            fill.Mail = b.Mail;
            fill.FullName = FullName;
            fill.NickName = NickName;
            fill.MobileNumber = MobileNumber;
            fill.DateOfBirth = DateOfBirth;
            fill.MotherName = MotherName;
            fill.TypeIdentity = TypeIdentity;
            fill.Identity = Identity;
            fill.AccessLevel_Id = AccessLevel_Id;
            fill.Password = Encrypt.Crypt(b.NewPassword);
            fill.Kode_Referance = Kode_Referance;
            fill.isActive = 1;
            if(first==null){
                return noPermissionResponse();
            }
            fill.id = id;
            Register.Update(fill);

            //Non Active Code OTP
            var OTP_Cus = new OTP_Customer();
            OTP_Cus.Mail = b.Mail;
            OTP_Cus.FullName = OTP.FullName;
            OTP_Cus.OTP = b.OTP;
            OTP_Cus.ctime_Stamp = DateTime.Now;
            OTP_Cus.Status = 0;
            OTP_Cus.Id = OTP.Id;
            OTP_Customer.Update(OTP_Cus);

            Emails.SuccessReset(first);

            return Ok(new {
                message = "Update Password successful",
            });
        }

    }
}