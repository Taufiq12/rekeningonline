using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class BranchDto{
    public int id { get; set; }
	[Required]
    public string KodeCabang { get; set; }
    public string NamaCabang { get; set; }
    public string AlamatCabang { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Branch")]
    [Route("/api")]
    public class BranchController : ProjectBYBController
    {
        public BranchController(IConfiguration config){}

        [HttpGet("Branch")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Branch.db()
                where
				// Like
				EF.Functions.Like(s.NamaCabang, $"%{q.search}%") || EF.Functions.Like(s.KodeCabang, $"%{q.search}%") || EF.Functions.Like(s.AlamatCabang, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "NamaCabang") query = query.OrderByDescending(x=>x.KodeCabang);
            else query = query.OrderBy(x=>x.KodeCabang);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Branch/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Branch.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Branch")]
        public IActionResult Crud([FromBody] BranchDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Branch.Find(b.id);
            if(first==null){
                if(Branch.db().Where(x=>x.KodeCabang==b.KodeCabang).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Branch already exists"});
                }else{
                    if(Branch.db().Where(x=>x.id!=b.id&&x.NamaCabang==b.NamaCabang).FirstOrDefault()!=null)
                        return BadRequest(new {message = "Branch already exists"});
                }

            var fill = new Branch();
            fill.NamaCabang = b.NamaCabang;
            fill.KodeCabang = b.KodeCabang;
            fill.AlamatCabang = b.AlamatCabang;
            if(first==null){
                Branch.Insert(fill);
                return Ok(new {
                    message = "Add Branch successful",
                });
            }
            fill.id = b.id;
            Branch.Update(fill);
            return Ok(new {
                message = "Update Branch successful",
            });
        }

        [HttpPost("Branch/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Branch.Find(b.id);
            Branch.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}