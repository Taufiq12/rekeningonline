using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Helpers;
using System.IO;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

public class UploaderDto{
    public IFormFile file { get; set; }
    public string param { get; set; }
    public Boolean uniq { get; set; }
    public string path { get; set; } = "";
}

namespace ProjectBYB.Controllers

{
    [Route("/api")]
    public class UploaderController : ProjectBYBController
    {
        public UploaderController(IConfiguration config){
        }

        [HttpPost("uploader")]
        public async Task<IActionResult> Upload([FromForm] UploaderDto b)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var sFilename = b.file.FileName.Split(".").ToList();
            var ext = sFilename[sFilename.Count-1];
            sFilename.RemoveAt(sFilename.Count-1);
            var filename = String.Join("", sFilename);
            if(b.uniq) filename = Slug.GenerateSlug(filename+"-"+(new DateTimeOffset(DateTime.Now)).ToUnixTimeSeconds());
            var uploadPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", b.path, filename+"."+ext);
            
            using (System.IO.Stream stream = new FileStream(uploadPath, FileMode.Create)){
                await b.file.CopyToAsync(stream);
            }
            var filetarget = b.path+"/"+filename+"."+ext;
            var file = filename+"."+ext;
            var fileurl = Config.BaseURL+filetarget;
            
            return Ok(new { b.file.Length, filename, ext, file, filetarget, fileurl});
        }
    }
}