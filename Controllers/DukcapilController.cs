using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProjectBYB.Middleware;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Models.Tables;
using System.Net;
using RestSharp;

public class DukcapilDto{
    [Required]
    public string Mail { get; set; }
}

//Login---------------------------------------------------------
public class ResponseSuccessLogin{
        public string code { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public ResponseDataSuccessLogin data { get; set; }
}

public class ResponseDataSuccessLogin {
    public ResponseDataSuccessLoginProfile profile { get; set; }
    public string token { get; set; }
}

public class ResponseDataSuccessLoginProfile {
    public string default_office { get; set; }             
    public string default_office_name { get; set; }
    public string email { get; set; }
    public string expired_password { get; set; }
    public string full_name { get; set; }
    public string gender { get; set; }
    public string job_position { get; set; }
    public string last_login { get; set; }
    public string last_pass_changed { get; set; }
    public string last_seen { get; set; }
    public string mobile_number { get; set; }
    public string nip { get; set; }
    public string roles { get; set; }
    public string user_id { get; set; }
    public string user_status { get; set; }
}

//--------------------------------------------------------------


//Check NIK-----------------------------------------------------
public class ResponseCheckNIK {
    public Boolean ALAMAT { get; set; }
    public Boolean JENIS_KLMIN { get; set; }
    public Boolean JENIS_PKRJN { get; set; }
    public Boolean KAB_NAME { get; set; }
    public Boolean KEC_NAME { get; set; }
    public Boolean KEL_NAME { get; set; }
    public Boolean NAMA_LGKP { get; set; }
    public Boolean NAMA_LGKP_IBU { get; set; }
    public Boolean NIK { get; set; }
    public Boolean NO_KAB { get; set; }
    public Boolean NO_KEC { get; set; }
    public Boolean NO_KEL { get; set; }
    public Boolean NO_KK { get; set; }
    public Boolean NO_PROP { get; set; }
    public Boolean NO_RT { get; set; }
    public Boolean NO_RW { get; set; }
    public Boolean PROP_NAME { get; set; }
    public Boolean STATUS_KAWIN { get; set; }
    public Boolean TGL_LHR { get; set; }
    public Boolean TMPT_LHR { get; set; }
}

//--------------------------------------------------------------

namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class DukcapilController : ProjectBYBController
    {
        public DukcapilController(IConfiguration config){}

        [HttpPost("VerificationDukcapil")]
        public IActionResult VerificationDukcapil([FromBody] DukcapilDto b){
            if(!ModelState.IsValid) return BadRequest(ModelState);
            
            var cus = Customer.Find(b.Mail);
            var attachment = Attachment.FindMail(b.Mail);

            var loginDukcapil = LoginDukcapil.FindMail(authUser.Mail);
            //var loginDukcapil = LoginDukcapil.FindMail("admin@yudhabhakti.co.id");

            //API Login Dukcapil-------------------------------------
            var iPAddress = Dns.GetHostAddresses("").LastOrDefault();
            var client = new RestClient(Config.ApiLoginDukcapil);
            var request = new RestRequest(Method.POST);
            var bodyObj = new{
                email = loginDukcapil.email,
                password = loginDukcapil.password
            };

            request.AddHeader("app-token","d1U6k3c8@3p7!0L1");
            request.AddHeader("content-type", "application/json");
            request.AddJsonBody(bodyObj);

            IRestResponse resp = client.Execute(request);
            var response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseSuccessLogin>(resp.Content);

            //var response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseSuccessLogin>("{\"code\": 200,\"status\": \"success\",\"message\": \"Login Berhasil\",\"data\": {\"profile\": {\"applications\": [],\"default_office\": \"110\",\"default_office_name\": \"KANTOR PUSAT\",\"email\": \"admin@yudhabhakti.co.id\",\"expired_password\": null,\"full_name\": \"Administrator SSO\",\"gender\": null,\"job_position\": null,\"last_login\": \"27/03/2020 09:44:37\",\"last_pass_changed\": null,\"last_seen\": \"27/03/2020 09:44:37\",\"mobile_number\": null,\"nip\": null,\"roles\": null,\"user_id\": \"admin\",\"user_status\": \"1\"},\"token\": \"VTJGc2RHVmtYMS94MXk1T2xPMmhlbDRDSHhEakhUQnBsYndjNTl0WnI2aFdqcXRDQUFnMWpkaDhYWFczR2NUeXY3aWtIUTJ1dEZ3eGZ3bUhsR2F5ODJGQkRVVGFtbk1xQjdLNFFLZTR0L3llb0ZPTFNWYjJqUHBoQ25uaGtjKzA=\"},\"errors\": null,\"path\": \"/auth/user_login\"} ");
            //-------------------------------------------------------

            if(response.status == "success") {

                //API Check Dukcapil-------------------------------------s
                var IPAddress = Dns.GetHostAddresses("").LastOrDefault();
                var Client = new RestClient(Config.ApiCheckNikDukcapil);
                var Request = new RestRequest(Method.POST);
                var BodyObj = new{
                    ALAMAT = cus.AddressIdentity.ToUpper(),
                    JENIS_KLMIN = cus.Gender != "M" ? "Perempuan" : "Laki-Laki",
                    KAB_NAME = cus.KabIdentity.ToUpper(),
                    KEC_NAME = cus.KecIdentity.ToUpper(),
                    KEL_NAME = cus.KelIdentity.ToUpper(),
                    NAMA_LGKP = cus.FullName.ToUpper(),
                    NAMA_LGKP_IBU = cus.MotherName.ToUpper(),
                    NIK = cus.Identity.ToUpper(),
                    NO_RT = cus.RTIdentity.ToUpper(),
                    NO_RW = cus.RWIdentity.ToUpper(),
                    PROP_NAME = cus.ProvinceIdentity.ToUpper(),
                    TGL_LHR = cus.DateOfBirth,
                    TMPT_LHR = cus.PlaceOfBirth.ToUpper()
                };

                Request.AddHeader("app-token","d1U6k3c8@3p7!0L1");
                Request.AddHeader("token",response.data.token);
                Request.AddHeader("content-type", "application/json");
                Request.AddJsonBody(BodyObj);

                IRestResponse rest = Client.Execute(Request);
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseCheckNIK>(rest.Content);

                //var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseCheckNIK>("{\"ALAMAT\": 0,\"JENIS_KLMIN\": 1,\"JENIS_PKRJN\": 1,\"KAB_NAME\": 1,\"KEC_NAME\": 1,\"KEL_NAME\": 1,\"NAMA_LGKP\": 0,\"NAMA_LGKP_IBU\": 0,\"NIK\": 1,\"NO_KAB\": 1,\"NO_KEC\": 1,\"NO_KEL\": 1,\"NO_KK\": 1,\"NO_PROP\": 1,\"NO_RT\": 1,\"NO_RW\": 1,\"PROP_NAME\": 1,\"STATUS_KAWIN\": 1,\"TGL_LHR\": 1,\"TMPT_LHR\": 1}");
                
                return Ok(new {
					data = new {
                        alamat = result.ALAMAT,
                        jenis_kelamin = result.JENIS_KLMIN,
                        nama_kab = result.KAB_NAME,
                        nama_kec = result.KEC_NAME,
                        nama_kel = result.KEL_NAME,
                        nama_lengkap = result.NAMA_LGKP,
                        nama_ibu = result.NAMA_LGKP_IBU,
                        nik = result.NIK,
                        no_rt = result.NO_RT,
                        no_rw = result.NO_RW,
                        nama_prov = result.PROP_NAME,
                        status_kawin = result.STATUS_KAWIN,
                        tgl_lahir = result.TGL_LHR,
                        tmp_lahir = result.TMPT_LHR
					}
				});
                //-------------------------------------------------------

            }

            return Ok(new {
					data = new {
                        alamat = false,
                        jenis_kelamin = false,
                        nama_kab = false,
                        nama_kec = false,
                        nama_kel = false,
                        nama_lengkap = false,
                        nama_ibu = false,
                        nik = false,
                        no_rt = false,
                        no_rw = false,
                        nama_prov = false,
                        status_kawin = false,
                        tgl_lahir = false,
                        tmp_lahir = false,
					}
				});
		}
    }
}