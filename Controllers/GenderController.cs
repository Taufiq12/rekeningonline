using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class GenderDto{
    public int id { get; set; }
	[Required]
    public string Gender_Type { get; set; }
    public string Gender_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Gender")]
    [Route("/api")]
    public class GenderController : ProjectBYBController
    {
        public GenderController(IConfiguration config){}

        [HttpGet("Gender")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Gender.db()
                where
				// Like
				EF.Functions.Like(s.Gender_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "gender_desc") query = query.OrderByDescending(x=>x.Gender_Desc);
            else query = query.OrderBy(x=>x.Gender_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Gender/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Gender.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Gender")]
        public IActionResult Crud([FromBody] GenderDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Gender.Find(b.id);
            if(first==null){
                if(Gender.db().Where(x=>x.Gender_Desc==b.Gender_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "gender already exists"});
            }else{
                if(Gender.db().Where(x=>x.id!=b.id&&x.Gender_Desc==b.Gender_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "gender already exists"});
            }

            var fill = new Gender();
            fill.Gender_Desc = b.Gender_Desc;
            fill.Gender_Type = b.Gender_Type;
            if(first==null){
                Gender.Insert(fill);
                return Ok(new {
                    message = "Add gender successful",
                });
            }
            fill.id = b.id;
            Gender.Update(fill);
            return Ok(new {
                message = "Update gender successful",
            });
        }

        [HttpPost("Gender/change-status")]
        public IActionResult ChangeStatus([FromBody] GenderDto b){
            var row = Gender.Find(b.id);
            Gender.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Gender/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Gender.Find(b.id);
            Gender.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}