using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class CoreBankingDto{
    [Required]
    public string Mail { get; set; }
    public string MobileNumber { get; set; }
    public string Identity { get; set; }
    public string MotherName { get; set; }
    public string CIF { get; set; }

}

namespace ProjectBYB.Controllers
{
    [Route("/api")]
    public class CoreBankingController : ProjectBYBController
    {
        public CoreBankingController(IConfiguration config){}

    
        [HttpPost("CoreBanking")]
        public IActionResult ResponseCoreBankingSuccess([FromBody] CoreBankingDto b){
            if(!ModelState.IsValid) return BadRequest(ModelState);

			var fill = Customer.Find(b.Mail);
            fill.cif = b.CIF;

            Customer.Update(fill);

            //Send Mail
            var ai = Customer.Find(b.Mail);
            Emails.InfoCIFCoreBanking(ai);

            return Ok(new {
                message = "Confirmation successful for CIF Core Banking",
            });
		}
    }
}