using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace ProjectBYB.Controllers
{
    [Route("/")]
    public class IndexController : Controller
    {
        [HttpGet("{segment1?}/{segment2?}/{segment3?}")]
        public IActionResult Index(string segment1 = "none", string segment2 = "", string segment3 = "")
        {
            return View("~/wwwroot/index.cshtml");
        }
    }
}