using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class HobbyDto{
    public int id { get; set; }
	[Required]
    public string Hobby_Type { get; set; }
    public string Hobby_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Hobby")]
    [Route("/api")]
    public class HobbyController : ProjectBYBController
    {
        public HobbyController(IConfiguration config){}

        [HttpGet("Hobby")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Hobby.db()
                where
				// Like
				EF.Functions.Like(s.Hobby_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Hobby_desc") query = query.OrderByDescending(x=>x.Hobby_Desc);
            else query = query.OrderBy(x=>x.Hobby_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Hobby/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Hobby.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Hobby")]
        public IActionResult Crud([FromBody] HobbyDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Hobby.Find(b.id);
            if(first==null){
                if(Hobby.db().Where(x=>x.Hobby_Desc==b.Hobby_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Hobby already exists"});
            }else{
                if(Hobby.db().Where(x=>x.id!=b.id&&x.Hobby_Desc==b.Hobby_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Hobby already exists"});
            }

            var fill = new Hobby();
            fill.Hobby_Desc = b.Hobby_Desc;
            fill.Hobby_Type = b.Hobby_Type;
            if(first==null){
                Hobby.Insert(fill);
                return Ok(new {
                    message = "Add Hobby successful",
                });
            }
            fill.id = b.id;
            Hobby.Update(fill);
            return Ok(new {
                message = "Update Hobby successful",
            });
        }

        [HttpPost("Hobby/change-status")]
        public IActionResult ChangeStatus([FromBody] HobbyDto b){
            var row = Hobby.Find(b.id);
            Hobby.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Hobby/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Hobby.Find(b.id);
            Hobby.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}