using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;

public class PostalAreaDto{
    public int id { get; set; }
	[Required]
    public string Provinsi { get; set; }
	[Required]
    public string Kabupaten { get; set; }
    public string Kecamatan { get; set; }
    public string Kelurahan { get; set; }
    public string Kodepos { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("PostalArea")]
    [Route("/api")]
    public class PostalAreaController : ProjectBYBController
    {
        public PostalAreaController(IConfiguration config){}

        [HttpGet("PostalArea")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in PostalArea.db()
                where
				// Like
				EF.Functions.Like(s.Provinsi, $"%{q.search}%") || EF.Functions.Like(s.Kabupaten, $"%{q.search}%") ||
				EF.Functions.Like(s.Kecamatan, $"%{q.search}%") || EF.Functions.Like(s.Kelurahan, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "provinsi_desc") query = query.OrderByDescending(x=>x.Provinsi);
            else if(q.sort == "city")  query = query.OrderBy(x=>x.Kabupaten);
            else if(q.sort == "city_desc")  query = query.OrderByDescending(x=>x.Kabupaten);
            else query = query.OrderBy(x=>x.Provinsi);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("PostalArea/{id}")]
        public IActionResult Detail(int id)
        {
            var row = PostalArea.db().Where(x=>x.Id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("PostalArea")]
        public IActionResult Crud([FromBody] PostalAreaDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = PostalArea.Find(b.id);

            var fill = new PostalArea();
            fill.Provinsi = b.Provinsi.ToUpper();
            fill.Kabupaten = b.Kabupaten.ToUpper();
            fill.Kecamatan = b.Kecamatan;
            fill.Kelurahan = b.Kelurahan;
            fill.Kodepos = b.Kodepos;
            fill.Updated_Date = DateTime.Now;
            if(first==null){
                PostalArea.Insert(fill);
                return Ok(new {
                    message = "Add postal area successful",
                });
            }
            fill.Id = b.id;
            PostalArea.Update(fill);
            return Ok(new {
                message = "Update postal area successful",
            });
        }

        [HttpPost("PostalArea/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = PostalArea.Find(b.id);
            PostalArea.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}