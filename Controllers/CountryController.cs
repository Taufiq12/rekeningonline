using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class CountryDto{
    public int id { get; set; }
	[Required]
    public string Country_ID { get; set; }
    public string Country_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Country")]
    [Route("/api")]
    public class CountryController : ProjectBYBController
    {
        public CountryController(IConfiguration config){}

        [HttpGet("Country")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Country.db()
                where
				// Like
				EF.Functions.Like(s.Country_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Country_desc") query = query.OrderByDescending(x=>x.Country_Desc);
            else query = query.OrderBy(x=>x.Country_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Country/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Country.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Country")]
        public IActionResult Crud([FromBody] CountryDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Country.Find(b.id);
            if(first==null){
                if(Country.db().Where(x=>x.Country_Desc==b.Country_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Country already exists"});
            }else{
                if(Country.db().Where(x=>x.id!=b.id&&x.Country_Desc==b.Country_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Country already exists"});
            }

            var fill = new Country();
            fill.Country_Desc = b.Country_Desc;
            fill.Country_ID = b.Country_ID;
            if(first==null){
                Country.Insert(fill);
                return Ok(new {
                    message = "Add Country successful",
                });
            }
            fill.id = b.id;
            Country.Update(fill);
            return Ok(new {
                message = "Update Country successful",
            });
        }

        [HttpPost("Country/change-status")]
        public IActionResult ChangeStatus([FromBody] CountryDto b){
            var row = Country.Find(b.id);
            Country.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Country/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Country.Find(b.id);
            Country.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}