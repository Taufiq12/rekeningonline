using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class ReligionDto{
    public int id { get; set; }
	[Required]
    public string Religion_Type { get; set; }
    public string Religion_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Religion")]
    [Route("/api")]
    public class ReligionController : ProjectBYBController
    {
        public ReligionController(IConfiguration config){}

        [HttpGet("Religion")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Religion.db()
                where
				// Like
				EF.Functions.Like(s.Religion_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Religion_desc") query = query.OrderByDescending(x=>x.Religion_Desc);
            else query = query.OrderBy(x=>x.Religion_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Religion/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Religion.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Religion")]
        public IActionResult Crud([FromBody] ReligionDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Religion.Find(b.id);
            if(first==null){
                if(Religion.db().Where(x=>x.Religion_Desc==b.Religion_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Religion already exists"});
            }else{
                if(Religion.db().Where(x=>x.id!=b.id&&x.Religion_Desc==b.Religion_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Religion already exists"});
            }

            var fill = new Religion();
            fill.Religion_Desc = b.Religion_Desc;
            fill.Religion_Type = b.Religion_Type;
            if(first==null){
                Religion.Insert(fill);
                return Ok(new {
                    message = "Add Religion successful",
                });
            }
            fill.id = b.id;
            Religion.Update(fill);
            return Ok(new {
                message = "Update Religion successful",
            });
        }

        [HttpPost("Religion/change-status")]
        public IActionResult ChangeStatus([FromBody] ReligionDto b){
            var row = Religion.Find(b.id);
            Religion.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Religion/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Religion.Find(b.id);
            Religion.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}