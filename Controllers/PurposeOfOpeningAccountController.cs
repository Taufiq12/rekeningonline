using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class PurposeOfOpeningAccountDto{
    public int id { get; set; }
	[Required]
    public string PurposeOfOpeningAccount_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("PurposeOfOpeningAccount")]
    [Route("/api")]
    public class PurposeOfOpeningAccountController : ProjectBYBController
    {
        public PurposeOfOpeningAccountController(IConfiguration config){}

        [HttpGet("PurposeOfOpeningAccount")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in PurposeOfOpeningAccount.db()
                where
				// Like
				EF.Functions.Like(s.PurposeOfOpeningAccount_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "PurposeOfOpeningAccount_desc") query = query.OrderByDescending(x=>x.PurposeOfOpeningAccount_Desc);
            else query = query.OrderBy(x=>x.PurposeOfOpeningAccount_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("PurposeOfOpeningAccount/{id}")]
        public IActionResult Detail(int id)
        {
            var row = PurposeOfOpeningAccount.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("PurposeOfOpeningAccount")]
        public IActionResult Crud([FromBody] PurposeOfOpeningAccountDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = PurposeOfOpeningAccount.Find(b.id);
            if(first==null){
                if(PurposeOfOpeningAccount.db().Where(x=>x.PurposeOfOpeningAccount_Desc==b.PurposeOfOpeningAccount_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "PurposeOfOpeningAccount already exists"});
            }else{
                if(PurposeOfOpeningAccount.db().Where(x=>x.id!=b.id&&x.PurposeOfOpeningAccount_Desc==b.PurposeOfOpeningAccount_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "PurposeOfOpeningAccount already exists"});
            }

            var fill = new PurposeOfOpeningAccount();
            fill.PurposeOfOpeningAccount_Desc = b.PurposeOfOpeningAccount_Desc;
            if(first==null){
                PurposeOfOpeningAccount.Insert(fill);
                return Ok(new {
                    message = "Add PurposeOfOpeningAccount successful",
                });
            }
            fill.id = b.id;
            PurposeOfOpeningAccount.Update(fill);
            return Ok(new {
                message = "Update PurposeOfOpeningAccount successful",
            });
        }

        [HttpPost("PurposeOfOpeningAccount/change-status")]
        public IActionResult ChangeStatus([FromBody] PurposeOfOpeningAccountDto b){
            var row = PurposeOfOpeningAccount.Find(b.id);
            PurposeOfOpeningAccount.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("PurposeOfOpeningAccount/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = PurposeOfOpeningAccount.Find(b.id);
            PurposeOfOpeningAccount.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}