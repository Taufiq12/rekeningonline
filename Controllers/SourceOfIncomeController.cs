using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class SourceOfIncomeDto{
    public int id { get; set; }
	[Required]
    public string SourceOfIncome_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("SourceOfIncome")]
    [Route("/api")]
    public class SourceOfIncomeController : ProjectBYBController
    {
        public SourceOfIncomeController(IConfiguration config){}

        [HttpGet("SourceOfIncome")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in SourceOfIncome.db()
                where
				// Like
				EF.Functions.Like(s.SourceOfIncome_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "SourceOfIncome_desc") query = query.OrderByDescending(x=>x.SourceOfIncome_Desc);
            else query = query.OrderBy(x=>x.SourceOfIncome_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("SourceOfIncome/{id}")]
        public IActionResult Detail(int id)
        {
            var row = SourceOfIncome.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("SourceOfIncome")]
        public IActionResult Crud([FromBody] SourceOfIncomeDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = SourceOfIncome.Find(b.id);
            if(first==null){
                if(SourceOfIncome.db().Where(x=>x.SourceOfIncome_Desc==b.SourceOfIncome_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "SourceOfIncome already exists"});
            }else{
                if(SourceOfIncome.db().Where(x=>x.id!=b.id&&x.SourceOfIncome_Desc==b.SourceOfIncome_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "SourceOfIncome already exists"});
            }

            var fill = new SourceOfIncome();
            fill.SourceOfIncome_Desc = b.SourceOfIncome_Desc;
            if(first==null){
                SourceOfIncome.Insert(fill);
                return Ok(new {
                    message = "Add SourceOfIncome successful",
                });
            }
            fill.id = b.id;
            SourceOfIncome.Update(fill);
            return Ok(new {
                message = "Update SourceOfIncome successful",
            });
        }

        [HttpPost("SourceOfIncome/change-status")]
        public IActionResult ChangeStatus([FromBody] SourceOfIncomeDto b){
            var row = SourceOfIncome.Find(b.id);
            SourceOfIncome.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("SourceOfIncome/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = SourceOfIncome.Find(b.id);
            SourceOfIncome.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}