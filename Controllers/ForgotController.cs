using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProjectBYB.Models.Tables;
using System;

public class ForgotDto{
    public string Mail { get; set; }
}


namespace ProjectBYB.Controllers
{
   [Route("/api")]
    public class ForgotController : ProjectBYBController
    {
        public ForgotController(IConfiguration config){}

        [HttpPost("Forgot")]
    
        public IActionResult Forgot([FromBody] ForgotDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var ai = Register.Find(b.Mail);

            if(ai==null)
            {
                return BadRequest(new {message = "e-Mail is wrong."});
            }

            Random rnd = new Random();
            string randomNumber = (rnd.Next(100000,999999)).ToString();

            //Generate OTP
            var OTP = new OTP_Customer();
			var firstAtt = OTP_Customer.Find(b.Mail);
			if(firstAtt!=null) OTP = firstAtt;
            OTP.Mail = b.Mail;
			OTP.FullName = ai.FullName;
			OTP.OTP = randomNumber;
			OTP.ctime_Stamp = DateTime.Now;
            OTP.Status = 1;

			if(firstAtt==null){
				OTP_Customer.Insert(OTP);
			}else{
				OTP_Customer.Update(OTP);
			}

            Emails.ResetPassword(ai);

            return Ok(new {
                message = "Submit Success",
            });

        }
    }
}