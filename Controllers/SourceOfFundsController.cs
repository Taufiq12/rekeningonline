using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class SourceOfFundsDto{
    public int id { get; set; }
	[Required]
    public string SourceOfFunds_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("SourceOfFunds")]
    [Route("/api")]
    public class SourceOfFundsController : ProjectBYBController
    {
        public SourceOfFundsController(IConfiguration config){}

        [HttpGet("SourceOfFunds")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in SourceOfFunds.db()
                where
				// Like
				EF.Functions.Like(s.SourceOfFunds_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "SourceOfFunds_desc") query = query.OrderByDescending(x=>x.SourceOfFunds_Desc);
            else query = query.OrderBy(x=>x.SourceOfFunds_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("SourceOfFunds/{id}")]
        public IActionResult Detail(int id)
        {
            var row = SourceOfFunds.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("SourceOfFunds")]
        public IActionResult Crud([FromBody] SourceOfFundsDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = SourceOfFunds.Find(b.id);
            if(first==null){
                if(SourceOfFunds.db().Where(x=>x.SourceOfFunds_Desc==b.SourceOfFunds_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "SourceOfFunds already exists"});
            }else{
                if(SourceOfFunds.db().Where(x=>x.id!=b.id&&x.SourceOfFunds_Desc==b.SourceOfFunds_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "SourceOfFunds already exists"});
            }

            var fill = new SourceOfFunds();
            fill.SourceOfFunds_Desc = b.SourceOfFunds_Desc;
            if(first==null){
                SourceOfFunds.Insert(fill);
                return Ok(new {
                    message = "Add SourceOfFunds successful",
                });
            }
            fill.id = b.id;
            SourceOfFunds.Update(fill);
            return Ok(new {
                message = "Update SourceOfFunds successful",
            });
        }

        [HttpPost("SourceOfFunds/change-status")]
        public IActionResult ChangeStatus([FromBody] SourceOfFundsDto b){
            var row = SourceOfFunds.Find(b.id);
            SourceOfFunds.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("SourceOfFunds/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = SourceOfFunds.Find(b.id);
            SourceOfFunds.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}