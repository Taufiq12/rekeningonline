using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProjectBYB.Middleware;
using System.Linq;
using ProjectBYB.Models.Tables;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Helpers;

namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class SelectsController : ProjectBYBController
    {
        public SelectsController(IConfiguration config){
        }

        [HttpGet("level-select")]
        public IActionResult LevelSelect()
        {
            var data = AccessLevel.db().Select(x=>new{x.al_id,x.al_name}).ToList();   
            return Ok(new {
                data = data
            });
        }

        [HttpGet("gender-select")]
        public IActionResult GenderSelect()
        {
            var data = new List<JObject>();
            Gender.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("SavingType-select")]
        public IActionResult SavingTypeSelect()
        {
            var result = Saving_Type.db().GroupBy(x=> x.SavingType)
                        .Select(grp => grp.First())
                        .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("TypeCard-select")]
        
        public IActionResult TypeCardSelect([FromQuery] string SavingType = "")
        {
            var result = Saving_Type.db().Where(a=> a.SavingType == SavingType).GroupBy(x => x.TypeCard)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("province-select")]
        public IActionResult ProvinceSelect()
        {

            var result = PostalArea.db().GroupBy(x => x.Provinsi)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("kabupaten-select")]
        
        public IActionResult KabupatenSelect([FromQuery] string Province = "")
        {
            var result = PostalArea.db().Where(a=> a.Provinsi == Province).GroupBy(x => x.Kabupaten)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Kecamatan-select")]
        
        public IActionResult KecamatanSelect([FromQuery] string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab).GroupBy(x => x.Kecamatan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Kelurahan-select")]
        
        public IActionResult KelurahanSelect([FromQuery] string Kec = "", string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kecamatan == Kec && a.Kabupaten == Kab).GroupBy(x => x.Kelurahan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KodePos-select")]
        
        public IActionResult KodePosSelect([FromQuery] string Kab="",string Kec="", string Kel = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab && a.Kecamatan == Kec && a.Kelurahan == Kel).GroupBy(x => x.Kodepos)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        //Family
        [HttpGet("provinceFamily-select")]
        public IActionResult ProvinceFamilySelect()
        {
            var result = PostalArea.db().GroupBy(x => x.Provinsi)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("kabupatenFamily-select")]
        
        public IActionResult KabupatenFamilySelect([FromQuery] string Province = "")
        {
            var result = PostalArea.db().Where(a=> a.Provinsi == Province).GroupBy(x => x.Kabupaten)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KecamatanFamily-select")]
        
        public IActionResult KecamatanFamilySelect([FromQuery] string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab).GroupBy(x => x.Kecamatan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KelurahanFamily-select")]
        
        public IActionResult KelurahanFamilySelect([FromQuery] string Kec = "", string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kecamatan == Kec && a.Kabupaten == Kab).GroupBy(x => x.Kelurahan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KodePosFamily-select")]
        
        public IActionResult KodePosFamilySelect([FromQuery] string Kab="",string Kec="", string Kel = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab && a.Kecamatan == Kec && a.Kelurahan == Kel).GroupBy(x => x.Kodepos)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        //Domisili
        [HttpGet("provinceDomisili-select")]
        public IActionResult ProvinceDomisiliSelect()
        {
            var result = PostalArea.db().GroupBy(x => x.Provinsi)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("kabupatenDomisili-select")]
        
        public IActionResult KabupatenDomisiliSelect([FromQuery] string Province = "")
        {
            var result = PostalArea.db().Where(a=> a.Provinsi == Province).GroupBy(x => x.Kabupaten)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KecamatanDomisili-select")]
        
        public IActionResult KecamatanDomisiliSelect([FromQuery] string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab).GroupBy(x => x.Kecamatan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KelurahanDomisili-select")]
        public IActionResult KelurahanDomisiliSelect([FromQuery] string Kec = "", string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kecamatan == Kec && a.Kabupaten == Kab).GroupBy(x => x.Kelurahan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KodePosDomisili-select")]
        
        public IActionResult KodePosDomisiliSelect([FromQuery] string Kab="",string Kec="", string Kel = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab && a.Kecamatan == Kec && a.Kelurahan == Kel).GroupBy(x => x.Kodepos)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        //Jobs
        [HttpGet("provinceJobs-select")]
        public IActionResult ProvinceJobsSelect()
        {
            var result = PostalArea.db().GroupBy(x => x.Provinsi)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("kabupatenJobs-select")]
        
        public IActionResult KabupatenJobsSelect([FromQuery] string Province = "")
        {
            var result = PostalArea.db().Where(a=> a.Provinsi == Province).GroupBy(x => x.Kabupaten)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KecamatanJobs-select")]
        
        public IActionResult KecamatanJobsSelect([FromQuery] string Kab = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab).GroupBy(x => x.Kecamatan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KelurahanJobs-select")]
        
        public IActionResult KelurahanJobsSelect([FromQuery] string Kec = "", string Kab= "")
        {
            var result = PostalArea.db().Where(a=> a.Kecamatan == Kec && a.Kabupaten == Kab).GroupBy(x => x.Kelurahan)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("KodePosJobs-select")]
        
        public IActionResult KodePosJobsSelect([FromQuery] string Kab="",string Kec="", string Kel = "")
        {
            var result = PostalArea.db().Where(a=> a.Kabupaten == Kab && a.Kecamatan == Kec && a.Kelurahan == Kel).GroupBy(x => x.Kodepos)
                   .Select(grp => grp.First())
                   .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("country-select")]
        public IActionResult countrySelect()
        {
            var data = new List<JObject>();
            Country.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Identity-select")]
        public IActionResult IdentitySelect()
        {
            var data = new List<JObject>();
            Identity.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Education-select")]
        public IActionResult EducationSelect()
        {
            var data = new List<JObject>();
            Education.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("MaterialStatus-select")]
        public IActionResult MaterialStatusSelect()
        {
            var data = new List<JObject>();
            MaterialStatus.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("HomeStatus-select")]
        public IActionResult HomeStatusSelect()
        {
            var data = new List<JObject>();
            HomeStatus.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Hobby-select")]
        public IActionResult HobbySelect()
        {
            var data = new List<JObject>();
            Hobby.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Jobs-select")]
        public IActionResult JobsSelect()
        {
            var data = new List<JObject>();
            Jobs.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("SourceOfIncome-select")]
        public IActionResult SourceOfIncomeSelect()
        {
            var data = new List<JObject>();
            SourceOfIncome.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("TotalIncome-select")]
        public IActionResult TotalIncomeSelect()
        {
            var data = new List<JObject>();
            TotalIncome.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("PurposeOfOpeningAccount-select")]
        public IActionResult PurposeOfOpeningAccountSelect()
        {
            var data = new List<JObject>();
            PurposeOfOpeningAccount.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("PostalArea-select")]
        public IActionResult PostalAreaSelect()
        {
            var data = new List<JObject>();
            PostalArea.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Branch-select")]
        public IActionResult BranchSelect()
        {
            var data = new List<JObject>();
            Branch.db().ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("BadanUsaha-select")]
        public IActionResult BadanUsahaSelect()
        {
            var result = BadanUsaha.db().GroupBy(x=> x.id)
                        .Select(grp => grp.First())
                        .ToList();

            var data = new List<JObject>();
            result.ToList().ForEach(x=>{
				data.Add(x.GetData());
			});
            return Ok(new {
                data = data
            });
        }

        [HttpGet("Role-select")]
        public IActionResult RoleSelect()
        {
            var data = new List<JObject>();
                        AccessLevel.db().Where(x=>x.al_name != "Customer").ToList().ForEach(x=>{
                            data.Add(x.GetData());
                        });
            return Ok(new {
                data = data
            });
        }

    }
}