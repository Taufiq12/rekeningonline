using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Helpers;

public class InstaMoneyUserDto {
	    public string owner_id { get; set; }
        public string external_id { get; set; }
        public string bank_code { get; set; }
        public string merchant_code { get; set; }
        public string name { get; set; }
        public string account_number { get; set; }
        public bool is_single_use { get; set; }
        public string status { get; set; }
        public DateTime expiration_date { get; set; }
        public bool is_closed { get; set; }
        public string id { get; set; }
        public decimal suggested_amount { get; set; }
        public DateTime updated { get; set; }
        public DateTime created { get; set; }
}


namespace ProjectBYB.Controllers
{
    [Route("/user")]
    public class InstaMoneyUserController : ProjectBYBController
    {
        public InstaMoneyUserController(IConfiguration config){}

        [HttpPost("virtual_account_paid_callback_url")]
        public IActionResult VirtualAccountPaidCallbackURL([FromBody] InstaMoneyUserDto b){

            //Credentials
            string username = Config.ApiAuthInstaMoney;
            string password = "";
            string svcCredentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

            var ResponseToken = Request.Headers["x-callback-token"].ToString();

            if (Config.AuthCallbackToken == ResponseToken)
            {

                var Email = Register.db().Where(x=> x.MobileNumber == b.external_id).FirstOrDefault(); 

                var va = new UserVirtualAccount();
                var first = UserVirtualAccount.db().Where(x=>x.id == b.id && x.owner_id==b.owner_id && x.external_id == b.external_id && x.bank_code == b.bank_code && x.merchant_code == Convert.ToInt32(b.merchant_code) && x.name == b.name && x.account_number == b.account_number).FirstOrDefault();
                if(first!=null) va = first;
                va.id = b.id;
                va.owner_id = b.owner_id;
                va.external_id = b.external_id;
                va.Mail = Email.Mail;
                va.merchant_code = Convert.ToInt32(b.merchant_code);
                va.account_number = b.account_number;
                va.bank_code = b.bank_code;
                va.name = b.name;
                va.is_closed = b.is_closed;  
                va.is_single_use = b.is_single_use;
                va.status = b.status;
                va.expiration_date = b.expiration_date;
                va.suggested_amount = b.suggested_amount;
                va.updated = b.updated;
                va.created = b.created;

                if(first==null){
                    UserVirtualAccount.Insert(va);
                }else{
                    UserVirtualAccount.Update(va);
                }

                //Log Payment 
                var lpa = new LogPayment();
                var data = LogPayment.db().Where(x=>x.owner_id == b.owner_id && x.external_id == b.external_id && x.status == "Menunggu Pembayaran").FirstOrDefault();
                if(data!=null) lpa = data;
                lpa.id = b.id;
                lpa.owner_id = b.owner_id;
                lpa.external_id = b.external_id;
                lpa.status = "Menunggu Pembayaran";
                lpa.created = DateTime.Now;

                LogPayment.Insert(lpa);
                
   
                return Ok(new {
                    id = b.id,
                    owner_id = b.owner_id,
                    external_id = b.external_id,
                    merchant_code = b.merchant_code,
                    account_number = b.account_number,
                    bank_code = b.bank_code,
                    name = b.name,
                    is_closed = b.is_closed,
                    is_single_use = b.is_single_use,
                    status = b.status,
                    expiration_date = b.expiration_date,
                    updated = b.updated,
                    created = b.created
                    
                });

            }else{
                return Ok(new {
                    StatusCode = 401,
                    message = "Unauthorized"
                    
                });
            }
        }

    }
}