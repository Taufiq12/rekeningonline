using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class WorkingFacilitySetupDto{
    [Required]
    public string ai_request_number { get; set; }
    public int ai_mjt_id { get; set; }
    public int mrf_id { get; set; }
    public string ai_cwid { get; set; }
	public List<JObject> fs { get; set; }
	public List<JObject> workingFacilitiesActive { get; set; }
	public List<JObject> fsDiff { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class InboxController : ProjectBYBController
    {
        public InboxController(IConfiguration config){}

        public Boolean inboxRole(vInbox v){
			if(v==null) return false;
			if(v.status=="DONE") return false;
			if(v.statusType=="BOADMIN"){
				return vCustomerDetail.db().Where(x=>x.Mail==v.Mail).FirstOrDefault() != null;
			}
            if(v.statusType!="BOADMIN"){
                return vCustomerDetail.db().Where(a=>a.Mail==v.Mail).FirstOrDefault() != null;
            }

            if(v.statusType=="BOSPV"){
				return vCustomerDetail.db().Where(x=>x.Mail==v.Mail).FirstOrDefault() != null;
			}
            if(v.statusType!="BOSPV"){
                return vCustomerDetail.db().Where(a=>a.Mail==v.Mail).FirstOrDefault() != null;
            }

			return v.toMail() == vuser.Mail;
		}

		public Boolean inboxRole(vCustomerDetail v){
			return inboxRole(vInbox.db().Where(x=>x.Mail==v.Mail).FirstOrDefault());
		}
        public Boolean searchLogic(vInbox v, ListTableDto q){
			if(EF.Functions.Like(v.Mail, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.FullName, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.Identity, $"%{q.search}%")) return true;
			return EF.Functions.Like(v.AddressIdentity, $"%{q.search}%");
		}

        public static Object InboxDetail(vCustomerDetail row){
			var emp = row.toCus(row.Mail);
			var data = Gen.Combine(row.GetData(), new {});

            return new {
                row = row == null ? new {} : data.ToObject<dynamic>(),
            };
        }

		
        [HttpGet("Inbox")]
        public IActionResult Get([FromQuery] ListTableDto q){
			if(!vuser.isMenu("Inbox")) return noPermissionResponse();

            int total;
            var query = vInbox.db();
            if(authUser.al_name == "Customer" || authUser.al_name == "Admin" )
            {
                query = from s in vInbox.dbCus()
                where searchLogic(s, q) && s.Mail == vuser.Mail
                select s;
			
			    query = query.Where(x=>inboxRole(x));

                total = query.Count();
            }else if (authUser.al_name == "Admin Back Office"){
                query = from s in vInbox.db()
                where searchLogic(s, q) && s.status == "Menunggu Verifikasi" && s.statusType == "BOADMIN"
                select s;
			
			    query = query.Where(x=>inboxRole(x));

                total = query.Count();
            }else{

                query = from s in vInbox.db()
                where searchLogic(s, q) && s.status == "Menunggu Verifikasi" && s.statusType == "BOSPV"
                select s;
			
			    query = query.Where(x=>inboxRole(x));

                total = query.Count();
            }

            var data = new List<JObject>();
            query.OrderByDescending(x=>x.Id)
                .Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
                
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }
        
        //Data Customer
        [HttpGet("ApprovalBackOffice/{Mail}")]
        public IActionResult ApprovalBackOfficeDetail(string Mail){
			var row = vCustomerDetail.Find(Mail);
			if(row==null) return noPermissionResponse();
			if(!detailRole(row)) return noPermissionResponse();
             
            return Ok(InboxController.InboxDetail(row));
        }

        [HttpGet("ApprovalBackOffice2/{Mail}")]
        public IActionResult ApprovalBackOffice2Detail(string Mail){
			var row = vCustomerDetail.Find(Mail);
			if(row==null) return noPermissionResponse();
			if(!detailRole(row)) return noPermissionResponse();
             
            return Ok(InboxController.InboxDetail(row));
        }

        public Boolean detailRole(vCustomerDetail row){
			if(row.Mail==vuser.Mail) return true;
			return inboxRole(row);
		}
    }
}