using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class EducationDto{
    public int id { get; set; }
	[Required]
    public string Education_Type { get; set; }
    public string Education_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Education")]
    [Route("/api")]
    public class EducationController : ProjectBYBController
    {
        public EducationController(IConfiguration config){}

        [HttpGet("Education")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Education.db()
                where
				// Like
				EF.Functions.Like(s.Education_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Education_desc") query = query.OrderByDescending(x=>x.Education_Desc);
            else query = query.OrderBy(x=>x.Education_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Education/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Education.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Education")]
        public IActionResult Crud([FromBody] EducationDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Education.Find(b.id);
            if(first==null){
                if(Education.db().Where(x=>x.Education_Desc==b.Education_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Education already exists"});
            }else{
                if(Education.db().Where(x=>x.id!=b.id&&x.Education_Desc==b.Education_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Education already exists"});
            }

            var fill = new Education();
            fill.Education_Desc = b.Education_Desc;
            fill.Education_Type = b.Education_Type;
            if(first==null){
                Education.Insert(fill);
                return Ok(new {
                    message = "Add Education successful",
                });
            }
            fill.id = b.id;
            Education.Update(fill);
            return Ok(new {
                message = "Update Education successful",
            });
        }

        [HttpPost("Education/change-status")]
        public IActionResult ChangeStatus([FromBody] EducationDto b){
            var row = Education.Find(b.id);
            Education.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("Education/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Education.Find(b.id);
            Education.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}