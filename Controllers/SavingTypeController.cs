using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class Saving_TypeDto{
    public int id { get; set; }
	[Required]
    public string TypeCard { get; set; }
    public string TypeName { get; set; }
    public string SavingType { get; set; }
    public decimal Nominal_Saldo { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("Saving_Type")]
    [Route("/api")]
    public class Saving_TypeController : ProjectBYBController
    {
        public Saving_TypeController(IConfiguration config){}

        [HttpGet("Saving_Type")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in Saving_Type.db()
                where
				// Like
				EF.Functions.Like(s.TypeName, $"%{q.search}%") || EF.Functions.Like(s.SavingType, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "TypeName") query = query.OrderByDescending(x=>x.TypeName);
            else query = query.OrderBy(x=>x.TypeName);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("Saving_Type/{id}")]
        public IActionResult Detail(int id)
        {
            var row = Saving_Type.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("Saving_Type")]
        public IActionResult Crud([FromBody] Saving_TypeDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = Saving_Type.Find(b.id);
            if(first==null){
                if(Saving_Type.db().Where(x=>x.id!=b.id&&x.TypeCard==b.TypeCard&&x.TypeName==b.TypeName).FirstOrDefault()!=null)
                    return BadRequest(new {message = "Saving Type already exists"});
            }

            var fill = new Saving_Type();
            fill.TypeCard = b.TypeCard;
            fill.TypeName = b.TypeName;
            fill.SavingType = b.SavingType;
            fill.Nominal_Saldo = b.Nominal_Saldo;
            if(first==null){
                Saving_Type.Insert(fill);
                return Ok(new {
                    message = "Add Saving Type successful",
                });
            }
            fill.id = b.id;
            Saving_Type.Update(fill);
            return Ok(new {
                message = "Update Saving Type successful",
            });
        }

        [HttpPost("Saving_Type/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = Saving_Type.Find(b.id);
            Saving_Type.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}