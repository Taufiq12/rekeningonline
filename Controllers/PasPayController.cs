using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class PasPayDto{
    [Required]
    public string Mail { get; set; }
    public string MobileNumber { get; set; }
    public string VA_Number { get; set; }
    public string Status { get; set; }

    public string FullName { get; set; }
    public string Identity { get; set; }
    public string MotherName { get; set; }

}

namespace ProjectBYB.Controllers
{
    [Route("/api")]
    public class PasPayController : ProjectBYBController
    {
        public PasPayController(IConfiguration config){}

    
        [HttpPost("PasPay")]
        public IActionResult ResponsePasPaySuccess([FromBody] PasPayDto b){
            if(!ModelState.IsValid) return BadRequest(ModelState);

			var PasPay = PasPay_VA.Find(b.Mail);
            PasPay.Status = b.Status;

            PasPay_VA.Update(PasPay);

            //Send Mail
            var ai = Register.Find(b.Mail);
            Emails.SuksesTransferVA(ai);


            return Ok(new {
                message = "Confirmation successful for PasPay",
            });
		}
    }
}