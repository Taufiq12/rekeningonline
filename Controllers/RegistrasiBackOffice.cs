using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class RegistrasiBackOfficeDto{
    public int id { get; set; }
	[Required]
    public string Mail { get; set; }
    public string Password { get; set; }
    public string FullName { get; set; }
    public string NickName { get; set; }
    public int al_id { get; set; }
    public string al_name { get; set; }
    public string al_access_menu { get; set; }
    public string al_access_module { get; set; }
    public string Password_Dukcapil { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("RegistrasiBackOffice")]
    [Route("/api")]
    public class RegistrasiBackOfficeController : ProjectBYBController
    {
        public RegistrasiBackOfficeController(IConfiguration config){}

        [HttpGet("RegistrasiBackOffice")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in vRoleAccess.db()
                where
				// Like
				EF.Functions.Like(s.FullName, $"%{q.search}%") //&&  s.AccessLevel_Id == 3
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "FullName") query = query.OrderBy(x=>x.FullName);
            else query = query.OrderBy(x=>x.FullName);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("RegistrasiBackOffice/{id}")]
        public IActionResult Detail(int id)
        {
            var row = vRoleAccess.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("RegistrasiBackOffice")]
        public IActionResult Crud([FromBody] RegistrasiBackOfficeDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var fill = new Register();
            var first_check = Register.db().Where(x=>x.Mail==b.Mail).FirstOrDefault();
            if(first_check!=null) fill = first_check;
            fill.Mail = b.Mail;
            fill.FullName = b.FullName;
            fill.NickName = b.NickName;
            fill.AccessLevel_Id = b.al_id;
            fill.Password = Encrypt.Crypt(b.Password);
            fill.isActive = 1;

            if(first_check==null){

                var AccessUser = new AccessCustomer();
                var AccessUser_check = AccessCustomer.db().Where(x=>x.ae_mail==b.Mail).FirstOrDefault();
                if(AccessUser_check!=null) AccessUser = AccessUser_check;
                    AccessUser.ae_mail = b.Mail;
                    AccessUser.ae_al_id = b.al_id;
                    AccessUser.ae_status = true;

                if(first_check==null){
                    AccessCustomer.Insert(AccessUser);
                }else{
                     AccessCustomer.Update(AccessUser);
                }

                if(b.Password_Dukcapil!=null){
                    var LoginDukcapil = new LoginDukcapil();
                    var LoginDukcapil_check = LoginDukcapil.db().Where(x=>x.email==b.Mail).FirstOrDefault();
                    if(LoginDukcapil_check!=null) LoginDukcapil = LoginDukcapil_check;
                        LoginDukcapil.email = b.Mail;
                        LoginDukcapil.password = b.Password_Dukcapil;

                    if(first_check==null){
                        LoginDukcapil.Insert(LoginDukcapil);
                    }else{
                        LoginDukcapil.Update(LoginDukcapil);
                    }
                }

                Register.Insert(fill);
                
                Emails.RegisterLoginBackOffice(fill);
                return Ok(new {
                    message = "Add Password successful",
                });
            }else{
                fill.id = b.id;
                Register.Update(fill);

                Emails.RegisterLoginBackOffice(fill);
                return Ok(new {
                    message = "Update Branch successful",
                });
            }

           
            
        }

        [HttpPost("RegistrasiBackOffice/delete")]
        public IActionResult Delete([FromBody] RegistrasiBackOfficeDto b){
            var row = Register.Find(b.id);

            Register.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}