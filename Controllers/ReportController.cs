using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using ProjectBYB.Middleware;
using System.IO;
using OfficeOpenXml;
using System.Net.Http.Headers;
using System.Text;
using System.Security.Cryptography;


namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class ReportController : ProjectBYBController
    {
        public ReportController(IConfiguration config){}

        public Boolean ReportRoleUserAccount(UserVirtualAccount v){
            if(vuser.isModule("ApprovalSTAFFBO")) return true;
            if(vuser.isModule("ApprovalSPVBO")) return true;
            if(vuser.isModule("Admin")) return true;
            if(v.name == vuser.FullName) return true;
            return false;
        }

        public Boolean ReportRolePayment(vPaymentList v){
            if(vuser.isModule("ApprovalSTAFFBO")) return true;
            if(vuser.isModule("ApprovalSPVBO")) return true;
            if(vuser.isModule("Admin")) return true;
            if(v.name == vuser.FullName) return true;
            return false;
        }

        public Boolean ReportRoleAccountFP(vHeaderInquiryPaymentFaspay v){
            if(vuser.isModule("ApprovalSTAFFBO")) return true;
            if(vuser.isModule("ApprovalSPVBO")) return true;
            if(vuser.isModule("Admin")) return true;
            if(v.FullName == vuser.FullName) return true;
            return false;
        }

		public Boolean SearchLogicUser(UserVirtualAccount v, ListTableDto q){
			if(EF.Functions.Like(v.id, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.owner_id, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.external_id, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.bank_code, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.account_number, $"%{q.search}%")) return true;
			return EF.Functions.Like(v.name, $"%{q.search}%");
		}

        public Boolean SearchLogicDataNasabah(vDataRekeningNasabah v, ListTableDto q){
			if(EF.Functions.Like(v.CIF, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.Rekening, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.Virtual_Account, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.NamaNasabah, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.KodeAO, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.NamaAO, $"%{q.search}%")) return true;
			return EF.Functions.Like(v.Kode_Referral, $"%{q.search}%");
		}

        public Boolean SearchLogicPayment(vPaymentList v, ListTableDto q){
			if(EF.Functions.Like(v.id, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.payment_id, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.owner_id, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.external_id, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.bank_code, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.account_number, $"%{q.search}%")) return true;
			return EF.Functions.Like(v.name, $"%{q.search}%");
		}

        public Boolean SearchLogic(KodeReferance v, ListTableDto q){
			if(EF.Functions.Like(v.Name, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.Title, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.KTP, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.Reporting, $"%{q.search}%")) return true;
			return EF.Functions.Like(v.Kode_AO, $"%{q.search}%");
		}

        public Boolean SearchLogicInquiryPaymentFP(vHeaderInquiryPaymentFaspay v, ListTableDto q){
			if(EF.Functions.Like(v.trx_id, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.FullName, $"%{q.search}%")) return true;
			if(EF.Functions.Like(v.bill_no, $"%{q.search}%")) return true;
            if(EF.Functions.Like(v.Payment_Channel, $"%{q.search}%")) return true;
			return EF.Functions.Like(v.redirect_url, $"%{q.search}%");
		}


        [HttpGet("ListPaymentReport")]
        public IActionResult ListPaymentReport([FromQuery] ListTableDto q){
            var query = from s in vPaymentList.db()
                where SearchLogicPayment(s, q)
                select s;
			
			query = query.Where(x=>ReportRolePayment(x)); 

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "bank_code") query = query.OrderBy(x=>x.bank_code);
            else if(q.sort == "payment_id") query = query.OrderByDescending(x=>x.payment_id);
            else if(q.sort == "merchant_code") query = query.OrderByDescending(x=>x.merchant_code);
            else if(q.sort == "name") query = query.OrderByDescending(x=>x.name);
            else if(q.sort == "account_number") query = query.OrderByDescending(x=>x.account_number);
            else query = query.OrderByDescending(x=>x.transaction_timestamp);

            if(q.downloadExcel){
                return DownloadExcelPaymentVA(query);
            }

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("ListAOAkulakuReport")]
        public IActionResult ListAOAkulakuReport([FromQuery] ListTableDto q){
            var query = from s in KodeReferance.db()
                where SearchLogic(s, q)
                select s;
			
            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "Kode_AO") query = query.OrderBy(x=>x.Kode_AO);
            else query = query.OrderBy(x=>x.Kode_AO);

            if(q.downloadExcel){
                return DownloadExcel(query);
            }

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }


        [HttpGet("ListVirtualAccountReport")]
        public IActionResult ListVirtualAccountReport([FromQuery] ListTableDto q){
          
            var query = from s in UserVirtualAccount.db()
                where SearchLogicUser(s, q)
                select s;
			
			query = query.Where(x=>ReportRoleUserAccount(x)); 


            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "bank_code") query = query.OrderBy(x=>x.bank_code);
            else if(q.sort == "merchant_code") query = query.OrderByDescending(x=>x.merchant_code);
            else if(q.sort == "name") query = query.OrderByDescending(x=>x.name);
            else if(q.sort == "account_number") query = query.OrderByDescending(x=>x.account_number);
            else query = query.OrderByDescending(x=>x.created);

            if(q.downloadExcel){
                return DownloadExcelUserVA(query);
            }

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("FlowStatusDetail")]
        public IActionResult FlowStatusDetail([FromQuery] string external_id, [FromQuery] string owner_id, [FromQuery] DateTime created){

            var va = UserVirtualAccount.Find(external_id,owner_id);
			var query = from s in LogPayment.db()
                where s.external_id == external_id && s.created.ToString("dd-MM-yyyy").Contains(created.ToString("dd-MM-yyyy")) 
                select s;
                
            var total = query.Count();
            var data = new List<JObject>();

            query.ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                ai = va.GetData(),
                history =  data
            });
        }

        public IActionResult DownloadExcelUserVA(IQueryable<UserVirtualAccount> query){
            string sWebRootFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", "excel");
            string sFileName = @"VirtualAccount.xlsx";
            string targetFile = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(targetFile);
            if (file.Exists){
                file.Delete();
                file = new FileInfo(targetFile);
            } using (ExcelPackage package = new ExcelPackage(file)) {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("VirtualAccount");
                //First add the headers
                worksheet.Cells["A1"].Value = "Virtual Account";
                worksheet.Cells["B1"].Value = "Name";
                worksheet.Cells["C1"].Value = "Bank";
                worksheet.Cells["D1"].Value = "Status";
				worksheet.Cells["A1:D1"].Style.Font.Bold = true;

				var i = 2;
                query.ToList().ForEach(v=>{
					worksheet.Cells[i, 1].Value = v.account_number;
					worksheet.Cells[i, 2].Value = v.name;
					worksheet.Cells[i, 3].Value = v.bank_code;
					worksheet.Cells[i, 4].Value = v.status;
                    i++;
                });
        
                package.Save(); //Save the workbook.
            }

            var result = PhysicalFile(targetFile, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment"){
                FileName = file.Name
            }.ToString();

            return result;
        }

        public IActionResult DownloadExcelPaymentVA(IQueryable<vPaymentList> query){
            string sWebRootFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", "excel");
            string sFileName = @"Payment.xlsx";
            string targetFile = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(targetFile);
            if (file.Exists){
                file.Delete();
                file = new FileInfo(targetFile);
            } using (ExcelPackage package = new ExcelPackage(file)) {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Payment");
                //First add the headers
                worksheet.Cells["A1"].Value = "Payment ID";
                worksheet.Cells["B1"].Value = "Virtual Account";
                worksheet.Cells["C1"].Value = "Name";
                worksheet.Cells["D1"].Value = "Bank";
                worksheet.Cells["E1"].Value = "Amount";
                worksheet.Cells["F1"].Value = "Transaction Date";
				worksheet.Cells["A1:F1"].Style.Font.Bold = true;

				var i = 2;
                query.ToList().ForEach(v=>{
                    worksheet.Cells[i, 1].Value = v.payment_id;
					worksheet.Cells[i, 2].Value = v.account_number;
					worksheet.Cells[i, 3].Value = v.name;
					worksheet.Cells[i, 4].Value = v.bank_code;
					worksheet.Cells[i, 5].Value = v.amount;
                    worksheet.Cells[i, 6].Value = v.transaction_timestamp;
                    i++;
                });
        
                package.Save(); //Save the workbook.
            }

            var result = PhysicalFile(targetFile, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment"){
                FileName = file.Name
            }.ToString();

            return result;
        }

        public IActionResult DownloadExcel(IQueryable<KodeReferance> query){
            string sWebRootFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", "excel");
            string sFileName = @"DataAOAkulaku.xlsx";
            string targetFile = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(targetFile);
            if (file.Exists){
                file.Delete();
                file = new FileInfo(targetFile);
            } using (ExcelPackage package = new ExcelPackage(file)) {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DataAOAkulaku");
                //First add the headers
                worksheet.Cells["A1"].Value = "Name";
                worksheet.Cells["B1"].Value = "Title";
                worksheet.Cells["C1"].Value = "KTP";
                worksheet.Cells["D1"].Value = "Reporting";
                worksheet.Cells["E1"].Value = "Code AO";
                worksheet.Cells["F1"].Value = "Code Referral";
				worksheet.Cells["A1:F1"].Style.Font.Bold = true;

				var i = 2;
                query.ToList().ForEach(v=>{
                    worksheet.Cells[i, 1].Value = v.Name;
					worksheet.Cells[i, 2].Value = v.Title;
					worksheet.Cells[i, 3].Value = v.KTP;
					worksheet.Cells[i, 4].Value = v.Reporting;
					worksheet.Cells[i, 5].Value = v.Kode_AO;
                    worksheet.Cells[i, 6].Value = v.Kode_Referral;
                    i++;
                });
        
                package.Save(); //Save the workbook.
            }

            var result = PhysicalFile(targetFile, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment"){
                FileName = file.Name
            }.ToString();

            return result;
        }

        public IActionResult DownloadExcelDataNasabah(IQueryable<vDataRekeningNasabah> query){
            string sWebRootFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", "excel");
            string sFileName = @"DataNasabah.xlsx";
            string targetFile = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(targetFile);
            if (file.Exists){
                file.Delete();
                file = new FileInfo(targetFile);
            } using (ExcelPackage package = new ExcelPackage(file)) {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DataNasabah");
                //First add the headers
                worksheet.Cells["A1"].Value = "CIF";
                worksheet.Cells["B1"].Value = "Rekening";
                worksheet.Cells["C1"].Value = "Virtual Account";
                worksheet.Cells["D1"].Value = "Nama Nasabah";
                worksheet.Cells["E1"].Value = "Title";
                worksheet.Cells["F1"].Value = "Tempat Lahir";
                worksheet.Cells["G1"].Value = "Tanggal Lahir";
                worksheet.Cells["H1"].Value = "Jenis Kelamin";
                worksheet.Cells["I1"].Value = "Nama Ibu Kandung";
                worksheet.Cells["J1"].Value = "No. KTP";
                worksheet.Cells["K1"].Value = "No. NPWP";
                worksheet.Cells["L1"].Value = "Tanggal Terbit KTP";
                worksheet.Cells["M1"].Value = "Tanggal Berakhir KTP";
                worksheet.Cells["N1"].Value = "Province";
                worksheet.Cells["O1"].Value = "Kabupaten";
                worksheet.Cells["P1"].Value = "Kecamatan";
                worksheet.Cells["Q1"].Value = "Kelurahan";
                worksheet.Cells["R1"].Value = "Kode AO";
                worksheet.Cells["S1"].Value = "Nama AO";
                worksheet.Cells["T1"].Value = "Kode Group";
				worksheet.Cells["A1:T1"].Style.Font.Bold = true;

				var i = 2;
                query.ToList().ForEach(v=>{
					worksheet.Cells[i, 1].Value = v.CIF;
					worksheet.Cells[i, 2].Value = v.Rekening;
					worksheet.Cells[i, 3].Value = v.Virtual_Account;
					worksheet.Cells[i, 4].Value = v.NamaNasabah;
                    worksheet.Cells[i, 5].Value = v.Title;
                    worksheet.Cells[i, 6].Value = v.TempatLahir;
                    worksheet.Cells[i, 7].Value = v.TanggalLahir;
                    worksheet.Cells[i, 8].Value = v.JenisKelamin;
                    worksheet.Cells[i, 9].Value = v.NamaIbuKandung;
                    worksheet.Cells[i, 10].Value = v.NoKTP;
                    worksheet.Cells[i, 11].Value = v.NPWP;
                    worksheet.Cells[i, 12].Value = v.TanggalTerbitKTP;
                    worksheet.Cells[i, 13].Value = v.TanggalBerakhirKTP;
                    worksheet.Cells[i, 14].Value = v.Province;
                    worksheet.Cells[i, 15].Value = v.Kabupaten;
                    worksheet.Cells[i, 16].Value = v.Kecamatan;
                    worksheet.Cells[i, 17].Value = v.Kelurahan;
                    worksheet.Cells[i, 18].Value = v.KodeAO;
                    worksheet.Cells[i, 19].Value = v.NamaAO;
                    worksheet.Cells[i, 20].Value = v.Kode_Referral;
                    i++;
                });
        
                package.Save(); //Save the workbook.
            }

            var result = PhysicalFile(targetFile, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment"){
                FileName = file.Name
            }.ToString();

            return result;
        }

        [HttpGet("ListNasabah")]
        public IActionResult ListNasabah([FromQuery] ListTableDto q){
            var query = from s in vDataRekeningNasabah.db()
                where SearchLogicDataNasabah(s, q)
                select s;
			
			query = query.Where(x=>1==1); 

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "NamaNasabah") query = query.OrderBy(x=>x.NamaNasabah);

            if(q.downloadExcel){
                return DownloadExcelDataNasabah(query);
            }

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("ListVirtualAccountFaspayReport")]
        public IActionResult ListVirtualAccountFaspayReport([FromQuery] ListTableDto q){
            var query = from s in vHeaderInquiryPaymentFaspay.db()
                where SearchLogicInquiryPaymentFP(s, q)
                select s;
			
			query = query.Where(x=>ReportRoleAccountFP(x)); 

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "trx_id") query = query.OrderBy(x=>x.trx_id);
            else if(q.sort == "FullName") query = query.OrderByDescending(x=>x.FullName);
            else if(q.sort == "bill_no") query = query.OrderByDescending(x=>x.bill_no);
            else if(q.sort == "Payment_Channel") query = query.OrderByDescending(x=>x.Payment_Channel);
            else query = query.OrderByDescending(x=>x.bill_no);

            if(q.downloadExcel){
                return DownloadExcelUserVAFP(query);
            }

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("FlowStatusDetailFaspay")]
        public IActionResult FlowStatusDetailFaspay([FromQuery] string trx_id, [FromQuery] string bill_no){

            //Check Inquiry
            string userid = Config.User_Merchant_Id;
            string password = Config.User_Merchant_Password;

            var Inq = UserVirtualAccountFasPay.db().Where(a=>a.trx_id == trx_id && a.bill_no == bill_no).FirstOrDefault();

            string testString = userid+password+Inq.bill_no;
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(testString);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string signatuerMD5 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            string signatuerSH1;
            byte[] temp;

            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(signatuerMD5));

            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
                 {
                    sb.Append(temp[i].ToString("x2"));
                  }

            signatuerSH1 = sb.ToString();

            ApiPasPay.InquiryRedirectProcess("Pengecekan Status Pembayaran",Inq.trx_id,Convert.ToInt32(Config.Merchant_Id),Inq.bill_no,signatuerSH1);

            //------------------------------------------------------------------------------

            var va = vHeaderInquiryPaymentFaspay.Find(trx_id,bill_no);
			var query = from s in vInquiryPaymentFaspay.db()
                where s.trx_id == trx_id && s.bill_no == bill_no
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            query.ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
                
            return Ok(new {
                ai = va.GetData(),
                history =  data
            });
        }

        public IActionResult DownloadExcelUserVAFP(IQueryable<vHeaderInquiryPaymentFaspay> query){
            string sWebRootFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", "excel");
            string sFileName = @"VirtualAccountFP.xlsx";
            string targetFile = Path.Combine(sWebRootFolder, sFileName);
            FileInfo file = new FileInfo(targetFile);
            if (file.Exists){
                file.Delete();
                file = new FileInfo(targetFile);
            } using (ExcelPackage package = new ExcelPackage(file)) {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("VirtualAccount");
                //First add the headers
                worksheet.Cells["A1"].Value = "Virtual Account";
                worksheet.Cells["B1"].Value = "Name";
                worksheet.Cells["C1"].Value = "Order Number";
                worksheet.Cells["D1"].Value = "Bank";
				worksheet.Cells["A1:D1"].Style.Font.Bold = true;

				var i = 2;
                query.ToList().ForEach(v=>{
					worksheet.Cells[i, 1].Value = v.trx_id;
					worksheet.Cells[i, 2].Value = v.FullName;
					worksheet.Cells[i, 3].Value = v.bill_no;
					worksheet.Cells[i, 4].Value = v.Payment_Channel;
                    i++;
                });
        
                package.Save(); //Save the workbook.
            }

            var result = PhysicalFile(targetFile, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment"){
                FileName = file.Name
            }.ToString();

            return result;
        }
    }
}