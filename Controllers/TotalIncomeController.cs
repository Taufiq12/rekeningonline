using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;

public class TotalIncomeDto{
    public int id { get; set; }
	[Required]
    public string TotalIncome_Desc { get; set; }
}

namespace ProjectBYB.Controllers
{
    [CustomerAuth("TotalIncome")]
    [Route("/api")]
    public class TotalIncomeController : ProjectBYBController
    {
        public TotalIncomeController(IConfiguration config){}

        [HttpGet("TotalIncome")]
        public IActionResult Get([FromQuery] ListTableDto q)
        {
            var query = from s in TotalIncome.db()
                where
				// Like
				EF.Functions.Like(s.TotalIncome_Desc, $"%{q.search}%")
                select s;

            var total = query.Count();
            var data = new List<JObject>();

            /* Sorting */
            if(q.sort == "TotalIncome_desc") query = query.OrderByDescending(x=>x.TotalIncome_Desc);
            else query = query.OrderBy(x=>x.TotalIncome_Desc);

            query.Skip(q.skip())
                .Take(q.limit)
                .ToList().ForEach(x=>{
                    data.Add(x.GetData());
                });
            return Ok(new {
                data = Gen.Pagination(total, data, q)
            });
        }

        [HttpGet("TotalIncome/{id}")]
        public IActionResult Detail(int id)
        {
            var row = TotalIncome.db().Where(x=>x.id==id).FirstOrDefault();
            return Ok(new {
                row = row == null ? new {Status=true} : row.GetData().ToObject<dynamic>(),
            });
        }

        [HttpPost("TotalIncome")]
        public IActionResult Crud([FromBody] TotalIncomeDto b){
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var first = TotalIncome.Find(b.id);
            if(first==null){
                if(TotalIncome.db().Where(x=>x.TotalIncome_Desc==b.TotalIncome_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "TotalIncome already exists"});
            }else{
                if(TotalIncome.db().Where(x=>x.id!=b.id&&x.TotalIncome_Desc==b.TotalIncome_Desc).FirstOrDefault()!=null)
                    return BadRequest(new {message = "TotalIncome already exists"});
            }

            var fill = new TotalIncome();
            fill.TotalIncome_Desc = b.TotalIncome_Desc;
            if(first==null){
                TotalIncome.Insert(fill);
                return Ok(new {
                    message = "Add TotalIncome successful",
                });
            }
            fill.id = b.id;
            TotalIncome.Update(fill);
            return Ok(new {
                message = "Update TotalIncome successful",
            });
        }

        [HttpPost("TotalIncome/change-status")]
        public IActionResult ChangeStatus([FromBody] TotalIncomeDto b){
            var row = TotalIncome.Find(b.id);
            TotalIncome.Update(row);
            return Ok(new {
                message = "Change status successful"
            });
        }

        [HttpPost("TotalIncome/delete")]
        public IActionResult Delete([FromBody] ChangeStatusDto b){
            var row = TotalIncome.Find(b.id);
            TotalIncome.Remove(row);
            return Ok(new {
                message = "Delete successful"
            });
        }
    }
}