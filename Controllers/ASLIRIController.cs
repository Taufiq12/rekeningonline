using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using System.IO;
using System.Drawing;

public class AsliRIDto{
    [Required]
    public string Mail { get; set; }

}

namespace ProjectBYB.Controllers
{
    [CustomerAuth]
    [Route("/api")]
    public class ASLIRIController : ProjectBYBController
    {
        public ASLIRIController(IConfiguration config){}

        [HttpPost("BasicVerificationASLIRI")]
        public IActionResult VerificationASLIRI([FromBody] AsliRIDto b){
            if(!ModelState.IsValid) return BadRequest(ModelState);
            
            var cus = Customer.Find(b.Mail);

            //Get Attachment FILE
            var attachment = Attachment.FindMail(b.Mail); 

            var attachFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploader", attachment.Attachment2);
            //------------------------------------------------------
            
            //Convert Base64encode
            byte[] fileBit = System.Text.ASCIIEncoding.ASCII.GetBytes(attachFile);
            string selfie_photo = System.Convert.ToBase64String(fileBit);
            //------------------------------------------------------
            string nik = cus.Identity;
            string name = cus.FullName;
            string birthdate = Convert.ToDateTime(cus.DateOfBirth).ToString("dd-MM-yyyy");
            string birthplace = cus.PlaceOfBirth;
            string address = cus.AddressIdentity;

            //Endpoint ASLI RI Validation E-KTP
            ApiAsliRI.ValidasiAsliRI(nik.Trim(),name.Trim(),birthdate.Trim(),birthplace.Trim(),address.Trim(),"",selfie_photo.Trim());

			var ai = Verification_AsliRI.Find(nik);

            var cusdet = vCustomerDetail.Find(b.Mail);
            
            return Ok(new {
					data = new {
						name = ai.Name != null ? ai.Name : "false",
						birthdate = ai.birthdate != null ? ai.birthdate : "false",
						birthplace = ai.birthplace != null ? ai.birthplace : "false",
						address = ai.address,
                        addressasliri = ai.addressasliri != null ? "true" : "false",
                        selfie_photo = ai.selfie_photo,
                        status = ai.status,
                        isRequestApprove = cusdet.isRequestApprove
					}
				});
		}
    }
}