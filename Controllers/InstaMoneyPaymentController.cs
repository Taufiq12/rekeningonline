using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using ProjectBYB.Middleware;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Helpers;

public class InstaMoneyPaymentDto {
	    public string payment_id { get; set; }
        public string callback_virtual_account_id { get; set; }
        public string owner_id { get; set; }
        public string external_id { get; set; }
        public string account_number { get; set; }
        public string bank_code { get; set; }
        public decimal amount { get; set; }
        public string merchant_code { get; set; }
        public string id { get; set; }
        public DateTime transaction_timestamp { get; set; }
        public DateTime updated { get; set; }
        public DateTime created { get; set; }
}


namespace ProjectBYB.Controllers
{
    [Route("/payment")]
    public class InstaMoneyPaymentController : ProjectBYBController
    {
        public InstaMoneyPaymentController(IConfiguration config){}

        [HttpPost("virtual_account_paid_callback_url")]
        public IActionResult VirtualAccountPaidCallbackURL([FromBody] InstaMoneyPaymentDto b){

            //Credentials
            string username = Config.ApiAuthInstaMoney;
            string password = "";
            string svcCredentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

            var ResponseToken = Request.Headers["x-callback-token"].ToString();

            if (Config.AuthCallbackToken == ResponseToken)
            {

                var pva = new PaymentVirtualAccount();
                var first = PaymentVirtualAccount.db().Where(x=>x.id==b.id && x.owner_id == b.owner_id && x.external_id == b.external_id && x.account_number == b.account_number && x.bank_code == b.bank_code).FirstOrDefault();

                if(first!=null) pva = first;
                pva.id = b.id;
                pva.payment_id = b.payment_id;
                pva.callback_virtual_account_id = b.callback_virtual_account_id;
                pva.owner_id = b.owner_id;
                pva.external_id = b.external_id;
                pva.account_number = b.account_number;
                pva.bank_code = b.bank_code;
                pva.amount = b.amount;
                pva.transaction_timestamp = b.transaction_timestamp;
                pva.merchant_code = Convert.ToInt32(b.merchant_code);
                pva.updated = b.updated;
                pva.created = b.created;
                
                if(first==null){
                    PaymentVirtualAccount.Insert(pva);
                }else{
                    PaymentVirtualAccount.Update(pva);
                }

                //Log Payment 
                var lpa = new LogPayment();
                var data = LogPayment.db().Where(x=>x.id == b.id && x.owner_id == b.owner_id && x.external_id == b.external_id && x.status == "Pembayaran sudah Diverifikasi").FirstOrDefault();
                if(data!=null) lpa = data;
                lpa.id = b.id;
                lpa.owner_id = b.owner_id;
                lpa.external_id = b.external_id;
                lpa.status = "Pembayaran sudah Diverifikasi";
                lpa.created = DateTime.Now;

                LogPayment.Insert(lpa);


                //Update Status
                var cus = Customer.db().Where(x=>x.MobileNumber == b.external_id).FirstOrDefault();
                cus.Status = "Pembayaran sudah Diverifikasi";
                Customer.Update(cus);
                
                //Send Mail
                var ai = Register.db().Where(x => x.MobileNumber == b.external_id).FirstOrDefault();
                Emails.SuksesTransferVA(ai);

                //Proses API Swagger----------------------------------------------------------   

                if(cus.cif == null)
                {                
                    //Step 1
                    //Enpoint Buat CIF
                     var cif = vCustomerCIF.db().Where(x=>x.namaNasabah == cus.FullName && x.noID == cus.Identity && x.namaIbuKandung == cus.MotherName && x.tanggalLahir == Convert.ToDateTime(cus.DateOfBirth).ToString("yyyy-MM-dd") && x.tempatLahir == cus.PlaceOfBirth).FirstOrDefault();
                        ApiSwagger.RequestCIF(
                        cif.namaNasabah.Trim().ToUpper(), cif.kodeCabang.Trim().ToUpper(), cif.alamatID1.Trim().ToUpper(), cif.alamatID2.Trim().ToUpper(),
                        cif.kelurahan.Trim().ToUpper(), cif.kecamatan.Trim().ToUpper(), cif.kota.Trim().ToUpper(), cif.kodePos.Trim().ToUpper(),
                        cif.propinsi.Trim().ToUpper(), cif.kodeNomorTelepon.Trim().ToUpper(), cif.nomorTelepon.Trim().ToUpper(), cif.nomorHP.Trim().ToUpper(),
                        cif.alamatEmail.Trim().ToUpper(), cif.kodeAO.Trim().ToUpper(), cif.npwp.Trim().ToUpper(), cif.jenisNasabah.Trim().ToUpper(),
                        cif.jenisKelamin.Trim().ToUpper(), cif.agama.Trim().ToUpper(), cif.statusPerkawinan.Trim().ToUpper(), cif.pendidikanTerakhir.Trim().ToUpper(),
                        cif.jumlahTanggungan.Trim().ToUpper(), cif.anak.Trim().ToUpper(), cif.istri.Trim().ToUpper(), cif.tempatLahir.Trim().ToUpper(),
                        cif.tanggalLahir.Trim().ToUpper(), cif.negaraAsal.Trim().ToUpper(), cif.jenisIdentitas.Trim().ToUpper(), cif.noID.Trim().ToUpper(),
                        cif.tanggalTerbitID.Trim().ToUpper(), cif.tanggalBerakhirID.Trim().ToUpper(), cif.jenisPekerjaan.Trim().ToUpper(), cif.sumberPenghasilan.Trim().ToUpper(),
                        cif.penghasilanPerBulan, cif.namaBadanUsaha.Trim().ToUpper(), cif.bidangUsaha.Trim().ToUpper(), cif.namaIbuKandung.Trim().ToUpper(),
                        cif.golonganPemilik.Trim().ToUpper(), cif.lamaBekerja.Trim().ToUpper(), cif.apuDataProfileResiko.Trim().ToUpper(), cif.apuIdentitasNasabah.Trim().ToUpper(),
                        cif.apuLokasiUsaha.Trim().ToUpper(), cif.apuProfilNasabah.Trim().ToUpper(), cif.apuJumlahTransaksi.Trim().ToUpper(), cif.apuKegiatanUsaha.Trim().ToUpper(),
                        cif.apuStrukturKepemilikan, cif.apuInformasiLain1.Trim().ToUpper(), cif.apuResumeAkhir.Trim().ToUpper()
                    );
                }

                if(cus.cif != null)
                {   
                    //Step 2
                    //Endpoint Pembukaan Rekening
                    var rek = vCustomerRekening.db().Where(x=>x.namaNasabah == cus.FullName && x.namaRekening == cus.FullName && x.Mail == cus.Mail).FirstOrDefault();

                    var EndDate = EndOfDay.db().FirstOrDefault();
                    DateTime currentTime = EndDate.eod_Date;
                    string eod = currentTime.ToString("yyyy-MM-ddTHH:mm:ssZ");

                    string date;
                    if (EndDate.is_eod == true)
                    {
                        date = eod;
                    }else{ 
                        date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
                    }
                    //date = "2019-06-05T09:06:15.565Z";
                    string tanggalBuka = date;
                    //-------------------------------------------------------------------
                    ApiSwagger.RequestPembukaanRekening(cus.Mail.Trim().ToUpper(),rek.jenisPenggunaan.Trim().ToUpper(),rek.jenisRekening.Trim().ToUpper(),rek.kodeCabang.Trim().ToUpper(),
                    rek.kodeMataUang.Trim().ToUpper(),rek.kolektibilitas.Trim().ToUpper(),rek.lokasi.Trim().ToUpper(),rek.namaNasabah.Trim().ToUpper(),rek.namaNasabah.Trim().ToUpper(),
                    rek.nomorNasabah.Trim().ToUpper(),rek.sektorEkonomi.Trim().ToUpper(),tanggalBuka.Trim().ToUpper());
                    //----------------------------------------------------------------------------
                }
                
                return Ok(new {
                    id = b.id,
                    payment_id = b.payment_id,
                    callback_virtual_account_id = b.callback_virtual_account_id,
                    owner_id = b.owner_id,
                    external_id = b.external_id,
                    account_number = b.account_number,
                    bank_code = b.bank_code,
                    amount = b.amount,
                    transaction_timestamp = b.transaction_timestamp,
                    merchant_code = b.merchant_code,
                    updated = b.updated,
                    created = b.created
                });

            }else{
                return Ok(new {
                    StatusCode = 401,
                    message = "Unauthorized"
                    
                });
            }
        }

    }
}