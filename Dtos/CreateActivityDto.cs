using System.ComponentModel.DataAnnotations;

public class CreateActivityDto{
    [Required]
    public string type { get; set; }
    [Required]
    public string cwid { get; set; }
    [Required]
    public string Req_ID { get; set; }
}