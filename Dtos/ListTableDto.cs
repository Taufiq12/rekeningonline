using System;

public class ListTableDto
{
    public string search { get; set; }
    public string sort { get; set; } = "";
    public string type { get; set; }
    public int page { get; set; } = 1;
    public int limit { get; set; } = 50;
    public DateTime startdate { get; set; }
    public DateTime enddate { get; set; }
    public DateTime effective_startdate { get; set; }
    public DateTime effective_enddate { get; set; }
    public Boolean downloadExcel { get; set; }

    public int skip(){
        return (this.page-1) * this.limit;
    }
}