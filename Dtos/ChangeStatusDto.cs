using System;
using System.Collections.Generic;

public class ChangeStatusDto{
    public int id { get; set; }
    public Boolean status { get; set; }

    public string Mail { get; set; }
}

public class SortingDto{
    public List<SortingObjDto> sorting { get; set; }
}
public class SortingObjDto{
    public int id { get; set; }
    public int sort { get; set; }
}