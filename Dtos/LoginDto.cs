using System.ComponentModel.DataAnnotations;

public class LoginDto{
    [Required]
    public string Mail { get; set; }
    [Required]
    public string password { get; set; }
    public bool noGenerate { get; set; }
}