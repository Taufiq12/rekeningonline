using System.ComponentModel.DataAnnotations;

public class AuditTrailDto{
    [Required]
    public string applicationName { get; set; }
    [Required]
    public string cwid { get; set; }
    public string functionName { get; set; }
    public string description { get; set; }
}