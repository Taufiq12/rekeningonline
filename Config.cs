using System;
using Microsoft.Extensions.Configuration;

class Config
{
    public static IConfigurationRoot Configuration { get; set; }
    public static string environment { get => Configuration["environment"]; }
    public static string appid { get => Configuration["appid"]; }
    public static string BYB { get => String("BYB"); }
    public static string BaseURL { get => String("BaseURL"); }
    public static string SMTPServer { get => String("SMTPServer"); }
    public static string SMTPEmail { get => String("SMTPEmail"); }
    public static int SMTPPort { get => Int("SMTPPort"); }
    public static string SMTPUsername { get => String("SMTPUsername"); }
    public static string SMTPPassword { get => String("SMTPPassword"); }
    public static string ToEmail { get => String("ToEmail"); }
    public static int Port = 5000;
    public static bool ByPass { get => Bool("ByPass"); }

    public static string User_Merchant_Id { get => String("User_Merchant_Id"); }
    public static string User_Merchant_Password { get => String("User_Merchant_Password"); }

     //Endpoint VA Faspay---------------------------
    public static string ApiRegisterFaspayUrl { get => String("ApiRegisterFaspayUrl");}
    public static string ApiResponseRedirectFaspay { get => String("ApiResponseRedirectFaspay"); }
    public static string ApiInquiryPaymentStatusFaspay { get => String("ApiInquiryPaymentStatusFaspay"); }
    public static string ApiCancelPaymentTransactionFaspay { get => String("ApiCancelPaymentTransactionFaspay"); }
    public static string Merchant_Id { get => String("Merchant_Id"); }
    public static string Merchant { get => String("Merchant"); }
    //-----------------------------------------------

    //Endpoint VA InstaMoney-------------------------
    public static string ApiAuthInstaMoney { get => String("ApiAuthInstaMoney");}
    public static string ApiCreateVirtualAccountInstaMoney { get => String("ApiCreateVirtualAccountInstaMoney");}
    public static string ApiVirtualAccountInstaMoney { get => String("ApiVirtualAccountInstaMoney");}
    public static string AuthCallbackToken { get => String("AuthCallbackToken"); }
    public static string ApiAuthIluma { get => String("ApiAuthIluma"); }
    public static string ApiIlumaPhoneValidator { get => String("ApiIlumaPhoneValidator"); }
    //-----------------------------------------------

    //Endpoint CoreBanking---------------------------
    public static string ApiCoreBankingUrl { get => String("ApiCoreBankingUrl");}
    //-----------------------------------------------

    //Endpoint ASLIRI--------------------------------
    public static string ApiAsliRIUrl { get => String("ApiAsliRIUrl");}
    public static string TokenASLIRI { get => String("TokenASLIRI");}
    //-----------------------------------------------

    //Endpoint Dukcapil------------------------------
    public static string ApiDukcapi { get => String("ApiDukcapil");}
    public static string ApiLoginDukcapil { get => String("ApiLoginDukcapil");}
    public static string ApiCheckNikDukcapil { get => String("ApiCheckNikDukcapil");}
    //-----------------------------------------------

    //Endpoin Swagger--------------------------------
    public static string ApiswaggerNasabah { get => String("ApiswaggerNasabah");}
    public static string ApiswaggerRekening { get => String("ApiswaggerRekening");}
    public static string ApiswaggerCekSaldo { get => String("ApiswaggerCekSaldo");}
    //-----------------------------------------------

    public static string String(string name)
    {
        var str = Configuration["Config:" + environment + ":" + name];
        return str != null ? str : "";
    }

    public static int Int(string name)
    {
        return Int32.Parse(String(name) == "" ? "0" : String(name));
    }

    public static bool Bool(string name)
    {
        return String(name).ToLower() == "true";
    }
}