using ProjectBYB.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProjectBYB.Models.Tables;
using ProjectBYB.Models.Views;
using Helpers;
using System;

namespace ProjectBYB.Middleware
{
    public class CustomerAuth : ActionFilterAttribute
    {
        public string accessMenu = null;
        public CustomerAuth(){}
        public CustomerAuth(string accessMenu){
            this.accessMenu = accessMenu;
        }
        public override void OnActionExecuting(ActionExecutingContext context){

            var ctrl = (ProjectBYBController) context.Controller;
            var authData = new AuthUser(context.HttpContext.Request.Query["token"]);
            var row = authData.authData();

            //Check Payment
            foreach (var cP in vCancelPayment.db())
            {
                var cus = Customer.Find(cP.mail);
                    string MobileNumber = cus.MobileNumber;
                    string CustomerName = cus.FullName;

                    //Update Status
                    cus.StatusType = "REJECT";
                    cus.Status = "Pembayaran telah Expired";

                    //Send Mail
                    var ai = Register.Find(cP.mail);
                    Customer.Update(cus);

                    //Insert Approval Log
                    var aal = new ApprovalLogs();
                    aal.Mail = cP.mail;
                    aal.Status = "Pembayaran telah Expired";
                    
                    aal.Remarks = "Batas pembayaran anda telah expired.";
                    aal.Created_By = "System";
                    aal.Created_Date = DateTime.Now;
                    ApprovalLogs.Insert(aal);

                    //Update VA
                    var VA = UserVirtualAccount.FindID(cP.id);
                    VA.status = "NOT ACTIVE";
                    VA.updated = DateTime.Now;
                    UserVirtualAccount.Update(VA);

                    Emails.CancelPayment(cP);
            }
            
            if(row == null){
                context.Result = new ContentResult(){
                    StatusCode = 401,
                    Content = "Unauthorized"
                };
            }else{
                ctrl.authUser = authData.authProfile();
                ctrl.vuser = ctrl.authUser.vuser();
                if(accessMenu!=null) if(!ctrl.vuser.isMenu(accessMenu)){
                    context.Result = ctrl.noPermissionResponse();
                }
            }
            
            base.OnActionExecuting(context);
        }
    }
}