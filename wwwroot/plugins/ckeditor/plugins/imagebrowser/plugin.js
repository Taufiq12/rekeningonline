CKEDITOR.plugins.add('imagebrowser', {
	"init": function (editor) {
		editor.config.filebrowserImageBrowseUrl = location.origin + "/backoffice/file-explore";
	}
});
